﻿using DummyClient.Network;

namespace DummyClient.Tasks
{
    interface ITask
    {
        void Process(Session session);
    }
}
