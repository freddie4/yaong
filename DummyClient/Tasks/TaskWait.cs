﻿using System.Threading;
using DummyClient.Network;

namespace DummyClient.Tasks
{
    class TaskWait : ITask
    {
        public int msec { get; set; }

        public void Process(Session session)
        {
            Thread.Sleep(1000);
        }
    }
}
