﻿using DummyClient.Network;

namespace DummyClient.Tasks
{
    class TaskConnect : ITask
    {
        public void Process(Session session)
        {
            session.Connect();
            Packet.SendConnectPacket(session);
        }
    }
}
