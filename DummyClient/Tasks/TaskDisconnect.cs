﻿using DummyClient.Network;

namespace DummyClient.Tasks
{
    class TaskDisconnect : ITask
    {
        public void Process(Session session)
        {
            session.Disconnect();
        }
    }
}
