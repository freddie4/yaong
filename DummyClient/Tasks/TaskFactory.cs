﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DummyClient.Logger;
using DummyClient.Network;

namespace DummyClient.Tasks
{
    internal class TaskFactory : IDisposable
    {
        private bool disposed; // to detect redundant calls

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Dispose managed resources.
                }

                // There are no unmanaged resources to release, but
                // if we add them, they need to be released here.
                disposed = true;
            }
        }

        public Queue<ITask> CreateTask(List<Scenario.Tasks> datas)
        {
            var taskQueue = new Queue<ITask>();

            foreach (var task in datas)
            {
                for (var i = 0; i < task.Count; ++i)
                {
                    var type = Type.GetType("DummyClient.Tasks." + "Task" + task.Name);

                    try
                    {
                        var obj = (ITask) Activator.CreateInstance(
                            type ?? throw new InvalidOperationException());

                        taskQueue.Enqueue(obj);
                    }
                    catch (Exception e)
                    {
                        DCLogger.Instance.LogError("Invalid TaskName : {0}", e.ToString());
                        return null;
                    }
                }
            }

            return taskQueue;
        }
    }
}
