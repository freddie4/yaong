﻿using System;
using DummyClient.Math;
using DummyClient.Network;

namespace DummyClient.Tasks
{
    class TaskRandomMove : ITask
    {
        public void Process(Session session)
        {
            var player = session.Player;

            player.RandomMove();
        }
    }
}
