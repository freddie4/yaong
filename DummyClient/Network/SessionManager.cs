﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using DummyClient.Logger;
using DummyClient.Scenario;
using DummyClient.Tasks;

namespace DummyClient.Network
{
    internal class SessionManager
    {
        public enum State
        {
            Running,
            Stop
        }

        public static SessionManager Instance { get; } = new SessionManager();

        public State _state { get; set; } = State.Stop;

        private readonly Dictionary<int, Session> _sessionMap = new Dictionary<int, Session>();
        private static readonly Dictionary<int, Thread> _threadMap = new Dictionary<int, Thread>();

        public int _scenarioEndClientCount;
        private readonly object _lock = new object();

        public ScenarioData _scenarioData { get; set; }

        private int _sessionIndex;

        private readonly Stopwatch _stopwatch = new Stopwatch();

        private SessionManager()
        {
            // ignore
        }

        private bool AddTasks(ref Session session)
        {
            using (var taskFactory = new TaskFactory())
            {
                var taskQueue = taskFactory.CreateTask(_scenarioData.Tasks);

                if (taskQueue == null)
                {
                    return false;
                }

                session.TaskQueue = taskQueue;
            }

            return true;
        }

        public bool CreateAndRunSessionByThread()
        {
            _state = State.Running;

            DCLogger.Instance.LogInfo("Running Scenario : {0}", _scenarioData.Name);

            for (var i = 0; i < _scenarioData.ClientNumber; ++i)
            {
                var session = new Session(_sessionIndex, _scenarioData.ServerIp, _scenarioData.ServerPort, null);

                if (false == AddTasks(ref session))
                {
                    return false;
                }

                var thread = new Thread(() => session.Run(_scenarioData.TaskInterval))
                {
                    Name = "DummyClient" + "-" + session.SessionId
                };

                _sessionMap[_sessionIndex] = session;
                _threadMap[_sessionIndex] = thread;

                _sessionIndex++;
            }

            _stopwatch.Start();

            foreach (var thread in _threadMap)
            {
                thread.Value.Start();
            }

            return true;
        }

        public void JoinThread(int sessionId)
        {
            var thread = _threadMap[sessionId];
            thread.Join();
        }

        public void StopAllThreads()
        {
            if (_sessionMap.Any())
            {
                foreach (var session in _sessionMap)
                {
                    session.Value.StopFlag = true;
                }
            }

            if (_threadMap.Any())
            {
                foreach (var thread in _threadMap)
                {
                    thread.Value.Join();
                }
            }
        }

        public void Clear()
        {
            StopAllThreads();

            if (_threadMap.Any())
            {
                _threadMap.Clear();
            }

            if (_sessionMap.Any())
            {
                _sessionMap.Clear();
            }

            _sessionIndex = 0;
            _scenarioEndClientCount = 0;
            _scenarioData = null;
        }

        public void ScenarioEnd()
        {
            lock (_lock)
            {
                _scenarioEndClientCount++;
                if (_scenarioEndClientCount != _scenarioData.ClientNumber) return;
            }

            _stopwatch.Stop();
            DCLogger.Instance.LogInfo("Scenario End. {0} msec",
                _stopwatch.ElapsedMilliseconds);

            Application.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Background,
                new Action(() => { UIGlobal.MainWaindow.RunButton.Opacity = 1; }));

            _scenarioEndClientCount = 0;
            _state = State.Stop;
        }
    }
}
