﻿using System;
using System.Runtime.InteropServices;
using DummyClient.Math;
using Google.Protobuf;

namespace DummyClient.Network
{
    struct PacketHeader
    {
        public ushort PacketSize;
        public ushort PacketNumber;
    }

    class Packet
    {
        public const int PacketHeaderSize = sizeof(ushort) + sizeof(ushort);

        public static byte[] MakePacket(IMessage packet, PacketCS.ProtocolNumber packetNumber)
        {
            var packetSize = packet.CalculateSize();
            var packetBuffer = new byte[PacketHeaderSize + packetSize];

            var header = new PacketHeader
            {
                PacketSize = (ushort)packetSize,
                PacketNumber = (ushort)packetNumber
            };

            var ptr = Marshal.AllocHGlobal(PacketHeaderSize + packetSize);
            Marshal.StructureToPtr(header, ptr, true);
            Marshal.Copy(ptr, packetBuffer, 0, PacketHeaderSize);
            Marshal.FreeHGlobal(ptr);
            Array.Copy(packet.ToByteArray(), 0, packetBuffer, PacketHeaderSize, packetSize);

            return packetBuffer;
        }

        public static void SendRandomMovePacket(Session session, FVector destPos)
        {
            var packet = new PacketCS.Move
            {
                X = destPos.X,
                Y = destPos.Y,
                Z = destPos.Z,
                PlayerId = session.Player.PlayerId
            };

            session.Send(packet, PacketCS.ProtocolNumber.Emove);
        }

        public static void SendConnectPacket(Session session)
        {
            var player = session.Player;
            var packet = new PacketCS.DummyConnect
            {
                PlayerId = player.PlayerId,
                X = player.CurrentPos.X,
                Y = player.CurrentPos.Y,
                Z = player.CurrentPos.Z,
            };

            session.Send(packet, PacketCS.ProtocolNumber.EdummyConnect);
        }
    }
}
