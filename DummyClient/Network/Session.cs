﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DummyClient.Game;
using Google.Protobuf;
using System.Runtime.InteropServices;
using DummyClient.Logger;
using DummyClient.Tasks;

namespace DummyClient.Network
{
    class Session
    {
        public int SessionId { get; }
        private Socket Socket;
        private readonly IPEndPoint EndPoint;

        private const int RecvBufSize = 256;
        public byte[] RecvBuf = new byte[RecvBufSize];

        public Player Player { get; }

        public Queue<ITask> TaskQueue;
        public bool StopFlag = false;

        public Session(int sessionId, string ip, ushort port, Queue<ITask> queue)
        {
            SessionId = sessionId;
            TaskQueue = queue;

            Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontLinger, true);

            EndPoint = new IPEndPoint(IPAddress.Parse(ip), port);

            Player = new Player(this);
        }

        public void Connect()
        {
            try
            {
                Socket.Connect(EndPoint);
            }
            catch (ArgumentNullException ane)
            {
                Reconnect(ane.ToString());
            }
            catch (SocketException se)
            {
                Reconnect(se.ToString());
            }
            catch (Exception e)
            {
                Reconnect(e.ToString());
            }
        }

        public void Reconnect(string error)
        {
            DCLogger.Instance.LogError("Connection Failed Session-{0} : {1}", SessionId, error);

            Thread.Sleep(5000);
            Connect();
        }

        public void Disconnect()
        {
            Socket.BeginDisconnect(true, DisconnectCallback, Socket);
        }

        private void DisconnectCallback(IAsyncResult ar)
        {
            try
            {
                var socket = (Socket) ar.AsyncState;
                socket.EndDisconnect(ar);

                Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontLinger, true);
            }
            catch (Exception)
            {
                // ignore
            }
        }

        public void Send(IMessage packet, PacketCS.ProtocolNumber packetNumber)
        {
            Send(Packet.MakePacket(packet, packetNumber));
        }

        public void Send(byte[] data)
        {
            if (!Socket.Connected) return;

            Socket.BeginSend(data, 0, data.Length, 0, SendCallback, Socket);
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                var socket = (Socket) ar.AsyncState;
                var byteSent = socket.EndSend(ar);
            }
            catch (Exception)
            {
                // ignore
            }
        }

        public void Receive()
        {
            try
            {
                Socket.BeginReceive(RecvBuf, 0, RecvBufSize, 0, ReceiveCallback, Socket);
            }
            catch (Exception)
            {
                // ignore
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                var socket = (Socket) ar.AsyncState;
                var byteTransferred = socket.EndReceive(ar);

                if (byteTransferred > 0)
                {

                }
            }
            catch (Exception)
            {
                // ignore
            }
        }

        public void Run(uint taskInterval)
        {
            while (true)
            {
                if (StopFlag) break;

                lock (TaskQueue)
                {
                    if (!TaskQueue.Any()) break;

                    var task = TaskQueue.Dequeue();
                    task.Process(this);
                }

                if (taskInterval > 0)
                {
                    Thread.Sleep((int)taskInterval);
                }
            }

            if (Socket.Connected)
            {
                Disconnect();
            }

            SessionManager.Instance.ScenarioEnd();
        }
    }
}
