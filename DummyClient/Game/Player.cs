﻿using System;
using DummyClient.Math;
using DummyClient.Network;

namespace DummyClient.Game
{
    class Player
    {
        private readonly Session Session;

        public int PlayerId;
        public int SessionId;

        public FVector CurrentPos { get; set; }

        private const int RandomPlayerInitPosX = 10000;
        private const int RandomPlayerInitPosY = 10000;
        private const int RandomPlayerInitPosZ = 1000;

        private const int RandomMoveDistanceX = 50;
        private const int RandomMoveDistanceY = 50;
        private const int RandomMoveDistanceZ = 5;

        public Player(Session session)
        {
            Session = session;
            PlayerId = session.SessionId;
            SessionId = session.SessionId;

            var random = new Random(Guid.NewGuid().GetHashCode());

            CurrentPos = new FVector
            {
                X = random.Next(-RandomPlayerInitPosX, RandomPlayerInitPosX),
                Y = random.Next(-RandomPlayerInitPosY, RandomPlayerInitPosY),
                Z = random.Next(-RandomPlayerInitPosZ, RandomPlayerInitPosZ)
            };
        }

        public void RandomMove()
        {
            var random = new Random(Guid.NewGuid().GetHashCode());

            var destPos = new FVector
            {
                X = CurrentPos.X + random.Next(-RandomMoveDistanceX, RandomMoveDistanceX),
                Y = CurrentPos.Y + random.Next(-RandomMoveDistanceY, RandomMoveDistanceY),
                Z = CurrentPos.Z + random.Next(-RandomMoveDistanceZ, RandomMoveDistanceZ)
            };

            CurrentPos = destPos;

            Packet.SendRandomMovePacket(Session, destPos);
        }
    }
}
