﻿using System.Collections.Generic;

namespace DummyClient.Scenario
{
    class ScenarioData
    {
        public string Name = "DefaultScenarioName";
        public string ServerIp = "";
        public ushort ServerPort = 0;
        public ushort ClientNumber = 0;
        public uint TaskInterval = 0;
        public List<Tasks> Tasks = null;
    }

    class Tasks
    {
        public string Name = "";
        public ushort Count = 0;
        public List<Parameters> Parameters = null;
    }

    class Parameters
    {
        public int msec = 0;
    }
}
