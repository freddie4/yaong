﻿using System;
using System.IO;
using System.Windows;
using DummyClient.Logger;
using DummyClient.Network;
using Newtonsoft.Json;

namespace DummyClient.Scenario
{
    class ScenarioParser
    {
        public static ScenarioParser Instance { get; } = new ScenarioParser();

        public string _path;

        public bool Parse()
        {
            try
            {
                using (var file = File.OpenText(_path))
                {
                    var serializer = new JsonSerializer();
                    var scenarioData = (ScenarioData) serializer.Deserialize(file, typeof(ScenarioData));

                    using (var validator = new ScenarioValidator())
                    {
                        if (!validator.ValidateData(scenarioData))
                        {
                            return false;
                        }
                    }

                    SessionManager.Instance._scenarioData = scenarioData;

                    return true;
                }
            }
            catch (Exception e)
            {
                DCLogger.Instance.LogError("Cannot Open File : {0}", e.ToString());
                MessageBox.Show("Cannot Open File", "Error");

                return false;
            }
        }
    }
}
