﻿using System;
using DummyClient.Logger;

namespace DummyClient.Scenario
{
    internal class ScenarioValidator : IDisposable
    {
        private bool disposed; // to detect redundant calls

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Dispose managed resources.
                }

                // There are no unmanaged resources to release, but
                // if we add them, they need to be released here.
                disposed = true;
            }
        }

        public bool ValidateData(ScenarioData data)
        {
            if (data.Name == "DefaultScenarioName")
            {
                DCLogger.Instance.LogWarn("Scenario name is default");
            }

            const string regex = @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b";
            if (!System.Text.RegularExpressions.Regex.IsMatch(data.ServerIp, regex))
            {
                DCLogger.Instance.LogError("Invalid Data : ServerIp : {0}", data.ServerIp);
                return false;
            }

            if (data.ServerPort <= 0)
            {
                DCLogger.Instance.LogError("Invalid Data : ServerPort : {0}", data.ServerPort);
                return false;
            }

            if (data.ClientNumber <= 0)
            {
                DCLogger.Instance.LogError("Invalid Data : ClientNumber : {0}", data.ClientNumber);
                return false;
            }

            if (data.Tasks.Count == 0)
            {
                DCLogger.Instance.LogError("Invalid Data : No Task");
                return false;
            }

            foreach (var task in data.Tasks)
            {
                if (task.Name.Length == 0)
                {
                    DCLogger.Instance.LogError("Invalid Data : Invalid Task Name");
                    return false;
                }

                if (task.Count <= 0)
                {
                    DCLogger.Instance.LogError("Invalid Data : {0} - Count : {1}",
                        task.Name, task.Count);
                }
            }

            return true;
        }
    }
}
