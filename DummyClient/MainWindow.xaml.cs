﻿using System.Windows;
using DummyClient.Logger;
using DummyClient.Network;
using DummyClient.Scenario;

namespace DummyClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            UIGlobal.MainWaindow = this;
        }

        private void RunButton_Click(object sender, RoutedEventArgs e)
        {
            if (OpenFileNameTextBox.Text == "Please Select File")
            {
                DCLogger.Instance.LogError("Please Select Scenario File");
                return;
            }

            if (SessionManager.Instance._state == SessionManager.State.Stop)
            {
                RunButton.Opacity = 0.3;
                SessionManager.Instance.Clear();
                if (!ScenarioParser.Instance.Parse())
                {
                    RunButton.Opacity = 1;
                    return;
                }

                if (!SessionManager.Instance.CreateAndRunSessionByThread())
                {
                    RunButton.Opacity = 1;
                }
            }
            else
            {
                DCLogger.Instance.LogError("DummyClient is already running");
            }
        }

        private void OpenFileButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = ".json",
                Filter = "JSON Files (*.json)|*.json"
            };

            var result = dialog.ShowDialog();

            if (true == result)
            {
                OpenFileNameTextBox.Text = dialog.FileName;
                ScenarioParser.Instance._path = OpenFileNameTextBox.Text;
            }
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            if (SessionManager.Instance._state == SessionManager.State.Running)
            {
                SessionManager.Instance.StopAllThreads();
            }
            else
            {
                DCLogger.Instance.LogError("DummyClient is not running");
            }
        }
    }

    public static class UIGlobal
    {
        public static MainWindow MainWaindow { get; set; }
    }
}
