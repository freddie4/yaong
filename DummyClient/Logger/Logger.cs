﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace DummyClient.Logger
{
    class DCLogger
    {
        public static DCLogger Instance { get; } = new DCLogger();
        public static ListBox _listbox;

        private object _lock = new object();

        public enum Level
        {
            Info,
            Warn,
            Error
        }

        public DCLogger()
        {
            _listbox = UIGlobal.MainWaindow.LogListBox;
        }

        public string Format(Level level, string log)
        {
            var dateTime = DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss");
            return $"[{dateTime} {level.ToString()}] {log}";
        }

        public void Log(Level level, Brush color, string log, params object[] args)
        {
            var line = Format(level, string.Format(log, args));

            lock (_lock)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                new Action(() =>
                {
                    var item = new TextBlock
                    {
                        Text = line,
                        Foreground = color
                    };

                    _listbox.Items.Add(item);
                    _listbox.ScrollIntoView(_listbox.Items[_listbox.Items.Count - 1]);
                }));
            }
        }

        public void LogInfo(string log, params object[] args)
        {
            Log(Level.Info, Brushes.Black, log, args);
        }

        public void LogError(string log, params object[] args)
        {
            Log(Level.Error, Brushes.Red, log, args);
        }

        public void LogWarn(string log, params object[] args)
        {
            Log(Level.Info, Brushes.DarkOrange, log, args);
        }
    }
}
