﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DummyClient.Math
{
    struct FVector
    {
        public float X;
        public float Y;
        public float Z;

        public FVector(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
}
