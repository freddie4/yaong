#pragma once
#pragma warning(disable:4091)

#include <Windows.h>
#include <DbgHelp.h>
#include <string>
#include <ctime>
#include <thread>
#include <csignal>

namespace yaong
{
	namespace utility
	{
		using MiniDumpWriteDumpPtr = decltype(&MiniDumpWriteDump);

		class MiniDump
		{
		public:
			MiniDump();
			~MiniDump() = default;

			bool Initialize();

		private:
			long WriteMiniDump(PEXCEPTION_POINTERS exceptionPtr);

			static long WINAPI HandleException(PEXCEPTION_POINTERS exceptionInfo);
			static void PureCall();
			static void Sigabrt(int);

		private:
			static MiniDump* _pMiniDump;
			const std::string _path = ".\\Dump\\";
			const std::string _dumpName = "minidump";
			MiniDumpWriteDumpPtr _pMiniDumpWriteDump = nullptr;
		};
	}
}
