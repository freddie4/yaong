#include "stdafx.h"
#include "MemoryLeakDetector.h"

namespace yaong
{
	namespace utility
	{
		MemoryLeakDetector::MemoryLeakDetector()
		{
			_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
		}

		MemoryLeakDetector::~MemoryLeakDetector()
		{
		}

		void MemoryLeakDetector::CrtDumpMemoryLeaks()
		{
			// Call Constructor or this function

			_CrtDumpMemoryLeaks();
		}
	}
}