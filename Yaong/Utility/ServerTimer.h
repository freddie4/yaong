#pragma once

#include <chrono>
#include <Utility/Singleton.h>
#include "Timer.h"

namespace yaong
{
	namespace utility
	{
		class ServerTimer : public Timer
		{
			friend class Singleton<ServerTimer>;
		public:
			ServerTimer();
			~ServerTimer();
			
		private:
		};
	}
}

#define gServerTimer yaong::utility::Singleton<ServerTimer>::Instance()