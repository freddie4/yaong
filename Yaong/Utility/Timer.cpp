#include "stdafx.h"

#include "Timer.h"
#include <Utility/Logger/Logger.h>

namespace yaong
{
	namespace utility
	{
		Timer::Timer()
		{
		}

		Timer::~Timer()
		{
		}

		void Timer::StartTimer()
		{
			if(_isRunning)
			{
				LOG_F("Timer is already running!");
				return;
			}

			_startTime = std::chrono::system_clock::now();

			_isRunning = true;
		}

		void Timer::StopTimer()
		{
			if(!_isRunning)
			{
				LOG_F("Timer is not Running!");
				return;
			}

			_endTime = std::chrono::system_clock::now();

			_elapsedTime = SysTimePoint(_endTime - _startTime);
		}

		void Timer::ResetTimer()
		{
			const SysTimePoint newTimePoint;

			_startTime = newTimePoint;
			_endTime = newTimePoint;
			_elapsedTime = newTimePoint;

			_isRunning = false;
		}

		SysTimePoint Timer::GetElapsedTimePoint() const
		{
			return _elapsedTime;
		}

		double Timer::GetElapsedTimeMilisec() const
		{
			if(!_isRunning)
			{
				const auto time = std::chrono::time_point_cast<std::chrono::milliseconds>(_elapsedTime);
				return static_cast<double>(time.time_since_epoch().count());
			}

			const auto now = std::chrono::system_clock::now();
			const auto elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(now - _startTime);
			return static_cast<double>(elapsedTime.count());
		}

		double Timer::GetElapsedTimeSec() const
		{
			if (!_isRunning)
			{
				const auto time = std::chrono::time_point_cast<std::chrono::seconds>(_elapsedTime);
				return static_cast<double>(time.time_since_epoch().count());
			}

			const auto now = std::chrono::system_clock::now();
			const auto elapsedTime = std::chrono::duration_cast<std::chrono::seconds>(now - _startTime);
			return static_cast<double>(elapsedTime.count());
		}

		double Timer::GetElapsedTimeMin() const
		{
			if (!_isRunning)
			{
				const auto time = std::chrono::time_point_cast<std::chrono::minutes>(_elapsedTime);
				return static_cast<double>(time.time_since_epoch().count());
			}

			const auto now = std::chrono::system_clock::now();
			const auto elapsedTime = std::chrono::duration_cast<std::chrono::minutes>(now - _startTime);
			return static_cast<double>(elapsedTime.count());
		}

		double Timer::GetElapsedTimeHour() const
		{
			if (!_isRunning)
			{
				const auto time = std::chrono::time_point_cast<std::chrono::hours>(_elapsedTime);
				return static_cast<double>(time.time_since_epoch().count());
			}

			const auto now = std::chrono::system_clock::now();
			const auto elapsedTime = std::chrono::duration_cast<std::chrono::hours>(now - _startTime);
			return static_cast<double>(elapsedTime.count());
		}
	}
}

