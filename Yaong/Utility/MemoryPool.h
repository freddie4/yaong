#pragma once

namespace yaong
{
	namespace utility
	{
		template <typename Class, size_t BlockSize = 4096>
		class MemoryPool
		{
		public:
			Class* Construct() const
			{
				return new Class;
			}

			void Destruct(Class* const obj) const
			{
				delete obj;
			}
		};
	}
}