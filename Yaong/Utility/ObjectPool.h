#pragma once
#include <string>
#include <mutex>

namespace yaong
{
	namespace utility
	{
		using Byte = unsigned char;

		template <typename Class, int AllocCount>
		class ObjectPool
		{
		public:
			void* operator new(const size_t size)
			{
				static_assert(sizeof(Class) >= sizeof(unsigned char*), "Invalid Class size");
				static_assert(std::is_polymorphic<Class>::value == false, "Class is derived object");

				if(size <= 0)
				{
					throw std::runtime_error("new : alloc size <= 0");
				}

				std::lock_guard<std::mutex> lock(_mutex);

				if(nullptr == _memoryPool)
				{
					AllocateMemory();
				}
				
				const auto objPtr = _memoryPool;
				_memoryPool = *reinterpret_cast<Byte**>(objPtr);

				++_currentUsingCount;

				return objPtr;
			}

			void operator delete(void* ptr)
			{
				std::lock_guard<std::mutex> lock(_mutex);

				--_currentUsingCount;

				*reinterpret_cast<Byte**>(ptr) = _memoryPool;
				_memoryPool = static_cast<Byte*>(ptr);
			}

		protected:
			ObjectPool() = default;
			virtual ~ObjectPool() = default;

		private:
			static void AllocateMemory()
			{
				_memoryPool = new Byte[sizeof(Class) * AllocCount];

				Byte** currentPos = reinterpret_cast<Byte**>(_memoryPool);
				Byte* nextPos = _memoryPool;

				for(auto i = 0; i < AllocCount - 1; ++i)
				{
					nextPos += sizeof(Class);
					*currentPos = nextPos;
					currentPos = reinterpret_cast<Byte**>(nextPos);
				}

				*currentPos = nullptr;
				_poolSize += AllocCount;
			}

		private:
			static std::mutex _mutex;

			static Byte* _memoryPool = nullptr;
			static int _currentUsingCount = 0;
			static int _poolSize = 0;
		};
	}
}
