#pragma once
#include <string>
#include <memory>
#include <mutex>

namespace yaong
{
	namespace utility
	{
		template <typename Class, int AllocCount>
		class SharedPtrMemoryPool
		{
		public:
			std::shared_ptr<Class> make_shared()
			{
				static_assert(std::is_polymorphic<Class>::value == false, "Class is derived object");

				if(nullptr == _memoryPool)
				{
					AllocateMemory();
				}

				const auto objPtr = std::shared_ptr<Class>(_memoryPool, &_memoryPool[_currentUsingCount]);

				++_currentUsingCount;

				return objPtr;
			}

		protected:
			SharedPtrMemoryPool() = default;
			virtual ~SharedPtrMemoryPool() = default;

		private:
			static void AllocateMemory()
			{
				_memoryPool = std::make_shared<Class[]>(AllocCount);
			}

			static void Delete()
			{
				--_currentUsingCount;
			}

		private:
			static std::mutex _mutex;

			static std::shared_ptr<Class[]> _memoryPool = nullptr;
			static int _currentUsingCount = 0;
			static int _poolSize;
		};
	}
}