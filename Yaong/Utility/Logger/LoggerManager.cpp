#include "stdafx.h"
#include "LoggerManager.h"

#include "Logger.h"

namespace yaong::utility::cpplogger
{
	LoggerManager::LoggerManager()
	{
		LoggerInfo defaultLoggerInfo = { "default", Level::Debug, Format::Default,
			".\\log\\", "default_log", ".log", 100, RotationTime::Day };
		const auto defaultLogger = std::make_shared<Logger>(defaultLoggerInfo);
		_loggerMap[defaultLoggerInfo._loggerName] = defaultLogger;
	}

	LoggerManager::~LoggerManager()
	{
		Clear();
	}

	std::shared_ptr<Logger> LoggerManager::GetLogger(const std::string& loggerName = "default")
	{
		std::scoped_lock<std::mutex> lock(_mutex);

		const auto found = _loggerMap.find(loggerName);

		if(found == _loggerMap.end())
		{
			throw std::runtime_error("No Logger in LoggerMap");
		}

		return found->second;
	}

	void LoggerManager::Clear()
	{
		std::lock_guard<std::mutex> lock(_mutex);
		_loggerMap.clear();
	}
}
