#include "stdafx.h"
#include "FileController.h"

#include "LoggerUtils.h"
#include <direct.h>
#include <io.h>
#include <iomanip>
#include <sstream>
#include <string>

namespace yaong::utility::cpplogger
{
	FileController::FileController(std::string filePath, std::string fileName,
		std::string fileExtension, const size_t maxFileSize, const int fileCount)
		: _filePath(std::move(filePath))
		, _fileName(std::move(fileName))
		, _fileExtension(std::move(fileExtension))
		, _maxFileSize(maxFileSize)
		, _fileCount(fileCount)
	{
		if(::_mkdir(filePath.c_str()) == ENOENT)
		{
			throw std::runtime_error("Path not Found");
		}
	}

	FileController::~FileController()
	{
		CloseFile();
	}

	void FileController::OpenFile(std::string filePath)
	{
		CloseFile();

		if(::_sopen_s(&_fd, std::move(filePath).c_str(), OFLAG, SHFLAG, PMODE) != OPEN_SUCCESS)
		{
			throw std::runtime_error("OpenFile failed");
		}
	}

	void FileController::CloseFile()
	{
		if(_fd)
		{
			::_close(_fd);
			_fd = 0;
		}
	}

	void FileController::WriteToFile(const char* buf)
	{
		if(_isOpen == false)
		{
			OpenFile(MakeFilePath());
			_isOpen = true;
		}

		const auto result = ::_write(_fd, buf, static_cast<unsigned int>(::strlen(buf)));

		if(result == WRITE_ERROR)
		{
			throw std::runtime_error("WriteFile failed");
		}
		_currentFileSize += result;
	}

	std::string FileController::MakeFilePath() const
	{
		std::stringstream path;
		if(_fileCount == 0)
		{
			path << _filePath << _fileName << "_" << GetCurrentDateWithTime()
				<< "_" << _fileCount << _fileExtension;
		}
		else
		{
			path << _filePath << _fileName << "_" << GetCurrentDateWithTime()
				<< "_" << std::setw(2) << std::setfill('0') << _fileCount << _fileExtension;
		}
		return path.str();
	}
}

