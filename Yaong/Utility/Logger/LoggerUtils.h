#pragma once
#include <Utility/Format/format.h>
#include <ctime>

namespace yaong::utility::cpplogger
{
	constexpr int BYTE_TO_MB = 1024 * 1024;
	constexpr int MSG_SIZE = 1024;
	constexpr int PATH_SIZE = 1024;
	constexpr int DATE_BUF_SIZE = 20;

	inline std::string GetCurrentDateWithTime()
	{
		const auto now = ::time(nullptr);
		tm timeStruct{};
		::localtime_s(&timeStruct, &now);

		char buf[DATE_BUF_SIZE];

		::strftime(buf, sizeof buf, "%Y%m%d%H", &timeStruct);
		return buf;
	}

	template <size_t BufferSize>
	auto ToStringView(const fmt::basic_memory_buffer<char, BufferSize>& buf) noexcept
	{
		return std::string_view(buf.data(), buf.size());
	}

	inline void ToUTF8Buf(const fmt::wmemory_buffer& wbuf, fmt::memory_buffer& target)
	{
		const auto wbufSize = static_cast<int>(wbuf.size());
		if(wbufSize == 0)
		{
			return;
		}

		std::wstring ws(wbuf.data());
		std::string str(ws.begin(), ws.end());
		target.push_back(*str.data());
	}
}
