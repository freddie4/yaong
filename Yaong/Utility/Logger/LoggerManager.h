#pragma once
#include <mutex>
#include <unordered_map>

namespace yaong::utility::cpplogger
{
	struct LoggerInfo;
	class Logger;

	using LoggerMap = std::unordered_map<std::string, std::shared_ptr<Logger>>;

	class LoggerManager
	{
	public:
		LoggerManager(const LoggerManager&) = delete;
		LoggerManager& operator=(const LoggerManager&) = delete;
		LoggerManager(LoggerManager&&) = delete;
		LoggerManager& operator=(LoggerManager&&) = delete;

		static LoggerManager& GetInstance()
		{
			static LoggerManager instance;
			return instance;
		}

		std::shared_ptr<Logger> GetLogger(const std::string& loggerName);

	private:
		LoggerManager();
		~LoggerManager();

		void Clear();

		std::mutex _mutex;
		LoggerMap _loggerMap;
	};
}
