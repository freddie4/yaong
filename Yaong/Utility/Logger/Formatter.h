#pragma once

namespace yaong::utility::cpplogger
{
	class Formatter abstract
	{
	public:
		Formatter() = default;
		virtual ~Formatter() = default;

		Formatter(const Formatter&) = delete;
		Formatter& operator=(const Formatter&) = delete;
		Formatter(Formatter&&) = delete;
		Formatter& operator=(Formatter&&) = delete;

		virtual void FormatMessage(char* buf, const size_t bufSize,
			const std::string_view message) = 0;

	protected:
		time_t _tt;
		struct tm _timeStructLocal;
		struct timespec _timeStructSpecLocal;
		struct tm _timeStructUTC;
	};

	// DefaultFormatter : "[YYYY-MM-DD HH:mm:ss:xxx]+-UTC diff(HH:00), message"
	class DefaultFormatter : public Formatter
	{
	public:
		DefaultFormatter() = default;
		virtual ~DefaultFormatter() = default;

	private:
		void FormatMessage(char* buf, const size_t bufSize, const std::string_view message) override
		{
			_tt = ::time(nullptr);
			::gmtime_s(&_timeStructUTC, &_tt);
			::localtime_s(&_timeStructLocal, &_tt);
			::timespec_get(&_timeStructSpecLocal, TIME_UTC);
			const auto ulhour = _timeStructLocal.tm_hour - _timeStructUTC.tm_hour;

			::sprintf_s(buf, bufSize, "[%04u-%02u-%02u %02u:%02u:%02u.%03u%+03d:00], %s\n",
				_timeStructLocal.tm_year + 1900, _timeStructLocal.tm_mon + 1, _timeStructLocal.tm_mday,
				_timeStructLocal.tm_hour, _timeStructLocal.tm_min, _timeStructLocal.tm_sec,
				_timeStructSpecLocal.tv_nsec / 1000000, ulhour, message.data());
		}
	};

	enum class Format;
	using FormatterConstructor = std::function<std::unique_ptr<Formatter>()>;
	using FormatterMap = std::unordered_map<Format, FormatterConstructor>;

	class FormatterFactory
	{
	public:
		FormatterFactory(const FormatterFactory&) = delete;
		FormatterFactory& operator=(const FormatterFactory&) = delete;
		FormatterFactory(FormatterFactory&&) = delete;
		FormatterFactory& operator=(FormatterFactory&&) = delete;

		static FormatterFactory& GetInstance()
		{
			static FormatterFactory instance;
			return instance;
		}

		auto GetFormatter(const Format format)
		{
			const auto found = _formatterMap.find(format);
			if(found == _formatterMap.end())
			{
				throw std::runtime_error("FormatFactory error : No Class");
			}

			return found->second();
		}

		void RegisterFormatterConstructor(const Format format, const FormatterConstructor& func)
		{
			_formatterMap[format] = func;
		}

	private:
		FormatterFactory() = default;
		~FormatterFactory() = default;

	private:
		FormatterMap _formatterMap;
	};

	template <typename FormatterClassType>
	class FormatterRegistrar
	{
	public:
		explicit FormatterRegistrar(const Format format)
		{
			auto constructor = []() -> std::unique_ptr<Formatter>
			{
				return std::make_unique<FormatterClassType>();
			};
			FormatterFactory::GetInstance().RegisterFormatterConstructor(format, constructor);
		}
		~FormatterRegistrar() = default;

		FormatterRegistrar(const FormatterRegistrar&) = delete;
		FormatterRegistrar& operator=(const FormatterRegistrar&) = delete;
		FormatterRegistrar(FormatterRegistrar&&) = delete;
		FormatterRegistrar& operator=(FormatterRegistrar&&) = delete;
	};
}