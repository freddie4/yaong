#pragma once
#include "LoggerManager.h"
#include "LoggerUtils.h"
#include <Utility/Format/format.h>

namespace yaong::utility::cpplogger
{
	enum class Level
	{
		Debug = 0,
		Notice = 1,
		Warn = 2,
		Error = 3,
		Crit = 4
	};

	enum class RotationTime
	{
		None = 0,
		Hour = 1,
		Day = 2,
		Month = 3
	};

	enum class Format
	{
		Default = 0
	};

	struct LoggerInfo
	{
		std::string _loggerName;
		Level _level;
		Format _format;
		std::string _filePath;
		std::string _fileName;
		std::string _fileExtension;
		int _fileSize; // MB
		RotationTime _rotationTime;
	};

	using TimePoint = std::chrono::system_clock::time_point;
	class FileController;
	class Formatter;

	class Logger
	{
	public:
		explicit Logger(const LoggerInfo& loggerInfo);
		virtual ~Logger();

		Logger(const Logger&) = delete;
		Logger& operator=(const Logger&) = delete;
		Logger(Logger&&) = delete;
		Logger& operator=(Logger&&) = delete;

		template <typename ...Args>
		void Debug(const char* stringFormat, const Args& ...args);
		template <typename ...Args>
		void Notice(const char* stringFormat, const Args& ...args);
		template <typename ...Args>
		void Warn(const char* stringFormat, const Args& ...args);
		template <typename ...Args>
		void Error(const char* stringFormat, const Args& ...args);
		template <typename ...Args>
		void Crit(const char* stringFormat, const Args& ...args);

		template <typename ...Args>
		void Debug(const wchar_t* stringFormat, const Args& ...args);
		template <typename ...Args>
		void Notice(const wchar_t* stringFormat, const Args& ...args);
		template <typename ...Args>
		void Warn(const wchar_t* stringFormat, const Args& ...args);
		template <typename ...Args>
		void Error(const wchar_t* stringFormat, const Args& ...args);
		template <typename ...Args>
		void Crit(const wchar_t* stringFormat, const Args& ...args);

		void Log(Level level, const std::string_view message);

	private:
		bool ShouldLog(const Level level) const { return level <= _level; }
		bool Rotate();
		void ResetFileController();

		TimePoint NextRotationTp();

		const std::string _loggerName;
		const Level _level;
		const RotationTime _rotationTime;

		std::unique_ptr<FileController> _fileController;
		std::unique_ptr<Formatter> _formatter;

		bool _isValid = false;

		TimePoint _nextRotationTp;

		std::mutex _mutex;
	};

	template <typename ...Args>
	void Logger::Debug(const char* stringFormat, const Args& ...args)
	{
		if(_isValid)
		{
			if(ShouldLog(Level::Debug))
			{
				fmt::memory_buffer buf;
				fmt::format_to(buf, stringFormat, args...);
				Log(Level::Debug, ToStringView(buf));
			}
		}
	}

	template <typename ...Args>
	void Logger::Notice(const char* stringFormat, const Args& ...args)
	{
		if(_isValid)
		{
			if(ShouldLog(Level::Notice))
			{
				fmt::memory_buffer buf;
				fmt::format_to(buf, stringFormat, args...);
				Log(Level::Notice, ToStringView(buf));
			}
		}
	}

	template <typename ...Args>
	void Logger::Warn(const char* stringFormat, const Args& ...args)
	{
		if(_isValid)
		{
			if(ShouldLog(Level::Warn))
			{
				fmt::memory_buffer buf;
				fmt::format_to(buf, stringFormat, args...);
				Log(Level::Warn, ToStringView(buf));
			}
		}
	}

	template <typename ...Args>
	void Logger::Crit(const char* stringFormat, const Args& ...args)
	{
		if(_isValid)
		{
			if(ShouldLog(Level::Crit))
			{
				fmt::memory_buffer buf;
				fmt::format_to(buf, stringFormat, args...);
				Log(Level::Crit, ToStringView(buf));
			}
		}
	}

	template <typename ...Args>
	void Logger::Error(const char* stringFormat, const Args& ...args)
	{
		if(_isValid)
		{
			if(ShouldLog(Level::Error))
			{
				fmt::memory_buffer buf;
				fmt::format_to(buf, stringFormat, args...);
				Log(Level::Error, ToStringView(buf));
			}
		}
	}

	template <typename ...Args>
	void Logger::Debug(const wchar_t* stringFormat, const Args& ...args)
	{
		if(_isValid)
		{
			if(ShouldLog(Level::Debug))
			{
				fmt::wmemory_buffer wbuf;
				fmt::format_to(wbuf, stringFormat, args...);
				fmt::memory_buffer buf;
				ToUTF8Buf(wbuf, buf);
				Log(Level::Debug, ToStringView(buf));
			}
		}
	}

	template <typename ...Args>
	void Logger::Notice(const wchar_t* stringFormat, const Args& ...args)
	{
		if(_isValid)
		{
			if(ShouldLog(Level::Notice))
			{
				fmt::wmemory_buffer wbuf;
				fmt::format_to(wbuf, stringFormat, args...);
				fmt::memory_buffer buf;
				ToUTF8Buf(wbuf, buf);
				Log(Level::Notice, ToStringView(buf));
			}
		}
	}

	template <typename ...Args>
	void Logger::Warn(const wchar_t* stringFormat, const Args& ...args)
	{
		if(_isValid)
		{
			if(ShouldLog(Level::Warn))
			{
				fmt::wmemory_buffer wbuf;
				fmt::format_to(wbuf, stringFormat, args...);
				fmt::memory_buffer buf;
				ToUTF8Buf(wbuf, buf);
				Log(Level::Warn, ToStringView(buf));
			}
		}
	}

	template <typename ...Args>
	void Logger::Crit(const wchar_t* stringFormat, const Args& ...args)
	{
		if(_isValid)
		{
			if(ShouldLog(Level::Crit))
			{
				fmt::wmemory_buffer wbuf;
				fmt::format_to(wbuf, stringFormat, args...);
				fmt::memory_buffer buf;
				ToUTF8Buf(wbuf, buf);
				Log(Level::Crit, ToStringView(buf));
			}
		}
	}

	template <typename ...Args>
	void Logger::Error(const wchar_t* stringFormat, const Args& ...args)
	{
		if(_isValid)
		{
			if(ShouldLog(Level::Error))
			{
				fmt::wmemory_buffer wbuf;
				fmt::format_to(wbuf, stringFormat, args...);
				fmt::memory_buffer buf;
				ToUTF8Buf(wbuf, buf);
				Log(Level::Error, ToStringView(buf));
			}
		}
	}
}

#define LOG_(message, ...) \
	yaong::utility::cpplogger::LoggerManager::GetInstance().GetLogger("default")->Debug(message, __VA_ARGS__);

#define LOG_D(loggerName, message, ...) \
	yaong::utility::cpplogger::LoggerManager::GetInstance().GetLogger(loggerName)->Debug(message, __VA_ARGS__);

#define LOG_N(loggerName, message, ...) \
	yaong::utility::cpplogger::LoggerManager::GetInstance().GetLogger(loggerName)->Notice(message, __VA_ARGS__);

#define LOG_W(loggerName, message, ...) \
	yaong::utility::cpplogger::LoggerManager::GetInstance().GetLogger(loggerName)->Warn(message, __VA_ARGS__);

#define LOG_E(loggerName, message, ...) \
	yaong::utility::cpplogger::LoggerManager::GetInstance().GetLogger(loggerName)->Error(message, __VA_ARGS__);

#define LOG_C(loggerName, message, ...) \
	yaong::utility::cpplogger::LoggerManager::GetInstance().GetLogger(loggerName)->Crit(message, __VA_ARGS__);

#define LOG_F(message, ...) \
	LOG_("[{}] " message, __FUNCTION__, __VA_ARGS__);

#define LOG_D_F(loggerName, message, ...) \
	LOG_D(loggerName, "[{}] " message, __FUNCTION__, __VA_ARGS__);

#define LOG_N_F(loggerName, message, ...) \
	LOG_N(loggerName, "[{}] " message, __FUNCTION__, __VA_ARGS__);

#define LOG_W_F(loggerName, message, ...) \
	LOG_W(loggerName, "[{}] " message, __FUNCTION__, __VA_ARGS__);

#define LOG_E_F(loggerName, message, ...) \
	LOG_E(loggerName, "[{}] " message, __FUNCTION__, __VA_ARGS__);

#define LOG_C_F(loggerName, message, ...) \
	LOG_C(loggerName, "[{}] " message, __FUNCTION__, __VA_ARGS__);
