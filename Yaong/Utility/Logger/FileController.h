#pragma once
#include <fcntl.h>
#include <string>

namespace yaong::utility::cpplogger
{
	class FileController
	{
	public:
		FileController(std::string filePath, std::string fileName,
			std::string fileExtension, const size_t maxFileSize, const int fileCount);
		~FileController();

		FileController(const FileController&) = delete;
		FileController& operator=(const FileController&) = delete;
		FileController(FileController&&) = delete;
		FileController& operator=(FileController&&) = delete;

		size_t GetCurrentFileSize() const { return _currentFileSize; }
		size_t GetMaxFileSize() const { return _maxFileSize; }
		std::string GetFileName() const { return _fileName; }
		std::string GetFilePath() const { return _filePath; }
		std::string GetFileExtension() const { return _fileExtension; }
		int GetFileCount() const { return _fileCount; }
		void SetFileCount(const int fileCount) { _fileCount = fileCount; }
		void IncreaseFileCount() { _fileCount++; }

		void WriteToFile(const char* buf);

	private:
		void OpenFile(std::string filePath);
		void CloseFile();

		std::string MakeFilePath() const;

		const std::string _filePath;
		const std::string _fileName;
		const std::string _fileExtension;
		const size_t _maxFileSize;

		int _fileCount = 0;

		size_t _currentFileSize = 0;

		int _fd = 0;

		bool _isOpen = false;

		static const int OFLAG = _O_WRONLY | _O_CREAT | _O_APPEND;
		static const int SHFLAG = _SH_DENYNO;
		static const int PMODE = _S_IREAD | _S_IWRITE;

		static const int OPEN_SUCCESS = 0;
		static const int WRITE_ERROR = -1;
	};
}

