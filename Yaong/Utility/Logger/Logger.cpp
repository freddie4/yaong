#include "stdafx.h"
#include "Logger.h"

#include "FileController.h"
#include "Formatter.h"

#define LOG_CONSOLE

namespace yaong::utility::cpplogger
{
	Logger::Logger(const LoggerInfo& loggerInfo)
		: _loggerName(loggerInfo._loggerName)
		, _level(loggerInfo._level)
		, _rotationTime(loggerInfo._rotationTime)
		, _fileController(std::make_unique<FileController>(loggerInfo._filePath,
			loggerInfo._fileName, loggerInfo._fileExtension, loggerInfo._fileSize, 0))
		, _formatter(FormatterFactory::GetInstance().GetFormatter(loggerInfo._format))
		, _isValid(true)
		, _nextRotationTp(NextRotationTp())
	{
	}

	Logger::~Logger() = default;

	void Logger::Log(Level level, const std::string_view message)
	{
		char buf[cpplogger::MSG_SIZE];

		std::scoped_lock<std::mutex> lock(_mutex);
		try
		{
			Rotate();
			_formatter->FormatMessage(buf, cpplogger::MSG_SIZE, message);

#ifdef LOG_CONSOLE
			std::cout << buf;
#endif
			_fileController->WriteToFile(buf);
		}
		catch(const std::runtime_error&)
		{
			_isValid = false;
		}
	}

	void Logger::ResetFileController()
	{
		_fileController.reset(new FileController(
			_fileController->GetFilePath()
			, _fileController->GetFileName()
			, _fileController->GetFileExtension()
			, _fileController->GetMaxFileSize()
			, _fileController->GetFileCount()));
	}

	bool Logger::Rotate()
	{
		if(_fileController->GetCurrentFileSize() >= _fileController->GetMaxFileSize() * BYTE_TO_MB)
		{
			ResetFileController();
			_fileController->IncreaseFileCount();
			return true;
		}

		if(_rotationTime != RotationTime::None)
		{
			if(std::chrono::system_clock::now() >= _nextRotationTp)
			{
				_nextRotationTp = NextRotationTp();
				ResetFileController();
				_fileController->SetFileCount(0);
				return true;
			}
		}
		return false;
	}

	TimePoint Logger::NextRotationTp()
	{
		if(_rotationTime == RotationTime::None)
		{
			return TimePoint(std::chrono::seconds(0));
		}

		const auto now = std::chrono::system_clock::now();
		const auto tnow = std::chrono::system_clock::to_time_t(now);

		tm timeStruct{};
		::localtime_s(&timeStruct, &tnow);

		switch(_rotationTime)
		{
		case RotationTime::Hour:
		{
			timeStruct.tm_min = 0;
			timeStruct.tm_sec = 0;
			const auto rotationTime = std::chrono::system_clock::from_time_t(std::mktime(&timeStruct));
			return TimePoint{ rotationTime + std::chrono::hours(1) };
		}
		case RotationTime::Day:
		{
			timeStruct.tm_hour = 0;
			timeStruct.tm_min = 0;
			timeStruct.tm_sec = 0;
			const auto rotationTime = std::chrono::system_clock::from_time_t(std::mktime(&timeStruct));
			return TimePoint{ rotationTime + std::chrono::hours(24) };
		}
		case RotationTime::Month:
		{
			const auto nowMonth = timeStruct.tm_mon + 1;
			timeStruct.tm_mday = 0;
			timeStruct.tm_hour = 0;
			timeStruct.tm_min = 0;
			timeStruct.tm_sec = 0;
			const auto rotationTime = std::chrono::system_clock::from_time_t(std::mktime(&timeStruct));

			if(nowMonth == 1 || nowMonth == 3 || nowMonth == 5 || nowMonth == 7 || nowMonth == 8 || nowMonth == 10 || nowMonth == 12)
			{
				return TimePoint{ rotationTime + std::chrono::hours(24 * 31) };
			}
			if(nowMonth == 2)
			{
				return TimePoint{ rotationTime + std::chrono::hours(24 * 28) };
			}

			return TimePoint{ rotationTime + std::chrono::hours(24 * 30) };
		}
		default:
			throw std::invalid_argument("Invalid RotationTime");
		}
	}

	// Register Formatter Class
	static FormatterRegistrar<DefaultFormatter> defaultFormatter(Format::Default);
}
