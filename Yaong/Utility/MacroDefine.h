#pragma once

namespace yaong
{
	#define FROM_ENUM( enumValue ) \
		static_cast<std::underlying_type<typename std::remove_reference<decltype(enumValue)>::type>::type>(enumValue)
	#define TO_ENUM( enumType, value ) \
		static_cast<enumType>(value)

	#define DISABLE_COPYMOVE( ClassName )	\
			ClassName(const ClassName&) = delete;	\
			ClassName( const ClassName&& ) = delete;	\
			ClassName& operator =(const ClassName&) = delete;	\
			ClassName& operator =( const ClassName&& ) = delete;

	#define OUT
}