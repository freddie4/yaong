#pragma once

#include <chrono>

namespace yaong
{
	namespace utility
	{
		using SysTimePoint = std::chrono::time_point<std::chrono::system_clock>;
		using StartTime = SysTimePoint;

		class Timer
		{
		public:
			Timer();
			~Timer();

			void StartTimer();
			void StopTimer();
			void ResetTimer();

			SysTimePoint GetElapsedTimePoint() const;
			double GetElapsedTimeMilisec() const;
			double GetElapsedTimeSec() const;
			double GetElapsedTimeMin() const;
			double GetElapsedTimeHour() const;

		private:
			SysTimePoint _startTime;
			SysTimePoint _endTime;
			SysTimePoint _elapsedTime;

			bool _isRunning = false;
		};
	}
}