#include "ProcessMonitor.h"
#include <Utility/Logger/Logger.h>

namespace yaong
{
	namespace utility
	{
		bool ProcessMonitor::Initialize()
		{
			PdhOpenQuery(nullptr, NULL, &_cpuQuery);
			PdhAddCounter(_cpuQuery, _cpuUsageQuery.data(), NULL, &_cpuTotalCounter);
			::PdhCollectQueryData(_cpuQuery);

			return true;
		}

		void ProcessMonitor::Finalize()
		{
		}

		void ProcessMonitor::BeginProfileTimer()
		{
		}

		void ProcessMonitor::PrintCpuUsage()
		{
			LOG_("[CPU Usage] : {}", GetCurrentCpuUsage());
		}

		void ProcessMonitor::PrintMemoryUsage()
		{
			LOG_("[Memory Usage] : TotalPhysicalMemory Usage : {}", GetTotalPhysicalMemory());
			LOG_("[Memory Usage] : TotalVirtualMemory Usage : {}", GetTotalVirtualMemory());
			LOG_("[Memory Usage] : CurrentVirtualMemory Usage : {}", GetCurrentVirtualMemoryUsage());
		}

		bool ProcessMonitor::GetMemoryStatus()
		{
			const auto result = ::GlobalMemoryStatusEx(&_memoryInfo);
			if (!result)
			{
				LOG_F("Failed to init Memory Status");
				return false;
			}

			return true;
		}

		double ProcessMonitor::GetCurrentCpuUsage()
		{
			PDH_FMT_COUNTERVALUE counterValue;

			::PdhCollectQueryData(_cpuQuery);
			::PdhGetFormattedCounterValue(_cpuTotalCounter, PDH_FMT_DOUBLE, nullptr, &counterValue);

			return counterValue.doubleValue;
		}

		bool ProcessMonitor::_GetProcessMemoryInfo()
		{
			const auto pCounter = reinterpret_cast<PPROCESS_MEMORY_COUNTERS>(&_processCounters);
			const auto complete = ::GetProcessMemoryInfo(GetCurrentProcess(), pCounter, sizeof(_processCounters));

			if (FALSE == complete)
			{
				LOG_F(L"Failed to Initialize GetProcessMemoryInfo");
				return false;
			}

			return true;
		}

		unsigned long long ProcessMonitor::GetTotalPhysicalMemory()
		{
			if(0 >= _totalPhysicalMemory)
			{
				const auto result = static_cast<double>(_memoryInfo.ullTotalPhys) / GB;

				_totalPhysicalMemory = ::llround(result);
			}

			return _totalPhysicalMemory;
		}

		unsigned long long ProcessMonitor::GetTotalVirtualMemory()
		{
			GetMemoryStatus();
			return _memoryInfo.ullAvailVirtual;
		}

		unsigned long long ProcessMonitor::GetCurrentVirtualMemoryUsage()
		{
			GetMemoryStatus();
			_GetProcessMemoryInfo();
			return _processCounters.PrivateUsage;
		}
	}
}