#include "stdafx.h"
#include "DateTime.h"

namespace yaong
{
	namespace utility
	{
		DateTime::DateTime()
		{
		}

		DateTime::~DateTime()
		{
		}

		int DateTime::GetCurrentYear()
		{
			time_t now = ::time(nullptr);
			struct tm timeStruct {};
			::localtime_s(&timeStruct, &now);

			return timeStruct.tm_year + TIME_YEAR_CORRECTION;
		}

		int DateTime::GetCurrentMonth()
		{
			time_t now = ::time(nullptr);
			struct tm timeStruct {};
			::localtime_s(&timeStruct, &now);

			return timeStruct.tm_mon + TIME_MONTH_CORRECTION;
		}

		int DateTime::GetCurrentDays()
		{
			time_t now = ::time(nullptr);
			struct tm timeStruct {};
			::localtime_s(&timeStruct, &now);

			return timeStruct.tm_mday;
		}

		int DateTime::GetCurrentTimeHour()
		{
			time_t now = ::time(nullptr);
			struct tm timeStruct {};
			::localtime_s(&timeStruct, &now);

			return timeStruct.tm_hour;
		}

		int DateTime::GetCurrentTimeMin()
		{
			time_t now = ::time(nullptr);
			struct tm timeStruct {};
			::localtime_s(&timeStruct, &now);

			return timeStruct.tm_hour;
		}

		int DateTime::GetCurrentTimeSec()
		{
			time_t now = ::time(nullptr);
			struct tm timeStruct {};
			::localtime_s(&timeStruct, &now);

			return timeStruct.tm_sec;
		}

		std::string DateTime::GetCurrentWeekDay()
		{
			time_t now = ::time(nullptr);
			struct tm timeStruct {};
			::localtime_s(&timeStruct, &now);

			switch (timeStruct.tm_wday)
			{
			case 0:
				return "Sun";
			case 1:
				return "Mon";
			case 2:
				return "Tue";
			case 3:
				return "Wed";
			case 4:
				return "Thur";
			case 5:
				return "Fri";
			case 6:
				return "Sat";
			default:
				return "None";
			}
		}

		std::string DateTime::GetCurrentDateTimeString()
		{
			time_t now = ::time(nullptr);
			struct tm timeStruct {};
			::localtime_s(&timeStruct, &now);

			char buf[DATETIME_STRING_SIZE];

			::strftime(buf, sizeof(buf), "%Y%m%d_%H:%M:%S", &timeStruct);
			return buf;
		}
	}
}