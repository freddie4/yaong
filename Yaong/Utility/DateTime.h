#pragma once

#include <chrono>
#include <string>
#include <Utility/Singleton.h>

namespace yaong
{
	namespace utility
	{
		constexpr int TIME_YEAR_CORRECTION = 1900;
		constexpr int TIME_MONTH_CORRECTION = 1;
		constexpr int DATETIME_STRING_SIZE = 19;

		class DateTime
		{
			friend class Singleton<DateTime>;

		public:
			DateTime();
			~DateTime();

		public:
			int GetCurrentYear();
			int GetCurrentMonth();
			int GetCurrentDays();
			int GetCurrentTimeHour();
			int GetCurrentTimeMin();
			int GetCurrentTimeSec();

			std::string GetCurrentWeekDay();
			std::string GetCurrentDateTimeString();
		};
	}
}

#define gDateTime yaong::utility::Singleton<DateTime>::Instance()