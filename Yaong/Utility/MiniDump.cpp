#include "stdafx.h"

#include "MiniDump.h"
#include <Utility/Logger/Logger.h>

namespace yaong
{
	namespace utility
	{
		MiniDump* MiniDump::_pMiniDump = nullptr;

		MiniDump::MiniDump()
		{
		}

		bool MiniDump::Initialize()
		{
			const auto dbgHelp = LoadLibraryA("dbghelp.dll");
			if(nullptr == dbgHelp)
			{
				LOG_F("dbgHelp Load failed");
				return false;
			}

			_pMiniDumpWriteDump = reinterpret_cast<MiniDumpWriteDumpPtr>(GetProcAddress(dbgHelp, "MiniDumpWriteDump"));
			if (_pMiniDumpWriteDump == nullptr)
			{
				LOG_F("MiniDumpWriteDump init failed");
				return false;
			}

			::SetUnhandledExceptionFilter(HandleException);
			::_set_purecall_handler(PureCall);
			::signal(SIGABRT, Sigabrt);

			_pMiniDump = this;

			return true;
		}

		long MiniDump::WriteMiniDump( PEXCEPTION_POINTERS exceptionPtr )
		{
			const auto now = ::time(nullptr);
			tm timeStruct{};
			::localtime_s(&timeStruct, &now);

			char dumpFileName[_MAX_PATH] = { 0 };
			::sprintf_s(dumpFileName, "%s%s_%04d%02d%02d_%02d%02d%02d.dmp",
				_path.data(), _dumpName.data(), timeStruct.tm_year + 1900, timeStruct.tm_mon + 1,
				timeStruct.tm_mday, timeStruct.tm_hour, timeStruct.tm_min, timeStruct.tm_sec);

			const auto fileHandle = CreateFile(dumpFileName, GENERIC_WRITE, 
				FILE_SHARE_WRITE, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL,
				nullptr);

			if( INVALID_HANDLE_VALUE != fileHandle)
			{
				MINIDUMP_EXCEPTION_INFORMATION exceptionInfo{};

				exceptionInfo.ThreadId = ::GetCurrentThreadId();
				exceptionInfo.ExceptionPointers = exceptionPtr;
				exceptionInfo.ClientPointers = FALSE;

				const auto writeDumpResult = _pMiniDumpWriteDump(
					::GetCurrentProcess(),
					::GetCurrentProcessId(),
					fileHandle,
					MINIDUMP_TYPE(MiniDumpWithFullMemory | MiniDumpScanMemory),
					&exceptionInfo,
					nullptr,
					nullptr);

				if(!writeDumpResult)
				{
					LOG_F("Failed to write dump file");
				}

				::CloseHandle(fileHandle);
			}

			return EXCEPTION_CONTINUE_SEARCH;
		}

		long MiniDump::HandleException( PEXCEPTION_POINTERS exceptionInfo )
		{
			_pMiniDump->WriteMiniDump(exceptionInfo);
			return EXCEPTION_EXECUTE_HANDLER;
		}

		void MiniDump::PureCall()
		{
		}

		void MiniDump::Sigabrt( int )
		{
		}
	}
}
