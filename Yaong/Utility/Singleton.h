#pragma once

namespace yaong
{
	namespace utility
	{
		template <typename T>
		class Singleton final
		{
		protected:
			Singleton() = default;

		public:
			~Singleton() = default;

			static T* Instance(void)
			{
				if (nullptr == _pInstance)
				{
					static T instance;

					_pInstance = &instance;
				}

				return _pInstance;
			}

			static T& Object(void)
			{
				return *Instance();
			}

		public:
			Singleton(const Singleton&) = delete;
			Singleton& operator=(const Singleton&) = delete;
			Singleton(Singleton&&) = delete;
			Singleton&& operator=(Singleton&&) = delete;

		private:
			static T* _pInstance;
		};

		template <typename T>
		T* utility::Singleton<T>::_pInstance = nullptr;
	}
}