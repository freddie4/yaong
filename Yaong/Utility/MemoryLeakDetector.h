#pragma once
#include <stdlib.h>
#include <crtdbg.h>

#define _CRTDBG_MAP_ALLOC

namespace yaong
{
	namespace utility
	{
		class MemoryLeakDetector
		{
		public:
			MemoryLeakDetector();
			~MemoryLeakDetector();

			void CrtDumpMemoryLeaks();

		private:
		};
	}
}