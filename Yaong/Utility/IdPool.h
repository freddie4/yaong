#pragma once;
#include "Logger/Logger.h"

namespace yaong::utility
{
	template <typename ElemType,
		ElemType MIN_VALUE = ElemType(),
		ElemType MAX_VALUE = std::numeric_limits<ElemType>::max(),
		ElemType PREVENT_REUSE_TIME = 10,
		ElemType DEFAULT_SIZE = 1000,
		ElemType INCREASE_SIZE = 100
	>
	class IdPool
	{
	public:
		IdPool()
			: _allocatedCount()
		{
			if(!ExpandPool(DEFAULT_SIZE))
			{
				LOG_F("IdPool init failed");
			}
		}

		~IdPool() = default;

		std::optional<ElemType> Get()
		{
			if(_priorityQueue.empty())
			{
				if(!_freeIds.empty())
				{
					const auto timestamp = static_cast<unsigned int>(std::chrono::duration_cast<std::chrono::seconds>
						(std::chrono::system_clock::now().time_since_epoch()).count());

					const auto itBegin = _freeIds.begin();
					const auto itEnd = _freeIds.upper_bound(timestamp);

					for(auto it = itBegin; it != itEnd; ++it)
					{
						const auto freeId = it->second;

						_priorityQueue.push(freeId);
					}

					_freeIds.erase(itBegin, itEnd);
				}

				if(_priorityQueue.empty())
				{
					if(!ExpandPool(INCREASE_SIZE))
					{
						return {};
					}
				}
			}

			const auto top = _priorityQueue.top();
			_priorityQueue.pop();
			return top;
		}

		bool Free(const ElemType& value)
		{
			if(!IsInRange(value))
			{
				LOG_F("fail to free. value({}) is out of range", value);
				return false;
			}

			if(IsExistInQueue(value))
			{
				LOG_F("fail to free. value({}) is exist in queue", value);
				return false;
			}

			const auto timestamp = static_cast<unsigned int>
				(std::chrono::duration_cast<std::chrono::seconds>
				(std::chrono::system_clock::now().time_since_epoch()).count())
				+ static_cast<unsigned int>(PREVENT_REUSE_TIME * 1000);

			_freeIds.emplace(timestamp, value);
			return true;
		}

	private:
		bool ExpandPool(const ElemType& size) noexcept
		{
			if(size > GetRemainSize())
			{
				LOG_F("fail to allocate. not enough remain size. RemainSize : {}, "
					"AllocateSize: {}", GetRemainSize(), size);
				return false;
			}

			for(auto i = GetLatestId(); i < size; ++i)
			{
				_priorityQueue.emplace(i);
				++_allocatedCount;
			}

			return true;
		}

		bool IsInRange(const ElemType& value) const
		{
			if(value < MIN_VALUE)
				return false;

			if(value > MAX_VALUE)
				return false;

			if(value > GetLatestId())
				return false;

			return true;
		}

		bool IsExistInQueue(const ElemType& value) const
		{
			for(auto it = _freeIds.begin(); it != _freeIds.end(); ++it)
			{
				const auto freeId = it->second;

				if(freeId == value)
				{

					return true;
				}
			}

			if(std::binary_search(_priorityQueue.begin(), _priorityQueue.end(), value))
			{
				return true;
			}

			return false;
		}

		ElemType GetLatestId() const { return MIN_VALUE + _allocatedCount; }
		ElemType GetRemainSize() const { return MAX_VALUE - GetLatestId(); }

		ElemType _allocatedCount;
		std::priority_queue<ElemType, std::vector<ElemType>, std::greater<ElemType>> _priorityQueue;
		std::multimap<unsigned int, ElemType> _freeIds;
	};
}
