#pragma once
#include <Windows.h>
#include <Psapi.h>
#include <Pdh.h>
#include <Utility/Singleton.h>
#include <string>

namespace yaong
{
	namespace utility
	{
		constexpr unsigned long long KB = 1024;
		constexpr unsigned long long MB = KB * KB;
		constexpr unsigned long long GB = MB * KB;
		constexpr unsigned long long TB = MB * KB;

		class ProcessMonitor
		{
			friend class Singleton<ProcessMonitor>;

		public:
			bool Initialize();
			void Finalize();

			void BeginProfileTimer();
			void PrintCpuUsage();
			void PrintMemoryUsage();
			bool GetMemoryStatus();

			double GetCurrentCpuUsage();
			bool _GetProcessMemoryInfo();
			unsigned long long GetTotalPhysicalMemory();
			unsigned long long GetTotalVirtualMemory();
			unsigned long long GetCurrentVirtualMemoryUsage();

		private:
			MEMORYSTATUSEX _memoryInfo;
			PROCESS_MEMORY_COUNTERS_EX _processCounters;

			unsigned long long _totalPhysicalMemory = 0;

			PDH_HQUERY _cpuQuery = nullptr;
			PDH_HCOUNTER _cpuTotalCounter = nullptr;
			std::string _cpuUsageQuery;
		};
	}
}

#define gProcessMonitor yaong::utility::Singleton<ProcessMonitor>::Instance()