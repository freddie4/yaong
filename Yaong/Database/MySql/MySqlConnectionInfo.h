#pragma once
#include <string>

namespace yaong
{
	namespace database
	{
		struct MySqlConnectionInfo
		{
			MySqlConnectionInfo() = default;
			~MySqlConnectionInfo() = default;

			void SetInfo(const std::string& host,
					     const std::string& user,
						 const std::string& password,
						 const std::string& db,
						 const unsigned short port)
			{
				_host = host;
				_user = user;
				_password = password;
				_db = db;
				_port = port;
			}
		
			std::string _host;
			std::string _user;
			std::string _password;
			std::string _db;
			unsigned short _port = 3306;
		};
	}
}
