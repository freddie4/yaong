#include "MySqlConnection.h"
#include "MySqlConnectionInfo.h"
#include <Utility/Logger/Logger.h>

namespace yaong
{
	namespace database
	{
		MySqlConnection::MySqlConnection( const MySqlConnectionInfo* connectionInfo )
			:_connectInfo(connectionInfo)
		{
		}

		MySqlConnection::~MySqlConnection()
		{
			Close();
		}

		bool MySqlConnection::IsConnected()
		{
			return mysql_ping(&_mySql) == 0 ? true : false;
		}

		bool MySqlConnection::Connect()
		{
			if(nullptr == mysql_init(&_mySql))
			{
				LOG_F("Failed to init mysql");
				return false;
			}

			my_bool reconnect = false;
			::mysql_options(&_mySql, MYSQL_OPT_RECONNECT, &reconnect);

			auto connectionTimeout = 3;
			::mysql_options(&_mySql, MYSQL_OPT_CONNECT_TIMEOUT, &connectionTimeout);

			std::string charSet = "utf8";
			::mysql_options(&_mySql, MYSQL_SET_CHARSET_NAME, charSet.c_str());

			const auto pMySql = mysql_real_connect(
				&_mySql,
				_connectInfo->_host.c_str(),
				_connectInfo->_user.c_str(),
				_connectInfo->_password.c_str(),
				_connectInfo->_db.c_str(),
				_connectInfo->_port,
				nullptr,
				CLIENT_MULTI_RESULTS
			);

			if(nullptr == pMySql)
			{
				const auto error = mysql_errno(&_mySql);
				LOG_F("Failed to Connect MySql : host : {} port : {} error : {}",
					_connectInfo->_host.c_str(),
					_connectInfo->_port,
					error
				);

				Close();
				
				return false;
			}

			return true;
		}

		void MySqlConnection::Close()
		{
			mysql_close(&_mySql);
		}

		void MySqlConnection::BeginTransaction()
		{
			mysql_query(&_mySql, "START TRANSACTION;");
		}

		void MySqlConnection::CommitTransaction()
		{
			mysql_query(&_mySql, "COMMIT;");
		}

		void MySqlConnection::RollbackTransaction()
		{
			mysql_query(&_mySql, "ROLLBACK;");
		}

		MYSQL_STMT* MySqlConnection::CreateStatement()
		{
			const auto stmt = mysql_stmt_init(&_mySql);

			if(nullptr == stmt)
			{
				throw std::runtime_error("mysql_stmt_init failed");
			}

			return stmt;
		}
	}
}