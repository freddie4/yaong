#include "MySqlParameter.h"
#include <stdexcept>

namespace yaong
{
	namespace database
	{
		MySqlParameter::MySqlParameter()
		{
			_bindInfo = new MySqlBindingInformation[MAX_MYSQL_PARAMETERS];
			_parameters = new MYSQL_BIND[MAX_MYSQL_PARAMETERS];
		}

		MySqlParameter::~MySqlParameter()
		{
			if( nullptr != _bindInfo)
			{
				delete[] _bindInfo;
				_bindInfo = nullptr;
			}

			if( nullptr != _parameters)
			{
				delete[] _parameters;
				_parameters = nullptr;
			}
		}

		void MySqlParameter::Reset()
		{
			_parameterPosition = 0;
		}

		void MySqlParameter::AddParameter( const enum_field_types type,
			void* buffer,
			int* outLength,
			const int length,
			const int maxLength,
			const bool isUnsigned )
		{
			if(MAX_MYSQL_PARAMETERS <= _parameterPosition)
			{
				throw std::runtime_error("parameterPosition mismatched");
			}

			const auto pInfo = &_bindInfo[_parameterPosition];
			pInfo->_isNull = 0;
			pInfo->_length = length;
			pInfo->_error = 0;

			const auto pBind = &_parameters[_parameterPosition];
			::memset(pBind, 0, sizeof(MYSQL_BIND));
			pBind->param_number = _parameterPosition + 1;
			pBind->buffer_type = type;
			pBind->buffer = buffer;
			pBind->buffer_length = maxLength;
			pBind->is_null = &pInfo->_isNull;
			pBind->length = nullptr != outLength ? reinterpret_cast<unsigned long*>(outLength) : &pInfo->_length;
			pBind->error = &pInfo->_error;
			pBind->is_unsigned = isUnsigned;

			++_parameterPosition;
		}

		void MySqlInputParameter::Bind( MYSQL_STMT* pStmt )
		{
			if (_parameterCount != _parameterPosition)
			{
				throw std::runtime_error("parameter mismatched");
			}

			const auto result = mysql_stmt_bind_param(pStmt, _parameters);

			if (result != 0)
			{
				throw std::runtime_error("bind_param failed");
			}
		}

		void MySqlOutputParameter::Bind( MYSQL_STMT* pStmt )
		{
			if (_parameterCount != _parameterPosition)
			{
				throw std::runtime_error("parameter mismatched");
			}

			const auto result = mysql_stmt_bind_result(pStmt, _parameters);

			if (result != 0)
			{
				throw std::runtime_error("bind_param failed");
			}
		}
	}
}
