#pragma once
#include <mysql.h>
#include <memory>
#include <string>

namespace yaong
{
	namespace database
	{
		class MySqlConnection;
		class MySqlResult;
		class MySqlInputParameter;

		class MySqlCommand
		{
		public:
			MySqlCommand(MYSQL_STMT* stmt, MySqlConnection* connection, const std::string& command);
			~MySqlCommand();

			MYSQL_STMT* GetStatement() const { return _stmt; }

			void Prepare();
			
			MySqlResult* Execute();
			void Close();
			void Refresh();

		private:
			MySqlResult* NewResult();

		private:
			MYSQL_STMT* _stmt;
			MySqlConnection* _connection;

			std::string _command;
			MySqlResult* _result = nullptr;
			MySqlInputParameter* _parameters = nullptr;
		};
	}
}
