#pragma once

namespace yaong
{
	namespace database
	{
		class MySqlCommand;
		class MySqlOutputParameter;

		class MySqlResult
		{
		public:
			MySqlResult(const MySqlCommand* command);
			~MySqlResult();

		private:

		};
	}
}