#pragma once
#include <mysql.h>

namespace yaong
{
	namespace database
	{
		struct MySqlBindingInformation
		{
			my_bool _isNull;
			unsigned long _length;
			my_bool _error;
		};

		constexpr int MAX_MYSQL_PARAMETERS = 128;

		class MySqlParameter abstract
		{
		public:
			MySqlParameter();
			virtual ~MySqlParameter();

			void SetParameterCount(const int numberOfParameters) { _parameterCount = numberOfParameters; }
			void Reset();
			void AddParameter(const enum_field_types type, void* buffer, int* outLength, 
				const int length, const int maxLength, const bool isUnsigned);

			virtual void Bind(MYSQL_STMT* pStmt) = 0;

		protected:
			MySqlBindingInformation* _bindInfo = nullptr;
			MYSQL_BIND* _parameters = nullptr;
			int _parameterCount = 1;
			int _parameterPosition = 0;
		};

		class MySqlInputParameter : public MySqlParameter
		{
		public:
			MySqlInputParameter() = default;
			virtual ~MySqlInputParameter() = default;

			void Bind(MYSQL_STMT* pStmt) override;
		};

		class MySqlOutputParameter : public MySqlParameter
		{
		public:
			MySqlOutputParameter() = default;
			virtual ~MySqlOutputParameter() = default;

			void Bind(MYSQL_STMT* pStmt) override;
		};
	}
}
