#include "MySqlCommand.h"
#include "MySqlConnection.h"
#include "MySqlResult.h"
#include "MySqlParameter.h"

namespace yaong
{
	namespace database
	{
		MySqlCommand::MySqlCommand(MYSQL_STMT* stmt, MySqlConnection* connection, const std::string& command)
			: _stmt(stmt)
			, _connection(connection)
			, _command(command)
		{
			_parameters = new MySqlInputParameter();
		}

		MySqlCommand::~MySqlCommand()
		{
			delete _result;

			if(nullptr != _stmt)
			{
				mysql_stmt_close(_stmt);
				_stmt = nullptr;
			}
		}

		void MySqlCommand::Prepare()
		{
			if(nullptr == _stmt)
			{
				throw std::runtime_error("stmt is nullptr");
			}

			const auto result = mysql_stmt_prepare(_stmt, _command.data(), static_cast<unsigned long>(_command.size()));

			if( 0 != result)
			{
				throw std::runtime_error("prepare failed");
			}

			const auto params = mysql_stmt_param_count(_stmt);
			_parameters->SetParameterCount(params);
		}

		MySqlResult* MySqlCommand::Execute()
		{
			if(nullptr == _parameters || nullptr == _result)
			{
				throw std::runtime_error("invalied handle");
			}

			_parameters->Bind(_stmt);

			const auto result = mysql_stmt_execute(_stmt);

			if(result != 0)
			{
				throw std::runtime_error("execute failed");
			}

			return _result;
		}

		void MySqlCommand::Close()
		{
			if (nullptr != _stmt)
			{
				::mysql_stmt_close(_stmt);
				_stmt = nullptr;
			}
		}

		void MySqlCommand::Refresh()
		{
			if(nullptr != _stmt)
			{
				mysql_stmt_close(_stmt);
				_stmt = nullptr;
			}

			_parameters->Reset();

			_stmt = _connection->CreateStatement();

			Prepare();
		}

		MySqlResult* MySqlCommand::NewResult()
		{
			return new MySqlResult(this);
		}
	}
}