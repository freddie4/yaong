#pragma once
#include <mysql.h>

namespace yaong
{
	namespace database
	{
		struct MySqlConnectionInfo;
		class MySqlCommand;

		class MySqlConnection
		{
		public:
			explicit MySqlConnection(const MySqlConnectionInfo* connectionInfo);
			~MySqlConnection();
			
			bool IsConnected();
			bool Connect();
			void Close();

			void BeginTransaction(void);
			void CommitTransaction(void);
			void RollbackTransaction(void);

			MYSQL_STMT* CreateStatement(void);

		private:
			MySqlCommand* Prepare();

		private:
			MYSQL _mySql;
			const MySqlConnectionInfo* _connectInfo;
		};
	}
}