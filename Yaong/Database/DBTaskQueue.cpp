#include "DBTaskQueue.h"
#include "DBTasks.h"
#include <Utility/Logger/Logger.h>

namespace yaong
{
	namespace database
	{
		DBTaskQueue::DBTaskQueue(const unsigned int threadCount)
			:_threadCount(threadCount)
		{
		}

		DBTaskQueue::~DBTaskQueue()
		{
		}

		bool DBTaskQueue::Initialize()
		{
			_completionPort = CreateIoCompletionPort(INVALID_HANDLE_VALUE, nullptr, 0, 0);
			if (nullptr == _completionPort)
			{
				LOG_F("CreateIoCompletionPort failed");
				return false;
			}

			if(false == StartIoThreads())
			{
				LOG_F("thread init failed");
				return false;
			}

			return true;
		}

		void DBTaskQueue::Finalize()
		{
			if(nullptr == _completionPort)
			{
				return;
			}

			for(auto& i : _threadVec)
			{
				if(i->joinable())
				{
					i->join();

					delete i;
				}
			}

			CloseHandle(_completionPort);
			_completionPort = nullptr;
		}

		bool DBTaskQueue::Post(const DBTask* dbTask)
		{
			if (false == PostQueuedCompletionStatus(_completionPort, 0, reinterpret_cast<ULONG_PTR>(dbTask), nullptr))
			{
				const auto error = GetLastError();
				LOG_F("Post DBTask failed : {}", error);
				
				return false;
			}

			return true;
		}

		bool DBTaskQueue::Enqueue(const DBTask* dbTask)
		{
			return Post(dbTask);
		}

		bool DBTaskQueue::Dequeue()
		{
		}

		void DBTaskQueue::Run()
		{
			DWORD dwTransferred = 0;
			LPOVERLAPPED lpOverlapped = nullptr;
			ULONG_PTR completionKey = 0;

			while (true)
			{
				const auto result = GetQueuedCompletionStatus(
					_completionPort,
					&dwTransferred,
					reinterpret_cast<PULONG_PTR>(&completionKey),
					&lpOverlapped, INFINITE);

				if(result == false)
				{
					if (WSAGetLastError() == WAIT_TIMEOUT)
					{
						HandleTimeout();
						continue;
					}
				}

				if (!lpOverlapped)
				{
					break;
				}

				HandleCompletion(completionKey);
			}
		}

		void DBTaskQueue::HandleCompletion(const ULONG_PTR task)
		{
		}

		void DBTaskQueue::HandleTimeout()
		{
			// check DB connection
		}

		bool DBTaskQueue::StartIoThreads()
		{
			for(unsigned int i = 0; i < _threadCount; ++i)
			{
				_threadVec.emplace_back(new std::thread(std::bind(&DBTaskQueue::Run, this)));
			}

			return true;
		}
	}
}
