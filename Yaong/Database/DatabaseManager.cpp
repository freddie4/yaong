#include "DatabaseManager.h"
#include <Database/MySql/MySqlConnection.h>
#include <Database/MySql/MySqlConnectionInfo.h>

namespace yaong
{
	namespace database
	{
		DatabaseManager::DatabaseManager()
			:_mysqlConnectionInfo(std::make_unique<MySqlConnectionInfo>())
		{
			_mysqlConnectionInfo->SetInfo("127.0.0.1", "neon_app", "neon1004", "neon_auth", 3306);

			_mysqlConnection = std::make_unique<MySqlConnection>(_mysqlConnectionInfo.get());
		}

		DatabaseManager::~DatabaseManager()
		{
			_mysqlConnection->Close();
		}

		bool DatabaseManager::Connect()
		{
			if(false == _mysqlConnection->Connect())
			{
				return false;
			}

			return true;
		}
	}
}