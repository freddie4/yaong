#pragma once
#include <Windows.h>
#include <thread>
#include <vector>

namespace yaong
{
	namespace database
	{
		constexpr int DEFAULT_IOCP_THREAD_COUNT = 4;

		class DBTask;

		class DBTaskQueue
		{
		public:
			explicit DBTaskQueue(const unsigned int threadCount);
			~DBTaskQueue();

			bool Initialize();
			void Finalize();

			bool Post(const DBTask* dbTask);
			bool Enqueue(const DBTask* dbTask);
			bool Dequeue();

			void Run();

			void HandleCompletion(const ULONG_PTR task);			

		private:
			bool StartIoThreads();
			void HandleTimeout();

		private:
			HANDLE _completionPort = nullptr;
			std::vector<std::thread*> _threadVec;
			unsigned int _threadCount = DEFAULT_IOCP_THREAD_COUNT;
		};
	}
}
