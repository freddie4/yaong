#pragma once
#include <memory>
#include <Utility/Singleton.h>

namespace yaong
{
	namespace database
	{
		class MySqlConnection;
		class MySqlConnectionInfo;

		class DatabaseManager
		{
			friend class utility::Singleton<DatabaseManager>;
		private:
			DatabaseManager();
			~DatabaseManager();

		public:
			bool Connect();

		private:
			std::unique_ptr<MySqlConnectionInfo> _mysqlConnectionInfo = nullptr;
			std::unique_ptr<MySqlConnection> _mysqlConnection = nullptr;
		};
	}
}

#define gDatabaseManager yaong::utility::Singleton<DatabaseManager>::Instance()
