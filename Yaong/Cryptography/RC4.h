#pragma once
#include "CryptoBase.h"

namespace yaong
{
	namespace crypto
	{
		using Byte = unsigned char;

		class Rc4 final : public CryptoBase
		{
		public:
			Rc4() = default;
			Rc4(const char key1, const char key2);
			virtual ~Rc4() = default;
			
			Rc4(const Rc4&) = delete;
			Rc4& operator=(const Rc4&) = delete;
			Rc4(Rc4&&) = delete;
			Rc4& operator=(Rc4&&) = delete;

			void SetKey(const std::string& key1, const std::string& key2) override;
			void Encode(char* buffer, const size_t bufSize) const override;
			void Decode(char* buffer, const size_t bufSize) const override;

		private:
			char _key1 = '\0';
			char _key2 = '\0';
		};
	}
}