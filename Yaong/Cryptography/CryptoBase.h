#pragma once
#include <string>

namespace yaong
{
	namespace crypto
	{
		using CipherBuffer = std::pair<char*, size_t>;

		class CryptoBase abstract
		{
		public:
			CryptoBase() = default;
			virtual ~CryptoBase() = default;

			virtual void SetKey(const std::string& key1, const std::string& key2) = 0;
			virtual void Encode(char* buffer, const size_t bufSize) const = 0;
			virtual void Decode(char* buffer, const size_t bufSize) const = 0;
		};
	}
}
