#include "stdafx.h"
#include "RC4.h"

namespace yaong
{
	namespace crypto
	{
		Rc4::Rc4( const char key1,
			const char key2 )
			: _key1(key1), _key2(key2)
		{
		}

		void Rc4::SetKey( const std::string& key1, const std::string& key2 )
		{
			_key1 = static_cast<char>(static_cast<Byte>((std::stoi(key1) % 0xff)));
			_key2 = static_cast<char>(static_cast<Byte>((std::stoi(key2) % 0xff)));
		}

		void Rc4::Encode( char* buffer,	const size_t bufSize ) const
		{
			const auto begin = buffer;
			const auto end = buffer + bufSize;
			auto key1 = _key1;
			auto key2 = _key2;

			for (auto* i = begin; i < end; ++i)
			{
				const auto value = *i;

				key1 = key1 ^ *i;
				*i = key1 + key2;
				key2 = key2 + value;
			}
		}

		void Rc4::Decode( char* buffer,	const size_t bufSize ) const
		{
			const auto begin = buffer;
			const auto end = buffer + bufSize;
			auto key1 = _key1;
			auto key2 = _key2;

			for (auto* i = begin; i < end; ++i)
			{
				const auto value = *i;

				*i = value - key2;
				*i = *i ^ key1;
				key1 = key1 ^ *i;
				key2 = key2 + *i;
			}
		}
	}
}