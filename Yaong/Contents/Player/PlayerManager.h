#pragma once
#include <Utility/Singleton.h>
#include <unordered_map>
#include <memory>
#include <Utility/Logger/Logger.h>

namespace yaong
{	
	struct PlayerInfo;
	class Player;
    
	using SessionID = int;
	using PlayerMap = std::unordered_map<SessionID, std::shared_ptr<Player>>;

	class PlayerManager
	{
		friend class utility::Singleton<PlayerManager>;

	public:
		PlayerMap* GetCharacterMap() { return &_playerMap; }

		void CreatePlayer(const PlayerInfo& playerInfo);
		void ReleasePlayer(const int playerId);
		auto GetPlayer(const int playerId)
		{
			const auto found = _playerMap.find(playerId);
			if(_playerMap.end() == found)
			{
				LOG_F("No SessionID");
			}

			return found->second;
		}

	private:
		PlayerMap _playerMap;
	};
}

#define gPlayerManager yaong::utility::Singleton<PlayerManager>::Instance()