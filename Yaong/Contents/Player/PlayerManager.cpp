#include "stdafx.h"
#include "PlayerManager.h"

#include "Player.h"
#include "PlayerInfo.h"
#include <Utility/Logger/Logger.h>

namespace yaong
{
	void PlayerManager::CreatePlayer(const PlayerInfo& playerInfo)
	{
		const auto player = std::make_shared<Player>(playerInfo);
		_playerMap[playerInfo.playerId] = player;
	}

	void PlayerManager::ReleasePlayer(const int playerId)
	{
		_playerMap.erase(playerId);
	}
}
