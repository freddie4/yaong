#pragma once
#include <Math/Vector.h>
#include <Contents/Player/PlayerInfo.h>

namespace yaong
{
	class Player
	{
	public:
		Player() = default;
		Player(const PlayerInfo& info)
		{
			_Pos = { info.x, info.y, info.z };
		}
		~Player() = default;

		auto GetPos() const { return _Pos; }
		void SetPos(const FVector& pos) { _Pos = pos; }

	private:
		float _hp;
		float _mp;

		FVector _Pos;
	};
}
