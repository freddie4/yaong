#pragma once

namespace yaong
{
	struct PlayerInfo
	{
		int sessionId = -1;
		int playerId = -1;
		float x = 0;
		float y = 0;
		float z = 0;
		float yaw = 0;
		float roll = 0;
		float pitch = 0;
		float vx = 0;
		float vy = 0;
		float vz = 0;

		int hp = 0;
		bool isAlive = false;
		bool isAttacking = false;
	};
}