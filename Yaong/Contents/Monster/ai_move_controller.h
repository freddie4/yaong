﻿#pragma once

namespace nxn
{

class ZoneMonster;

class AIMoveController
{
public:
    explicit AIMoveController(ZoneMonster& zoneMonster);
    virtual ~AIMoveController() = default;

    bool Update(const msec& delta);

    void SetActive();
    void SetDisable();

    void SetEndPos(const FVector& endPos);

protected:
    ZoneMonster& mZoneMonster;

private:
    bool GetPositionFromFollowPath(const float distance, FVector& pos, bool& isArrived) const;

    bool mIsActive;

    bool mIsChangedEndPos;
    boost::optional<FVector> mEndPos;
    std::vector<FVector> mFollowPath;

    msec mMoveAccumulatedDelta;
    float mMoveAccumulatedDistance;
};

} // namespace nxn
