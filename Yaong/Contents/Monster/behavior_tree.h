﻿#pragma once
#include "behavior_tree_data.h"
#include "zone_monster.h"

namespace nxn
{
class Node;
class BehaviorTree;

class Node : public std::enable_shared_from_this<Node>
{
public:
    enum class Result
    {
        Success,
        Failure,
    };

public:
    Node() = default;
    explicit Node(const std::string& nodeName) :mName(nodeName) {}
    virtual ~Node() = default;

    auto GetName() const { return mName; }
    auto GetTree() const { return mSpTree; }
    ZoneMonster* GetOwner() const { return mpOwner; }

    void SetTree(const std::shared_ptr<BehaviorTree>& spTree) { mSpTree = spTree; }
    void SetOwner(ZoneMonster* pOwner) { mpOwner = pOwner; }
    void SetAIData(const std::shared_ptr<AIData>& spData) { mSpAIData = spData; }

    virtual Result Update() = 0;

protected:
    std::string mName;

    // TODO : Generalize Unit class
    ZoneMonster* mpOwner = nullptr;

    std::shared_ptr<AIData> mSpAIData = nullptr;
    std::shared_ptr<BehaviorTree> mSpTree = nullptr;
};

inline Node::Result UpdateNode(const std::shared_ptr<Node>& spNode);

class BehaviorTree : public std::enable_shared_from_this<BehaviorTree>
{
public:
    enum class BTState
    {
        Active,
        Stopped,
        Running,
    };

    explicit BehaviorTree(const std::string& name) : mName(name) {}
    ~BehaviorTree() = default;

    void StopTree()
    {
        mState = BTState::Stopped;
    }

    void SetState(const BTState state)
    {
        mState = state;
    }

    void SetRoot(const std::shared_ptr<Node>& spNode)
    {
        mSpRoot = spNode;
        mSpRoot->SetTree(shared_from_this());
    }

    void SetOwner(ZoneMonster* pUnit)
    {
        ASSERT(pUnit);

        mpOwner = pUnit;

        for (const auto& spNode : mSpAllNodes)
        {
            spNode->SetOwner(pUnit);
        }
    }

    void SetAIData(const std::shared_ptr<MonsterAIData>& spData)
    {
        mSpAIData = spData;

        for (const auto& spNode : mSpAllNodes)
        {
            spNode->SetAIData(spData);
        }
    }

    void AddNode(const std::shared_ptr<Node>& spNode) { mSpAllNodes.emplace_back(spNode); }

    auto GetAIData() const { return mSpAIData; }

    bool SingleRun()
    {
        if (BTState::Stopped == mState || BTState::Running == mState)
        {
            return false;
        }

        if (!mSpRoot)
        {
            LOG_ERROR(LOG_FILTER_MONSTER_AI, "BehaviorTree has no root node");
            SetState(BTState::Stopped);
            return false;
        }

        SetState(BTState::Running);
        UpdateNode(mSpRoot);
        SetState(BTState::Active);

        return true;
    }

protected:
    BTState mState = BTState::Active;

    // TODO : Generalize owner type
    ZoneMonster* mpOwner = nullptr;
    std::shared_ptr<MonsterAIData> mSpAIData = nullptr;

    std::string mName;

    std::shared_ptr<Node> mSpRoot = nullptr;

    std::vector<std::shared_ptr<Node>> mSpAllNodes;
};

inline Node::Result UpdateNode(const std::shared_ptr<Node>& spNode)
{
    if (!spNode->GetOwner())
    {
        spNode->GetTree()->StopTree();
        return Node::Result::Failure;
    }

    return spNode->Update();
}

class Composite : public Node
{
public:
    explicit Composite(const std::string& nodeName) : Node(nodeName) {}
    ~Composite() override = default;

    std::size_t GetChildCount() const { return mSpChildNodes.size(); }

    void AddChild(const std::shared_ptr<Node>& spNode)
    {
        spNode->SetTree(mSpTree);
        mSpChildNodes.emplace_back(spNode);
    }

    bool IsEmpty() const
    {
        return mSpChildNodes.empty();
    }

    void ClearChildNodes()
    {
        mSpChildNodes.clear();
    }

protected:
    std::vector<std::shared_ptr<Node>> mSpChildNodes;
};

class Sequence : public Composite
{
public:
    Sequence() : Composite(__func__) {}
    explicit Sequence(const std::string& nodeName) : Composite(nodeName) {}
    ~Sequence() override = default;

    Result Update() override
    {
        if (!mSpChildNodes.empty())
        {
            for (auto& spChildNode : mSpChildNodes)
            {
                const auto state = UpdateNode(spChildNode);

                if (state == Result::Failure)
                {
                    return Result::Failure;
                }
            }

            return Result::Success;
        }

        LOG_ERROR(LOG_FILTER_MONSTER_AI, "Sequence has no child node");
        return Result::Failure;
    }
};

class Selector : public Composite
{
public:
    Selector() : Composite(__func__) {}
    explicit Selector(const std::string& nodeName) : Composite(nodeName) {}
    ~Selector() override = default;

    Result Update() override
    {
        if (!mSpChildNodes.empty())
        {
            for (auto& spChildNode : mSpChildNodes)
            {
                const auto state = UpdateNode(spChildNode);

                if (state == Result::Success)
                {
                    return Result::Success;
                }
            }

            return Result::Failure;
        }

        LOG_ERROR(LOG_FILTER_MONSTER_AI, "Selector has no child node");
        return Result::Failure;
    }
};

class Decorator : public Node
{
public:
    explicit Decorator(const std::string& nodeName) : Node(nodeName) {}
    ~Decorator() override = default;

    void AddChild(const std::shared_ptr<Node>& spNode)
    {
        spNode->SetTree(mSpTree);
        mSpChild = spNode;
    }

    std::shared_ptr<Node> GetChild() const { return mSpChild; }

protected:
    std::shared_ptr<Node> mSpChild = nullptr;
};

class Inverter : public Decorator
{
public:
    explicit Inverter(const std::string& nodeName) : Decorator(nodeName) {}
    ~Inverter() override = default;

    Result Update() override
    {
        const auto state = UpdateNode(mSpChild);

        if (state == Result::Success)
        {
            return Result::Failure;
        }
        else if (state == Result::Failure)
        {
            return Result::Success;
        }

        return state;
    }
};

class Succeeder : public Decorator
{
public:
    explicit Succeeder(const std::string& nodeName) : Decorator(nodeName) {}
    ~Succeeder() override = default;

    Result Update() override
    {
        UpdateNode(mSpChild);

        return Result::Success;
    }
};

class Repeater : public Decorator
{
public:
    Repeater(const std::string& nodeName, const uint32 repeatCount)
        : Decorator(nodeName), mRepeatCount(repeatCount) {}
    ~Repeater() override = default;

    Result Update() override
    {
        for (uint32 i = 0; i < mRepeatCount; ++i)
        {
            UpdateNode(mSpChild);
        }

        return Result::Success;
    }

private:
    uint32 mRepeatCount = 1;
};

class RepeatUntilFail : public Decorator
{
public:
    explicit RepeatUntilFail(const std::string& nodeName,
        const uint32 repeatCount) : Decorator(nodeName), mRepeatCount(repeatCount) {}
    ~RepeatUntilFail() override = default;

    Result Update() override
    {
        for (uint32 i = 0; i < mRepeatCount; ++i)
        {
            const auto state = UpdateNode(mSpChild);

            if (state == Result::Failure)
            {
                return Result::Success;
            }
        }

        return Result::Failure;
    }

private:
    uint32 mRepeatCount = 1;
};

class TaskNodeBase : public Node
{
public:
    explicit TaskNodeBase(const std::string& nodeName) : Node(nodeName){}
    ~TaskNodeBase() override = default;

    void SetChildBehaviorTree(const std::shared_ptr<BehaviorTree>& spTree)
    {
        mSpChildTree = spTree;
    }

private:
    std::shared_ptr<BehaviorTree> mSpChildTree = nullptr;
};

template <typename Parent>
class DecoratorBuilder;

template <typename Parent>
class CompositeBuilder
{
public:
    CompositeBuilder(Parent* pParent, Composite* pNode,
        const std::shared_ptr<BehaviorTree>& spTree)
        : mpParent(pParent), mpNode(pNode), mSpTree(spTree) {}
    ~CompositeBuilder() = default;

    template
    <
        template <typename, typename...>
        typename NodeType, typename... Args,
        typename _TypeCheck = std::enable_if_t<std::is_base_of_v<TaskNodeBase, NodeType<Args...>>>
    >
    auto task(Args... args)
    {
        auto spChild = std::make_shared<NodeType<Args...>>(args...);
        mpNode->AddChild(spChild);
        mSpTree->AddNode(spChild);
        return *this;
    }

    template
    <
        typename CompositeType, typename... Args,
        typename _TypeCheck = std::enable_if_t<std::is_base_of_v<Composite, CompositeType>>
    >
    auto composite(Args... args)
    {
        auto spChild = std::make_shared<CompositeType>(args...);
        mpNode->AddChild(spChild);
        mSpTree->AddNode(spChild);
        return CompositeBuilder<CompositeBuilder<Parent>>(this,
            static_cast<CompositeType*>(spChild.get()), mSpTree);
    }

    template
    <
        template <typename, typename...>
        typename DecoratorType, typename... Args,
        typename _TypeCheck = std::enable_if_t<std::is_base_of_v<Decorator, DecoratorType<Args...>>>
    >
    auto decorator(Args... args)
    {
        auto spChild = std::make_shared<DecoratorType<Args...>>(args...);
        mpNode->AddChild(spChild);
        mSpTree->AddNode(spChild);
        return DecoratorBuilder<CompositeBuilder<Parent>>(this,
            static_cast<DecoratorType<Args...>*>(spChild.get()), mSpTree);
    }

    auto& end()
    {
        ASSERT(mpParent);
        return *mpParent;
    }

private:
    Parent* mpParent;
    Composite* mpNode;
    std::shared_ptr<BehaviorTree> mSpTree = nullptr;
};

template <typename Parent>
class DecoratorBuilder
{
public:
    DecoratorBuilder(Parent* pParent, Decorator* pNode,
        const std::shared_ptr<BehaviorTree>& spTree)
        : mpParent(pParent), mpNode(pNode), mSpTree(spTree) {}
    ~DecoratorBuilder() = default;

    template
    <
        template <typename, typename...>
        typename NodeType, typename... Args,
        typename _TypeCheck = std::enable_if_t<std::is_base_of_v<TaskNodeBase, NodeType<Args...>>>
    >
    auto task(Args... args)
    {
        auto spChild = std::make_shared<NodeType<Args...>>(args...);
        mpNode->AddChild(spChild);
        mSpTree->AddNode(spChild);
        return *this;
    }

    template
    <
        typename CompositeType, typename... Args,
        typename _TypeCheck = std::enable_if_t<std::is_base_of_v<Composite, CompositeType>>
    >
    auto composite(Args... args)
    {
        auto spChild = std::make_shared<CompositeType>(args...);
        mpNode->AddChild(spChild);
        mSpTree->AddNode(spChild);
        return CompositeBuilder<DecoratorBuilder<Parent>>(this,
            static_cast<CompositeType*>(spChild.get()), mSpTree);
    }

    template
    <
        template <typename, typename...>
        typename DecoratorType, typename... Args,
        typename = std::enable_if_t<std::is_base_of_v<Decorator, DecoratorType<Args...>>>
    >
    auto decorator(Args... args)
    {
        auto spChild = std::make_shared<DecoratorType<Args...>>(args...);
        mpNode->AddChild(spChild);
        mSpTree->AddNode(spChild);
        return DecoratorBuilder<DecoratorBuilder<Parent>>(this,
            static_cast<DecoratorType<Args...>*>(spChild.get()), mSpTree);
    }

    auto& end()
    {
        ASSERT(mpParent);
        return *mpParent;
    }

private:
    Parent* mpParent;
    Decorator* mpNode;
    std::shared_ptr<BehaviorTree> mSpTree = nullptr;
};

class BTBuilder
{
public:
    explicit BTBuilder(const std::string& treeName)
        : mSpTree(std::make_shared<BehaviorTree>(treeName))
    {
    }
    ~BTBuilder() = default;

    template
    <
        template <typename, typename...>
        typename NodeType, typename... Args,
        typename _TypeCheck = std::enable_if_t<std::is_base_of_v<TaskNodeBase, NodeType<Args...>>>
    >
    auto task(Args... args)
    {
        mSpRoot = std::make_shared<NodeType<Args...>>(args...);
        mSpRoot->SetTree(mSpTree);
        mSpTree->AddNode(mSpRoot);
        return *this;
    }

    template
    <
        typename CompositeType, typename... Args,
        typename _TypeCheck = std::enable_if_t<std::is_base_of_v<Composite, CompositeType>>
    >
    auto composite(Args... args)
    {
        mSpRoot = std::make_shared<CompositeType>(args...);
        mSpRoot->SetTree(mSpTree);
        mSpTree->AddNode(mSpRoot);
        return CompositeBuilder<BTBuilder>(this,
            static_cast<CompositeType*>(mSpRoot.get()), mSpTree);
    }

    template
    <
        template <typename, typename...>
        typename DecoratorType, typename... Args,
        typename _TypeCheck = std::enable_if_t<std::is_base_of_v<Decorator, DecoratorType<Args...>>>
    >
    auto decorator(Args... args)
    {
        mSpRoot = std::make_shared<DecoratorType<Args...>>(args...);
        mSpRoot->SetTree(mSpTree);
        mSpTree->AddNode(mSpRoot);
        return DecoratorBuilder<BTBuilder>(this,
            static_cast<DecoratorType<Args...>*>(mSpRoot.get()), mSpTree);
    }

    auto build()
    {
        if (!mSpRoot)
        {
            LOG_ERROR(LOG_FILTER_MONSTER_AI, "Behavior Tree has no root");
        }

        mSpTree->SetRoot(mSpRoot);
        return mSpTree;
    }

private:
    std::shared_ptr<Node> mSpRoot = nullptr;
    std::shared_ptr<BehaviorTree> mSpTree = nullptr;
};

} // namespace nxn

#define DECLARE_NODE(NodeName, NodeType, OwnerType, AIDataType)\
template<typename... Args>\
class NodeName : public NodeType\
{\
public:\
    explicit NodeName(Args... args) : NodeType(__func__)\
    {\
        mParameters = std::tuple<Args...>((args)...);\
    }\
    virtual ~NodeName() = default;\
    NodeName(const NodeName&) = delete;\
    NodeName& operator=(const NodeName&) = delete;\
    NodeName(NodeName&&) = delete;\
    NodeName& operator=(NodeName&&) = delete;\
    Node::Result Update() override\
    {\
        return this->Update(static_cast<OwnerType*>(mpOwner),\
            std::static_pointer_cast<AIDataType>(mSpAIData));\
    }\
    Node::Result Update(OwnerType* pOwner,\
        const std::shared_ptr<AIDataType>& spAiData);\
private:\
    template<auto Index>\
    auto GetParameter() const { return std::get<Index - 1>(mParameters); }\
    std::tuple<Args...> mParameters;\
};

#define IMPLEMENT_NODE(NodeName, OwnerType, AIDataType)\
template<typename... Args>\
Node::Result NodeName<Args...>::Update(OwnerType* pOwner,\
        const std::shared_ptr<AIDataType>& spAiData)
