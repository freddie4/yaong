﻿#include "stdafx.h"
#include "behavior_tree_manager.h"

#include "zone_behavior_trees.h"

namespace nxn
{

BehaviorTreeManager::BehaviorTreeManager()
{
    Initialize();
}

#define REGISTER_TREE_MAKER(x) RegisterTreeMaker(#x, x)
void BehaviorTreeManager::Initialize()
{
    REGISTER_TREE_MAKER(BT_MeleeMonster);
    REGISTER_TREE_MAKER(BT_LeaderMonster);
    REGISTER_TREE_MAKER(BT_RangeMonster);
    REGISTER_TREE_MAKER(BT_Cow);
}
#undef REGISTER_TREE_MAKER

void BehaviorTreeManager::RegisterTreeMaker(const TreeName& treeName, const TreeMaker& treeMaker)
{
    const MapHashId mapHashId(GetHash64FromString(treeName));
    mTreeFactory[mapHashId] = treeMaker;
}

std::shared_ptr<BehaviorTree> BehaviorTreeManager::GetBehaviorTree(const TreeName& treeName)
{
    const MapHashId mapHashId(GetHash64FromString(treeName));

    const auto found = mTreeFactory.find(mapHashId);

	if(found == mTreeFactory.end())
	{
        LOG_ERROR(LOG_FILTER_MONSTER_AI, "BehaviorTree Not Found : {}", treeName);
        return nullptr;
	}

    return found->second();
}

void BehaviorTreeManager::Clear()
{
    mTreeFactory.clear();
}

} // namespace nxn
