﻿#pragma once
#include "behavior_tree.h"
#include "zone_behavior_tasks.h"

namespace nxn
{

inline auto BT_Test()
{
auto tree = BTBuilder(__func__)
.composite<Selector>() // {45.8,106.2}
.decorator<InCombat>() // {204.8,280.8}
.decorator<IsTargetClose>() // {438.4,312.8}
.task<BeFarAway>() // {394.4,116.8}
.end()
.end()
.task<SearchTarget>() // {275.4,130.2}
.end()
.build();

return tree;
}

inline auto BT_MeleeMonster()
{
    auto tree = BTBuilder(__func__)
        .composite<Selector>() // {558.2,54.8}
    		.decorator<IsReturning>() // {30.6,213.4}
    			.task<CheckIfReturned>() // {19.8,305}
    		.end()
            .decorator<InCombat>() // {315.4,197.4}
    			.composite<Selector>() // {321,288.6}
    				.decorator<IsFarFromSpawnedPos>(5000.f) // {154.6,374.2}
    					.task<ReturnToSpawnedPos>() // {148,487.2}
    				.end()
	                .decorator<TargetAliveCheck>() // {368.4,374.8}
	                    .composite<Selector>() // {411.4,458.2}
	                        .decorator<CanAttack>() // {376.6,567.4}
	                            .task<AttackTarget>() // {342.8,648.4}
	                        .end()
	                        .task<MoveToAttackPos>() // {488.4,570.8}
	                    .end()
	                .end()
    			.end()
            .end()
            .decorator<InGroupMove>() // {685,213.4}
                .composite<Selector>() // {714.8,338}
                    .task<SearchTarget>() // {619.2,471.2}
                    .task<FormationMove>() // {755,473.8}
                .end()
            .end()
            .composite<Selector>() // {962.8,267.6}
                .task<FindLeaderMonster>() // {1049.8,380.6}
                .composite<Selector>() // {924.6,367.2}
                    .task<SearchTarget>() // {923.2,481.6}
                .end()
            .end()
     .end()
    .build();

    return tree;
}

inline auto BT_LeaderMonster()
{
    auto tree = BTBuilder(__func__)
        .composite<Selector>() // {402.6,36.6}
			.decorator<IsReturning>() // {114.6,115.9}
				.task<CheckIfReturned>() // {106.4,224.2}
			.end()
            .decorator<InCombat>() // {389.9,132.2}
				.composite<Selector>() // {391.5,222.4}
					.decorator<IsFarFromSpawnedPos>(5000.f) // {211,361.5}
						.task<ReturnToSpawnedPos>() // {210.8,454}
					.end()
					.decorator<TargetAliveCheck>() // {447.6,361.2}
						.composite<Selector>() // {498.2,437.9}
							.decorator<CanAttack>() // {446.8,515.6}
								.task<AttackTarget>() // {425.7,598}
							.end()
							.task<MoveToAttackPos>() // {581.8,519}
						.end()
					.end()
				.end()
			.end()
            .composite<Selector>() // {656.7,139.8}
                .task<SearchTarget>() // {647.6,246.5}
            .end()     
		.end()
    .build();

    return tree;
}

inline auto BT_RangeMonster()
{
    auto tree = BTBuilder(__func__)
        .composite<Selector>() // {146.8,123.6}
            .decorator<InCombat>() // {258.6,36.6}
                .decorator<TargetAliveCheck>() // {289,107}
                    .composite<Selector>() // {235.4,131}
                        .decorator<LowHp>() // {77.8,199.8}
                            .composite<Selector>() // {390.6,54.2}
                                .decorator<IsTargetClose>() // {335.4,142.2}
                                    .task<BeFarAway>() // {198.6,219.8}
                                .end()
                                .task<RandomMove>() // {149,254.2}
                            .end()
                        .end()
                        .composite<Selector>() // {69.8,290.2}
                            .decorator<CanAttack>() // {527.4,347}
                            .composite<Selector>() // {206.6,404.6}
                                .decorator<IsTargetClose>() // {138.6,452.6}
                                    .task<BeFarAway>() // {53,457.4}
                                .end()
                                .task<AttackTarget>() // {333,417.4}
                            .end()
                        .end()
                        .end()
                    .end()
                .end()
            .end()
            .decorator<InGroupMove>() // {365.8,126.2}
                .composite<Selector>() // {369.8,172.6}
                    .task<SearchTarget>() // {323.4,233.4}
                    .task<FormationMove>() // {244.2,257.4}
                .end()
            .end()
            .composite<Selector>() // {133,275}
                .task<FindLeaderMonster>() // {52.2,331}
                .composite<Selector>() // {473.8,112.6}
                    .task<SearchTarget>() // {310.6,299.8}
                    .task<RandomMove>() // {544.2,223.8}
                .end()
            .end()
        .end()
    .build();

    return tree;
}

inline auto BT_Cow()
{
    auto tree = BTBuilder(__func__)
    .build();

    return tree;
}

} // namespace nxn
