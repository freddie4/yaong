﻿#pragma once
#include "behavior_tree.h"
#include "zone_monster.h"
#include "zone_server_app.h"

#include "ai_aggro_manager.h"
#include "ai_formation_manager.h"
#include "ai_skill_controller.h"
#include "ai_stat_regenerator.h"
#include "faction.h"
#include "zone_unitlock.h"

//#define ACTIVATE_BT_LOG
#if defined(ACTIVATE_BT_LOG)
#define BT_LOG_DEBUG \
LOG_DEBUG(LOG_FILTER_MONSTER_AI, "{} - {}", pOwner->GetUnitServerId(), mName)
#else
#define BT_LOG_DEBUG
#endif

namespace nxn
{

// Task
DECLARE_NODE(RandomMove, TaskNodeBase, ZoneMonster, MonsterAIData)
DECLARE_NODE(SearchTarget, TaskNodeBase, ZoneMonster, MonsterAIData)
DECLARE_NODE(MoveToTargetPos, TaskNodeBase, ZoneMonster, MonsterAIData)
DECLARE_NODE(MoveToAttackPos, TaskNodeBase, ZoneMonster, MonsterAIData)
DECLARE_NODE(AttackTarget, TaskNodeBase, ZoneMonster, MonsterAIData)
DECLARE_NODE(RunAway, TaskNodeBase, ZoneMonster, MonsterAIData)
DECLARE_NODE(FindLeaderMonster, TaskNodeBase, ZoneMonster, MonsterAIData)
DECLARE_NODE(FormationMove, TaskNodeBase, ZoneMonster, MonsterAIData)
DECLARE_NODE(BeFarAway, TaskNodeBase, ZoneMonster, MonsterAIData)
DECLARE_NODE(DoNothing, TaskNodeBase, ZoneMonster, MonsterAIData)
DECLARE_NODE(ReturnToSpawnedPos, TaskNodeBase, ZoneMonster, MonsterAIData)
DECLARE_NODE(CheckIfReturned, TaskNodeBase, ZoneMonster, MonsterAIData)

// Decorator
DECLARE_NODE(InCombat, Decorator, ZoneMonster, MonsterAIData)
DECLARE_NODE(InGroupMove, Decorator, ZoneMonster, MonsterAIData)
DECLARE_NODE(CanAttack, Decorator, ZoneMonster, MonsterAIData)
DECLARE_NODE(LowHp, Decorator, ZoneMonster, MonsterAIData)
DECLARE_NODE(TargetAliveCheck, Decorator, ZoneMonster, MonsterAIData)
DECLARE_NODE(IsTargetClose, Decorator, ZoneMonster, MonsterAIData)
DECLARE_NODE(IsFarFromSpawnedPos, Decorator, ZoneMonster, MonsterAIData)
DECLARE_NODE(IsReturning, Decorator, ZoneMonster, MonsterAIData)

/*
    IMPLEMENT_NODE(NodeName, OwnerType, AIDataType)
    Result NodeName::Update(
    const std::any& args,
    OwnerType* owner,
    const std::shared_ptr<AIDataType>& aiData)
*/

IMPLEMENT_NODE(RandomMove, ZoneMonster, MonsterAIData)
{
    const auto& now = std::chrono::steady_clock::now();
    const auto& interval =
        std::chrono::duration_cast<std::chrono::milliseconds>(now - spAiData->mLastMoved);

    if (interval < std::chrono::milliseconds(5000))
    {
        return Result::Success;
    }

    spAiData->mLastMoved = now;

    const auto mapName = pOwner->GetMapName(); // Do not modify.(const T&)
    const auto currentPos = pOwner->GetPos(); // Do not modify.(const T&)
    const auto spawnedPos = pOwner->GetSpawnedPos();

    constexpr auto tenMeter = 10 * ONE_METER;

    FVector movePos;
    if (!g_ZoneNavigation.FindRandomPositionAroundCircle(mapName, spawnedPos, tenMeter, movePos))
    {
        movePos = currentPos;
    }

    pOwner->Move(movePos);

    BT_LOG_DEBUG;

    return Result::Success;
}



IMPLEMENT_NODE(SearchTarget, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    const auto mapName = pOwner->GetMapName(); // Do not modify.(const T&)
    const auto monsterUnitServerId = pOwner->GetUnitServerId(); // Do not modify.(const T&)
    const auto sightAABB = pOwner->GetSightAABB();
    const auto& sightUnitServerIds =
        g_ZoneNavigation.GetOverlappingWithCollision(mapName, { sightAABB });

    const auto currentPos = pOwner->GetPos(); // Do not modify.(const T&)
    float distanceClosest = 0;

    UnitServerId targetUnitServerId;

    auto& zoneUnitManager = g_ZoneUnitManager;
    auto& gameDataTable = g_GameDataTable;

    for (const auto& sightUnitServerId : sightUnitServerIds)
    {
        if (Faction::IsHostile(gameDataTable, monsterUnitServerId, sightUnitServerId))
        {
            const auto& heroUnitSharedData =
                zoneUnitManager.GetHeroUnitSharedData(sightUnitServerId);

            if (const auto spTargetState = heroUnitSharedData.GetUnitState())
            {
                const UnitState targetState = *spTargetState;

                if (!IsCombatable(targetState))
                    continue;

                if (const auto spTargetPos = heroUnitSharedData.GetPosition())
                {
                    const FVector targetPos = *spTargetPos;

                    const auto distance = FVector::Distance(currentPos, targetPos);

                    if (distanceClosest == 0)
                    {
                        distanceClosest = distance;
                        targetUnitServerId = sightUnitServerId;
                    }
                    else if (distanceClosest > distance)
                    {
                        distanceClosest = distance;
                        targetUnitServerId = sightUnitServerId;
                    }
                }
                else
                {
                    LOG_ERROR(LOG_FILTER_MONSTER_AI, "Target is nullptr");
                    return Result::Failure;
                }
            }
        }
    }

    if (distanceClosest != 0)
    {
        pOwner->AddAggro(targetUnitServerId, AggroPoint(1));
        return Result::Success;
    }

    return Result::Failure;
}

IMPLEMENT_NODE(MoveToTargetPos, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    const auto targetUnitServerId = pOwner->GetAggroTargetUnitServerId();

    auto& zoneUnitManager = g_ZoneUnitManager;

    if (const auto spTargetPos =
        zoneUnitManager.GetHeroUnitSharedData(targetUnitServerId).GetPosition())
    {
        const auto targetPos = *spTargetPos;
        const auto myPos = pOwner->GetPos();

        // TODO : FIX
        constexpr auto RANDOM_DEGREE = 90.f;
        constexpr auto MIN_RANGE = ONE_METER * 2;
        constexpr auto APPROACH_DISTANCE = ONE_METER * 10;

        if (APPROACH_DISTANCE > FVector::Distance(myPos, targetPos))
        {
            const auto unitVec = FVector(myPos - targetPos).GetSafeNormal();
            const auto randomAngleDeg =
                g_RandomEngine.GetRandom<float>(-RANDOM_DEGREE, RANDOM_DEGREE);
            const auto unitVecRotated =
                unitVec.RotateAngleAxis(randomAngleDeg, FVector::UpVector);
            const auto movePos = targetPos + unitVecRotated * MIN_RANGE;

            pOwner->Move(movePos);
            return Result::Success;
        }

        pOwner->Move(targetPos - FVector(targetPos - myPos).GetSafeNormal() * MIN_RANGE);
        return Result::Success;
    }

    pOwner->RemoveAggro(targetUnitServerId);
    return Result::Failure;
}

IMPLEMENT_NODE(MoveToAttackPos, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    if (pOwner->GetMonsterAI().IsMoveBlock())
    {
        return Result::Success;
    }

    const auto targetUnitServerId = pOwner->GetAggroTargetUnitServerId();

    auto& zoneUnitManager = g_ZoneUnitManager;

    if (const auto spTargetPos =
        zoneUnitManager.GetHeroUnitSharedData(targetUnitServerId).GetPosition())
    {
        const auto targetPos = *spTargetPos;
        const auto myPos = pOwner->GetPos();

        // TODO : FIX
        constexpr auto randomDegree = 60.f;
        constexpr auto minRange = ONE_METER * 2;
        constexpr auto APPROACH_DISTANCE = ONE_METER * 10;

        const auto nextSkill = spAiData->mNextSkillId;
        const auto skillController = pOwner->GetMonsterAI().GetSkillController();
        auto skillRange = skillController->GetSkillRange(nextSkill);

        if (skillRange < minRange)
        {
            skillRange = minRange;
        }

        constexpr auto allowance = 10.f;

        if (APPROACH_DISTANCE > FVector::Distance(myPos, targetPos))
        {
            const auto unitVec = FVector(myPos - targetPos).GetSafeNormal();
            const auto randomAngleDeg =
                g_RandomEngine.GetRandom<float>(-randomDegree, randomDegree);
            const auto unitVecRotated =
                unitVec.RotateAngleAxis(randomAngleDeg, FVector::UpVector);
            const auto movePos = targetPos + unitVecRotated * (skillRange - allowance);

            pOwner->Move(movePos);
            return Result::Success;
        }

        const auto movePos = targetPos - FVector(targetPos - myPos).GetSafeNormal()
            * (skillRange - allowance);

        pOwner->Move(movePos);
        return Result::Success;
    }

    pOwner->RemoveAggro(targetUnitServerId);
    return Result::Failure;
}

IMPLEMENT_NODE(AttackTarget, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    const auto targetUnitServerId = pOwner->GetAggroTargetUnitServerId();
    pOwner->UseSkill(spAiData->mNextSkillId, targetUnitServerId);

    return Result::Success;
}

IMPLEMENT_NODE(InCombat, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    if (pOwner->HasAggroTarget())
    {
        return mSpChild->Update();
    }

    return Result::Failure;
}

IMPLEMENT_NODE(CanAttack, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    const auto targetUnitServerId = pOwner->GetAggroTargetUnitServerId();

    auto& zoneUnitManager = g_ZoneUnitManager;

    // ActionBlock Check
    if (pOwner->GetMonsterAI().IsActionBlock())
    {
        return Result::Failure;
    }

    // ExcludedTargeting Check
    if (const auto spTargetStatContainer =
        zoneUnitManager.GetHeroUnitSharedData(targetUnitServerId).GetStatContainer())
    {
        const auto& targetStatContainer = *spTargetStatContainer;
        const StatConditionId statConditionId(EGameX_StatConditionType::IsExcludedTargeting);

        if (targetStatContainer.FindStatCondition(statConditionId))
        {
            return Result::Failure;
        }
    }

    // Skill Range Check
    if (const auto spTargetPos =
        zoneUnitManager.GetHeroUnitSharedData(targetUnitServerId).GetPosition())
    {
        const auto& targetPos = *spTargetPos;
        const auto currentPos = pOwner->GetPos();

        const auto skillController = pOwner->GetMonsterAI().GetSkillController();
        const auto nextSkillId = skillController->GetAvailableSkillId();
        spAiData->mNextSkillId = nextSkillId;
        const auto skillRange = skillController->GetSkillRange(nextSkillId);

        const auto distance = FVector::Distance(targetPos, currentPos);

        if (distance <= skillRange)
        {
            return mSpChild->Update();
        }

        return Result::Failure;
    }

    pOwner->RemoveAggro(targetUnitServerId);
    return Result::Failure;
}

IMPLEMENT_NODE(FindLeaderMonster, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    const auto mapName = pOwner->GetMapName(); // Do not modify.(const T&)
    const auto monsterUnitServerId = pOwner->GetUnitServerId(); // Do not modify.(const T&)

    auto& zoneNavigation = g_ZoneNavigation;

    const auto sightAABB = pOwner->GetSightAABB(); // Do not modify.(const T&)
    const auto sightUnitServerIds =
        zoneNavigation.GetOverlappingWithCollision(mapName, { sightAABB }); // Do not modify.(const T&)

    auto& gameDataTable = g_GameDataTable;

    for (const auto& sightUnitServerId : sightUnitServerIds)
    {
        if (!Faction::IsHostile(gameDataTable, monsterUnitServerId, sightUnitServerId)
            && sightUnitServerId != monsterUnitServerId)
        {
            UnitScopeLock<ZoneUnitLock> unitScopeLock({ sightUnitServerId });

            if (ZoneMonster& zoneMonster = *ZoneUnitLock::GetZoneMonster(sightUnitServerId);
                &zoneMonster)
            {
                if (zoneMonster.GetCharacterUnitType() == +EGameX_UnitType::BossMonster)
                {
                    auto leaderAIData = zoneMonster.GetMonsterAI().GetBehaviorTree()->GetAIData();

                    // TODO : FIX
                    if (leaderAIData->mMemberMonsterIds.size() > 8)
                    {
                        return Result::Failure;
                    }

                    spAiData->mLeaderId = sightUnitServerId;

                    leaderAIData->mMemberMonsterIds.emplace_back(monsterUnitServerId);

                    spAiData->mMemberNumber =
                        static_cast<int>(leaderAIData->mMemberMonsterIds.size());

                    return Result::Success;
                }
            }
        }
    }

    return Result::Failure;
}

IMPLEMENT_NODE(InGroupMove, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    if (!spAiData->mLeaderId.IsInvalid())
    {
        return mSpChild->Update();
    }

    return Result::Failure;
}

IMPLEMENT_NODE(FormationMove, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    auto& zoneUnitManager = g_ZoneUnitManager;
    auto& formationManager = g_FormationManager;

    if (const auto spLeaderPos =
        zoneUnitManager.GetMonsterUnitSharedData(spAiData->mLeaderId).GetPosition())
    {
        if (const auto spLeaderYaw =
            zoneUnitManager.GetMonsterUnitSharedData(spAiData->mLeaderId).GetRotationYaw())
        {

            const auto movePos = formationManager.GetPosition(EGameX_FormationType::DualVertical,
                spAiData->mMemberNumber, *spLeaderPos, *spLeaderYaw);

            pOwner->Move(movePos);

            return Result::Success;
        }
    }

    spAiData->mLeaderId = UnitServerId::INVALID;
    return Result::Success;
}

IMPLEMENT_NODE(LowHp, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    if (const auto maxHp = pOwner->GetMaxHp())
    {
        if (pOwner->GetHp() <= maxHp.value() * 0.3)
        {
            return mSpChild->Update();
        }

        return Result::Failure;
    }

    return Result::Failure;
}

IMPLEMENT_NODE(RunAway, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    const auto targetUnitServerId = pOwner->GetAggroTargetUnitServerId();

    auto& zoneUnitManager = g_ZoneUnitManager;

    if (const auto spTargetPos =
        zoneUnitManager.GetHeroUnitSharedData(targetUnitServerId).GetPosition())
    {
        // TODO : FIX
        constexpr auto RUN_AWAY_DISTANCE = ONE_METER * 10;

        const auto myPos = pOwner->GetPos();
        const auto movePos = myPos +
            FVector(myPos - *spTargetPos).GetSafeNormal() * RUN_AWAY_DISTANCE;

        pOwner->Move(movePos);
        return Result::Success;
    }

    pOwner->RemoveAggro(targetUnitServerId);
    return Result::Success;
}

IMPLEMENT_NODE(TargetAliveCheck, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    auto& zoneUnitManager = g_ZoneUnitManager;
    const auto targetUnitServerId = pOwner->GetAggroTargetUnitServerId();

    if (const auto spTargetState =
        zoneUnitManager.GetHeroUnitSharedData(targetUnitServerId).GetUnitState())
    {
        const auto type = spTargetState->GetGroupType();

        if (type != +EGameX_UnitStateGroup::Dead)
        {
            return mSpChild->Update();
        }

        pOwner->RemoveAggro(targetUnitServerId);
        return Result::Failure;
    }

    pOwner->RemoveAggro(targetUnitServerId);
    return Result::Failure;
}

IMPLEMENT_NODE(IsTargetClose, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    auto& zoneUnitManager = g_ZoneUnitManager;
    const auto targetUnitServerId = pOwner->GetAggroTargetUnitServerId();

    if (const auto spTargetPos =
        zoneUnitManager.GetHeroUnitSharedData(targetUnitServerId).GetPosition())
    {
        const auto distance = FVector::Distance(pOwner->GetPos(), *spTargetPos);

        // TODO : FIX
        constexpr auto DODGE_RANGE = ONE_METER * 5;

        if (distance <= DODGE_RANGE)
        {
            return mSpChild->Update();
        }

        return Result::Failure;
    }

    pOwner->RemoveAggro(targetUnitServerId);
    return Result::Failure;
}

IMPLEMENT_NODE(BeFarAway, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    auto& zoneUnitManager = g_ZoneUnitManager;
    const auto targetUnitServerId = pOwner->GetAggroTargetUnitServerId();

    if (const auto spTargetPos =
        zoneUnitManager.GetHeroUnitSharedData(targetUnitServerId).GetPosition())
    {
        // TODO : FIX
        constexpr auto FAR_AWAY_DISTANCE = ONE_METER * 10;

        const auto myPos = pOwner->GetPos();
        const auto movePos = myPos +
            FVector(myPos - *spTargetPos).GetSafeNormal() * FAR_AWAY_DISTANCE;

        pOwner->Move(movePos);

        spAiData->mLastMoved = std::chrono::steady_clock::now();

        return Result::Success;
    }

    pOwner->RemoveAggro(targetUnitServerId);
    return Result::Success;
}

IMPLEMENT_NODE(DoNothing, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;
    return Result::Success;
}

IMPLEMENT_NODE(ReturnToSpawnedPos, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    if (pOwner->GetMonsterAI().IsMoveBlock())
    {
        return Result::Success;
    }

    const auto spawnedPos = pOwner->GetSpawnedPos();

    pOwner->ResetAggro();
    pOwner->Move(spawnedPos);

    return Result::Success;
}

IMPLEMENT_NODE(IsFarFromSpawnedPos, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    const auto currentPos = pOwner->GetPos();
    const auto spawnedPos = pOwner->GetSpawnedPos();

    const float returnDistance = GetParameter<1>();

    const auto distance = FVector::Distance(currentPos, spawnedPos);

    if (distance >= returnDistance)
    {
        const auto returning = MonsterTrigger::returning;
        if (pOwner->CanFire(returning))
        {
            pOwner->Fire(returning);

            const auto statRegenerator = pOwner->GetMonsterAI().GetStatRegenerator();
            statRegenerator->SetReturningStatRegen();
        }

        return mSpChild->Update();
    }

    return Result::Failure;
}

IMPLEMENT_NODE(CheckIfReturned, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    const auto currentPos = pOwner->GetPos();
    const auto spawnedPos = pOwner->GetSpawnedPos();

    const auto distance = FVector::Distance(currentPos, spawnedPos);

    constexpr auto allowance = 50.f;

    if (distance <= allowance)
    {
        const auto rest = MonsterTrigger::rest;
        if (pOwner->CanFire(rest))
        {
            pOwner->Fire(rest);

            const auto statRegenerator = pOwner->GetMonsterAI().GetStatRegenerator();
            statRegenerator->SetStatRegenDefault();
        }
    }

    return Result::Success;
}

IMPLEMENT_NODE(IsReturning, ZoneMonster, MonsterAIData)
{
    BT_LOG_DEBUG;

    if (pOwner->IsReturningState())
    {
        return mSpChild->Update();
    }

    return Result::Failure;
}

} // namespace nxn
