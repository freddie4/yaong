﻿#pragma once
#include "unit_ai.h"

namespace nxn
{

class ZoneMonster;
class BehaviorTree;
class AggroManager;
class AIMoveController;
class AISkillController;
class StatRegenerator;

class MonsterAI : public UnitAI
{
public:
    explicit MonsterAI(ZoneMonster& zoneMonster);
    ~MonsterAI() override;

    bool Initialize();
    void Update(const msec& delta) override;

    void OnRestState();
    void OnCombatState();

    bool IsMoveBlock() const;
    bool IsRotateBlock() const;
    bool IsActionBlock() const;

    void SetBehaviorTree(const std::string& treeName);

    auto GetBehaviorTree() const { return mSpBehaviorTree; }
    auto GetMoveController() const { return mpMoveController; }
    auto GetSkillController() const { return mpSkillController; }
    auto GetStatRegenerator() const { return mpStatRegenerator; }
    auto GetAggroManager() const { return mpAggroManager; }

    void SetSpawnedPos(const FVector& pos);
    FVector GetSpawnedPos() const;

private:
    ZoneMonster& mZoneMonster;
    AggroManager* mpAggroManager = nullptr;
    AIMoveController* mpMoveController = nullptr;
    AISkillController* mpSkillController = nullptr;
    StatRegenerator* mpStatRegenerator = nullptr;

    // TODO : Up to UnitAI
    std::shared_ptr<BehaviorTree> mSpBehaviorTree = nullptr;

    FVector mSpawnedPos;
};

} // namespace nxn
