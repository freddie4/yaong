﻿#pragma once
#include "unit_server_id.h"
#include "unit_move_vector.h"

namespace nxn
{
class Exec;
class InstanceKey;
class ZoneMonster;

class AISkillController
{
    using SteadyTimePoint = std::chrono::time_point<std::chrono::steady_clock>;
    inline static const std::chrono::milliseconds ACTIVE_SKILL_COOLDOWN
        = std::chrono::milliseconds(20000);

public:
    explicit AISkillController(ZoneMonster& zoneMonster);
    ~AISkillController() = default;

    void UseSkill(const SkillId skillId, const UnitServerId& unitServerId);
    SkillId GetAvailableSkillId() const;

    float GetSkillRange(const SkillId skillId) const;

private:
    bool IsUsingSkill() const;
    bool IsGlobalCooldown() const;
    bool IsSkillCooldown(const SkillId skillId) const;
    std::optional<std::vector<Exec>> MakeExecs(const SkillId skillId,
        const InstanceKey& instanceKey, const FVector& casterPos,
        const FVector& destPos, const UnitServerId& targetUnitServerId);
    void SetCooldown(const SkillId skillId, const SteadyTimePoint& timePoint);
    void SetGlobalCooldown(const SteadyTimePoint& timePoint);
    void SetSkillUsingDelay(const SkillId skillId, const SteadyTimePoint& timePoint);
    void GetExecCollisionTargetUnitMoveVectors(const ExecId& execId,
        const FVector& sourcePos, const FVector& destPos,
        const UnitServerId& ignoreUnitServerId,
        const UnitServerId& targetUnitServerId,
        UnitMoveVectors& targetUnitMoveVectors);
	
    ZoneMonster& mZoneMonster;

    uint32 mInstanceIndex;

    std::pair<SteadyTimePoint, std::chrono::milliseconds> mSkillUsingDelay;
    SteadyTimePoint mLastSkillUsed;
    const std::chrono::milliseconds mGlobalCooldownTime;

    nxn_unordered_map<SkillId, SteadyTimePoint> mSkillCooldownMap;
};

} // namespace nxn
