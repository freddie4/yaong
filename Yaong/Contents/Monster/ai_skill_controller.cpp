﻿#include "stdafx.h"
#include "ai_skill_controller.h"

#include "behavior_tree.h"
#include "exec.h"
#include "faction.h"
#include "game_data_util.h"
#include "zone_server_app.h"

namespace nxn
{

AISkillController::AISkillController(ZoneMonster& zoneMonster)
    : mZoneMonster(zoneMonster)
    , mInstanceIndex(0)
    , mSkillUsingDelay({ std::chrono::steady_clock::now(), std::chrono::milliseconds(0) })
    , mLastSkillUsed(std::chrono::steady_clock::now())
    , mGlobalCooldownTime(std::chrono::milliseconds(3000))
{
}

void AISkillController::UseSkill(const SkillId skillId, const UnitServerId& unitServerId)
{
    if (IsUsingSkill() || IsGlobalCooldown()
        || unitServerId.IsInvalid() || skillId == SkillId(0))
    {
        return;
    }

	if(IsSkillCooldown(skillId))
	{
        return;
	}

    auto& zoneUnitManager = g_ZoneUnitManager;

    if (const auto spTargetPos =
        zoneUnitManager.GetHeroUnitSharedData(unitServerId).GetPosition())
    {
        const auto& targetPos = *spTargetPos;

        const InstanceKey instanceKey(EGameX_InstanceKey::FGameX_SkillInstanceKey, ++mInstanceIndex);
        const auto myPos = mZoneMonster.GetPos();

        const auto now = std::chrono::steady_clock::now();

        SetGlobalCooldown(now);
        SetCooldown(skillId, now);
        SetSkillUsingDelay(skillId, now);

        mZoneMonster.PlaySkill(skillId, instanceKey, myPos, targetPos, unitServerId);

        if (const auto execs = MakeExecs(skillId, instanceKey, myPos, targetPos, unitServerId))
        {
            mZoneMonster.ExecuteExecs(execs.value());
        }
    }
}

SkillId AISkillController::GetAvailableSkillId() const
{
    const auto monsterId = mZoneMonster.GetMonsterId();
    const auto characterData = GetCharacterData(monsterId, g_GameDataTable);
    if (!characterData)
    {
        LOG_ERROR(LOG_FILTER_MONSTER_AI, "Failed to get characterData - MonsterId - {}", monsterId.Get());
        return SkillId(0);
    }

    const auto characterSkillSetId = characterData.value().CharacterSkillSetID;
    const auto skillSet = GetSkillSet(characterSkillSetId, g_GameDataTable);
    if (!skillSet)
    {
        LOG_ERROR(LOG_FILTER_MONSTER_AI,
            "Failed to get SkillSet - CharacterSkillSetID - {}", characterSkillSetId);
        return SkillId(0);
    }

    // ActiveSkills
    /*for (const auto& skillId : skillSet.value().activeSkills)
    {
        const auto skillData = GetSkillData(SkillId(skillId), g_GameDataTable);
        if (!skillData)
        {
            return SkillId(0);
        }

        if (!IsSkillCooldown(SkillId(skillId)))
        {
            return SkillId(skillId);
        }
    }*/

    // BaseSkill
    return SkillId(skillSet.value().baseSkill);
}

float AISkillController::GetSkillRange(const SkillId skillId) const
{
    const auto monsterId = mZoneMonster.GetMonsterId();
    const auto characterData = GetCharacterData(monsterId, g_GameDataTable);
    if (!characterData)
    {
        LOG_ERROR(LOG_FILTER_MONSTER_AI, "Failed to get characterData - MonsterId - {}", monsterId.Get());
        return 0.f;
    }

    const auto characterSkillSetId = characterData.value().CharacterSkillSetID;
    const auto skillSet = GetSkillSet(characterSkillSetId, g_GameDataTable);
    if (!skillSet)
    {
        LOG_ERROR(LOG_FILTER_MONSTER_AI,
            "Failed to get SkillSet - CharacterSkillSetID - {}", characterSkillSetId);
        return 0.f;
    }

    const auto skillData = GetSkillData(skillId, g_GameDataTable);
    if (!skillData)
    {
        LOG_ERROR(LOG_FILTER_MONSTER_AI,
            "Failed to get SkillData - skillId - {}", skillId.Get());
        return 0.f;
    }

    return skillData.value().useableDistance;
}

bool AISkillController::IsUsingSkill() const
{
    return std::chrono::duration_cast<std::chrono::milliseconds>
        (std::chrono::steady_clock::now() - mSkillUsingDelay.first) < mSkillUsingDelay.second;
}

bool AISkillController::IsGlobalCooldown() const
{
    return std::chrono::duration_cast<std::chrono::milliseconds>
        (std::chrono::steady_clock::now() - mLastSkillUsed) < mGlobalCooldownTime;
}

bool AISkillController::IsSkillCooldown(const SkillId skillId) const
{
    const auto skillCooldown = ACTIVE_SKILL_COOLDOWN;
    const auto found = mSkillCooldownMap.find(skillId);
    if (found == mSkillCooldownMap.end())
    {
        return false;
    }

    if (std::chrono::duration_cast<std::chrono::milliseconds>
        (std::chrono::steady_clock::now() - found->second) < skillCooldown)
    {
        return true;
    }

    return false;
}

std::optional<std::vector<Exec>> AISkillController::MakeExecs(const SkillId skillId,
    const InstanceKey& instanceKey,
    const FVector& casterPos,
    const FVector& destPos,
    const UnitServerId& targetUnitServerId)
{
    if (const auto oSkillData = GetSkillData(skillId, g_GameDataTable))
    {
        const auto skillData = oSkillData.value();
        const auto now = GetTimestamp64();

        static_assert(skillData.execTableIDs.max_size() == skillData.execPreDelays.max_size(),
            "must be same size.");

        std::vector<Exec> execs;

        const auto casterUnitServerId(mZoneMonster.GetUnitServerId());

        for (auto i = 0; i < skillData.execTableIDs.max_size(); ++i)
        {
            ExecId execId(skillData.execTableIDs[i]);

            UnitMoveVector casterMoveVector(casterUnitServerId, casterPos, casterPos);

            UnitMoveVectors targetUnitMoveVectors;
            GetExecCollisionTargetUnitMoveVectors(execId, casterPos,
                destPos, casterUnitServerId, targetUnitServerId, targetUnitMoveVectors);

            const Timestamp64 execTimeStamp64 =
                AddTime(now, msec(static_cast<int64>(skillData.execPreDelays[i] * 1000)));

            Exec exec(execId, instanceKey, false,
                casterMoveVector, targetUnitMoveVectors,
                casterPos, destPos, execTimeStamp64);

            if (exec.IsValid())
            {
                execs.emplace_back(exec);
            }
        }

        return execs;
    }

    return {};
}

void AISkillController::SetGlobalCooldown(const SteadyTimePoint& timePoint)
{
    mLastSkillUsed = timePoint;
}

void AISkillController::SetCooldown(const SkillId skillId, const SteadyTimePoint& timePoint)
{
    if (const auto characterData = GetCharacterData(mZoneMonster.GetMonsterId(), g_GameDataTable))
    {
        if (const auto skillSet = GetSkillSet(characterData.value().CharacterSkillSetID, g_GameDataTable))
        {
            if (skillId.Get() == skillSet.value().baseSkill)
            {
                return;
            }
        }
    }

    mSkillCooldownMap[skillId] = timePoint;
}

void AISkillController::SetSkillUsingDelay(const SkillId skillId, const SteadyTimePoint& timePoint)
{
    if (const auto characterData = GetCharacterData(mZoneMonster.GetMonsterId(), g_GameDataTable))
    {
        if (const auto skillSet = GetSkillSet(characterData.value().CharacterSkillSetID, g_GameDataTable))
        {
            // TODO : Get Skill Motion Delay
            mSkillUsingDelay = { timePoint, std::chrono::milliseconds(2000) };
        }
    }
}

void AISkillController::GetExecCollisionTargetUnitMoveVectors(const ExecId& execId,
    const FVector& sourcePos,
    const FVector& destPos,
    const UnitServerId& ignoreUnitServerId,
    const UnitServerId& targetUnitServerId,
    UnitMoveVectors& targetUnitMoveVectors)
{
    using namespace NxnExt_CollisionUtil;

    auto& gameDataTable = g_GameDataTable;
    auto& zoneUnitManager = g_ZoneUnitManager;
    auto& zoneNavigation = g_ZoneNavigation;

    gamedata::ExecData execData;
    if (!gameDataTable.Get(execId.Get(), execData))
        return;

    gamedata::ExecCollision execCollision;
    if (!gameDataTable.Get(execData.execCollisionID, execCollision))
        return;

    if (destPos.IsNormalized())
    {
        LOG_ERROR(LOG_FILTER_MONSTER_AI, "Exec DestPosition must not be normalized."
            " execId: {}, {}",
            execId, destPos);
        return;
    }

    // MoveToCasterFront
    FVector additiveDelta(FVector::ZeroVector);
    if ((+EGameX_ExecType::MoveToCasterFront) == execData.GetEGameX_ExecType_execType())
    {
        const FVector& directionVector(destPos - sourcePos);
        additiveDelta = execData.execValue2 * directionVector.GetSafeNormal();
    }

    const std::wstring& mapName(mZoneMonster.GetMapName());
    const UnitServerId& monsterUnitServerId(mZoneMonster.GetUnitServerId());

    FBox aabb;

    switch (execCollision.GetEGameX_ExecCollisionShapeType_shapeType())
    {
    case EGameX_ExecCollisionShapeType::Box:
    {
        const FVector& centerPos(execCollision.spawnPosition);
        const FVector extent(execCollision.shapeValue1, execCollision.shapeValue2,
            execCollision.shapeValue3);
        const FRotator& rotation(execCollision.spawnRotation.Rotation());

        const FNxnExt_OrientBoxDesc obb(centerPos, extent, rotation);
        aabb = GetAABBFromOrientBoxDesc(obb, sourcePos, destPos);

        const auto targetUnitServerIds =
            zoneNavigation.GetOverlappingWithCollision(mapName, { aabb });

        for (const auto& unitServerId : targetUnitServerIds)
        {
            if (unitServerId == ignoreUnitServerId)
                continue;

            if (!Faction::IsCasterEnemy(gameDataTable, monsterUnitServerId, unitServerId))
                continue;

            auto targetPos = zoneUnitManager.GetHeroUnitSharedData(unitServerId).GetPosition();
            if (!targetPos)
                continue;
			
            if (PointOrientBox_Intersection(*targetPos - sourcePos, obb))
            {
				*targetPos = *targetPos + additiveDelta;
                targetUnitMoveVectors.emplace(unitServerId, sourcePos, *targetPos);
            }
        }
    } break;

    case EGameX_ExecCollisionShapeType::Cylinder:
    {
        const FVector& centerPos(execCollision.spawnPosition);
        const float fRadius = execCollision.shapeValue1;
        const float fHalfHeight = execCollision.shapeValue2;

        const FNxnExt_CylinderDesc cylinder(centerPos, fRadius, fHalfHeight);
        aabb = GetAABBFromCylinderDesc(cylinder, centerPos, destPos);

        const auto targetUnitServerIds =
            zoneNavigation.GetOverlappingWithCollision(mapName, { aabb });

        for (const auto& unitServerId : targetUnitServerIds)
        {
            if (unitServerId == ignoreUnitServerId)
                continue;

            if (!Faction::IsCasterEnemy(gameDataTable, monsterUnitServerId, unitServerId))
                continue;

            auto targetPos =
                zoneUnitManager.GetHeroUnitSharedData(unitServerId).GetPosition();
            if (!targetPos)
                continue;

            *targetPos = *targetPos + additiveDelta;

            if (PointCylinder_Intersection(*targetPos - sourcePos, cylinder))
            {
                targetUnitMoveVectors.emplace(unitServerId, sourcePos, *targetPos);
            }
        }
    } break;

    case EGameX_ExecCollisionShapeType::Arc:
    {
        const FVector& centerPos(execCollision.spawnPosition);
        const FRotator& rotation(execCollision.spawnRotation.Rotation());
        const float fOuterRadius = execCollision.shapeValue1;
        const float fInnerRadius = execCollision.shapeValue2;
        const float fAngle = execCollision.shapeValue3;
        const float fHalfHeight = execCollision.shapeValue4;

        const FNxnExt_ArcDesc arc(centerPos, rotation.Quaternion().GetForwardVector(),
            fInnerRadius, fOuterRadius, fHalfHeight, fAngle);
        aabb = GetAABBFromArcDesc(arc, sourcePos, destPos);

        const auto targetUnitServerIds =
            zoneNavigation.GetOverlappingWithCollision(mapName, { aabb });

        for (const auto& unitServerId : targetUnitServerIds)
        {
            if (unitServerId == ignoreUnitServerId)
                continue;

            if (!Faction::IsCasterEnemy(gameDataTable, monsterUnitServerId, unitServerId))
                continue;

            auto targetPos = zoneUnitManager.GetHeroUnitSharedData(unitServerId).GetPosition();
            if (!targetPos)
                continue;

            *targetPos = *targetPos + additiveDelta;

            if (PointArc_Intersection(*targetPos - sourcePos, arc))
            {
                targetUnitMoveVectors.emplace(unitServerId, sourcePos, *targetPos);
            }
        }
    } break;

    case EGameX_ExecCollisionShapeType::OneUnit:
    {
        switch (execCollision.GetEGameX_ExecCollisionActionType_actionType())
        {
        case EGameX_ExecCollisionActionType::FollowToCaster:
        {
            targetUnitMoveVectors.emplace(mZoneMonster.GetUnitServerId(), sourcePos, sourcePos);
        } break;

        case EGameX_ExecCollisionActionType::FollowToTarget:
        {
            if (const auto spTargetPos =
                zoneUnitManager.GetHeroUnitSharedData(targetUnitServerId).GetPosition())
            {
                *spTargetPos = *spTargetPos + additiveDelta;

                targetUnitMoveVectors.emplace(targetUnitServerId, *spTargetPos, *spTargetPos);
            }
        } break;

        case EGameX_ExecCollisionActionType::SpawnedByCaster:
        {
            targetUnitMoveVectors.emplace(mZoneMonster.GetUnitServerId(), sourcePos, sourcePos);
        } break;

        case EGameX_ExecCollisionActionType::SpawnedByTarget:
        {
            if (const auto spTargetPos =
                zoneUnitManager.GetHeroUnitSharedData(targetUnitServerId).GetPosition())
            {
                *spTargetPos = *spTargetPos + additiveDelta;

                targetUnitMoveVectors.emplace(targetUnitServerId, *spTargetPos, *spTargetPos);
            }
        } break;

        default:
            LOG_ERROR(LOG_FILTER_MONSTER_AI, "Invalid ExecCollisionActionType : {}",
                execCollision.GetEGameX_ExecCollisionActionType_actionType()._to_string());
            break;
        } // switch

    } break;

    default:
    {
        LOG_ERROR(LOG_FILTER_MONSTER_AI, "ExecCollision ShapeType is not exist. {}",
            (+execCollision.GetEGameX_ExecCollisionShapeType_shapeType())._to_string());
    } break;

    } // switch
}

} // namespace nxn
