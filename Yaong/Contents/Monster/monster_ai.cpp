﻿#include "stdafx.h"
#include "monster_ai.h"

#include "ai_aggro_manager.h"
#include "ai_move_controller.h"
#include "ai_skill_controller.h"
#include "ai_stat_regenerator.h"
#include "behavior_tree.h"
#include "behavior_tree_manager.h"
#include "zone_server_app.h"

namespace nxn
{

MonsterAI::MonsterAI(ZoneMonster& zoneMonster)
    : mZoneMonster(zoneMonster)
{
}

MonsterAI::~MonsterAI()
{
    delete mpAggroManager;
    delete mpMoveController;
    delete mpSkillController;
    delete mpStatRegenerator;
}

bool MonsterAI::Initialize()
{
    mpAggroManager = new AggroManager(mZoneMonster);
    mpMoveController = new AIMoveController(mZoneMonster);
    mpSkillController = new AISkillController(mZoneMonster);
    mpStatRegenerator = new StatRegenerator(mZoneMonster);

    if (const auto characterUnitType = mZoneMonster.GetCharacterUnitType())
    {
        switch(characterUnitType.value())
        {
        case EGameX_UnitType::Monster:
            SetBehaviorTree("BT_MeleeMonster");
            break;

        case EGameX_UnitType::LeadMonster:
            SetBehaviorTree("BT_LeaderMonster");
            break;

        case EGameX_UnitType::BossMonster:
            SetBehaviorTree("BT_LeaderMonster");
            break;

        default:
            LOG_ERROR(LOG_FILTER_MONSTER_AI,
                "There is no behaviorTree correspond with CharacterUnitType. {}",
                characterUnitType.value()._to_string());
            return false;
        }
    }

    return true;
}

void MonsterAI::Update(const msec& delta)
{
    if (mZoneMonster.IsActiveState())
    {
        if (!IsMoveBlock() && !IsRotateBlock())
        {
            mpMoveController->Update(delta);
        }

        mpStatRegenerator->Update(delta);

        if (mSpBehaviorTree)
        {
			mSpBehaviorTree->SingleRun();            
        }
    }
}

void MonsterAI::OnRestState()
{
    mZoneMonster.Fire(MonsterTrigger::rest);

    LOG_DEBUG(LOG_FILTER_MONSTER_AI, "MonsterTrigger rest. {}", mZoneMonster.GetUnitServerId());
}

void MonsterAI::OnCombatState()
{
    mZoneMonster.Fire(MonsterTrigger::combat);

    LOG_DEBUG(LOG_FILTER_MONSTER_AI, "MonsterTrigger combat. {}", mZoneMonster.GetUnitServerId());
}

bool MonsterAI::IsMoveBlock() const
{
    if (!mZoneMonster.IsActiveAIState())
        return true;

    const auto& statContainer = mZoneMonster.GetStatContainer();
    if (statContainer.FindStatCondition(StatConditionId(EGameX_StatConditionType::IsMoveBlock)))
        return true;

    return false;
}

bool MonsterAI::IsRotateBlock() const
{
    if (!mZoneMonster.IsActiveAIState())
        return true;

    const auto& statContainer = mZoneMonster.GetStatContainer();
    if (statContainer.FindStatCondition(StatConditionId(EGameX_StatConditionType::IsRotateBlock)))
        return true;

    return false;
}

bool MonsterAI::IsActionBlock() const
{
    if (!mZoneMonster.IsActiveAIState())
        return true;

    const auto& statContainer = mZoneMonster.GetStatContainer();
    if (statContainer.FindStatCondition(StatConditionId(EGameX_StatConditionType::IsActionBlock)))
        return true;

    return false;
}

void MonsterAI::SetBehaviorTree(const std::string& treeName)
{
    auto& zoneBehaviorTreeManager = g_ZoneBehaviorTreeManager;

    mSpBehaviorTree = zoneBehaviorTreeManager.GetBehaviorTree(treeName);

    if (mSpBehaviorTree)
    {
        const auto spAiData = std::make_shared<MonsterAIData>();

        mSpBehaviorTree->SetOwner(&mZoneMonster);
        mSpBehaviorTree->SetAIData(spAiData);
    }
}
void MonsterAI::SetSpawnedPos(const FVector& pos)
{
    mSpawnedPos = pos;
}

FVector MonsterAI::GetSpawnedPos() const
{
    return mSpawnedPos;
}

} // namespace nxn
