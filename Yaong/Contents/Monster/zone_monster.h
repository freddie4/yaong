﻿#pragma once
#include "monster_ai.h"
#include "monster_unit.h"
#include "zone_monster_statemachine.h"

namespace nxn
{
enum class Formation;
enum class AISkill;

class ZoneMonster : public MonsterUnit
{
public:
    explicit ZoneMonster(const MonsterUnitData& unitData);
    ~ZoneMonster() override = default;

    std::optional<EGameX_UnitType> GetCharacterUnitType();
    std::optional<int64> GetMaxHp();
    MonsterAI& GetMonsterAI() { return mMonsterAI; }

    void Move(const FVector& pos);
    void UseSkill(const SkillId skillId, const UnitServerId& unitServerId);
    SkillId GetAvailableSkillId() const;

    UnitServerId GetAggroTargetUnitServerId() const;
    void AddAggro(const UnitServerId& unitServerId, const AggroPoint aggroPoint);
    void RemoveAggro(const UnitServerId& unitServerId);
    void ResetAggro();
    bool HasAggroTarget() const;

    void SetBehaviorTree(const std::string& treeName);

    void From(const pb_gameX::MonsterUnitData& unitData) override;

    void SetPos(const Timestamp64& timestamp64, const FVector& pos) override;
    void SetSpawnedPos(const FVector& pos);
    FVector GetSpawnedPos() const { return mMonsterAI.GetSpawnedPos(); };

    bool Initialize() override;
    void Update(const msec& delta) override;

    void PlaySkill(const SkillId& skillId, const InstanceKey& instanceKey,
        const FVector& casterPos, const FVector& destPos,
        const UnitServerId& targetServerId);
    void ExecuteExecs(const std::vector<Exec>& execs);

    Uuid GetUuid() const;

    StatContainer& GetStatContainer();
    const StatContainer& GetStatContainer() const;

    MonsterState GetState() const;

    bool IsActiveState() const;
    bool IsRestState() const;
    bool IsMovingState() const;
    bool IsCombatableState() const;
    bool IsActiveAIState() const;
    bool IsDeadState() const;
    bool IsCombatState() const;
    bool IsReturningState() const;

    void Fire(const MonsterTrigger& trigger);
    bool CanFire(const MonsterTrigger& trigger) const;

    void UpdateState();

private:
    MonsterAI mMonsterAI;
    ZoneMonsterStateMachine mStateMachine;
};

} // namespace nxn
