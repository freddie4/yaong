﻿#pragma once

namespace nxn
{

BETTER_ENUM(EGameX_FormationType, uint32, \
    None, \
    SurroundTarget, \
    NarrowRect, \
    Rect, \
    Diamond, \
    Triangle, \
    ReverseTriangle, \
    Vic, \
    CraneWing, \
    DualVertical);

} // namespace nxn

template <>
struct std::hash<nxn::EGameX_FormationType>
    : public boost::functional::detail::unary_function<nxn::EGameX_FormationType, std::size_t>
{
    typedef nxn::EGameX_FormationType _Kty;

    std::size_t operator()(const _Kty& _Keyval) const
    {
        return std::hash<uint32>()(_Keyval._to_integral());
    }
};

template <>
struct boost::hash<nxn::EGameX_FormationType>
    : public boost::functional::detail::unary_function<nxn::EGameX_FormationType, std::size_t>
{
    typedef nxn::EGameX_FormationType _Kty;

    std::size_t operator()(const _Kty& _Keyval) const
    {
        return boost::hash<uint32>()(_Keyval._to_integral());
    }
};

namespace nxn
{

class AIFormationManager
{
public:
    AIFormationManager();
    ~AIFormationManager() = default;

    FVector GetPosition(const EGameX_FormationType& formationType, const int index,
        const FVector& pos, const float rotationYaw = 0.0f) const;

private:
    void Initialize();

    FVector GetDirectionVector(const EGameX_FormationType& formationType,
        const int index) const;

    void BuildSurroundTarget();
    void BuildNarrowRect();
    void BuildRect();
    void BuildDiamond();
    void BuildTriangle();
    void BuildReverseTriangle();
    void BuildVic();
    void BuildCraneWing();
    void BuildDualVertical();

    const FVector mDirectionFront;
    const FVector mDirectionBack;
    const FVector mDirectionLeft;
    const FVector mDirectionRight;

    nxn_unordered_map<EGameX_FormationType, std::vector<FVector>> mFormations;
};

} // namespace nxn
