﻿#pragma once

namespace nxn
{

class BehaviorTree;

using TreeName = std::string;
using TreeHashId = MapHashId;
using TreeMaker = std::function<std::shared_ptr<BehaviorTree>()>;

class BehaviorTreeManager
{
public:
    BehaviorTreeManager();
    virtual ~BehaviorTreeManager() = default;

    void Initialize();
    void RegisterTreeMaker(const TreeName& treeName, const TreeMaker& treeMaker);

    std::shared_ptr<BehaviorTree> GetBehaviorTree(const TreeName& treeName);

    void Clear();

private:
    nxn_unordered_map<TreeHashId, TreeMaker> mTreeFactory;
};

} // namespace nxn

#define g_ZoneBehaviorTreeManager SINGLETON_STATIC(nxn::BehaviorTreeManager)
