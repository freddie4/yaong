﻿#pragma once

namespace nxn
{

class ZoneMonster;

class StatRegenerator
{
public:
    explicit StatRegenerator(ZoneMonster& monster);
    virtual ~StatRegenerator() = default;

    int32 GetHpRegen() const { return mHpRegen; }
    void SetHpRegen(const int32 hpRegen) { mHpRegen = hpRegen; }

    msec GetHpRegenInterval() const { return mHpRegenInterval; }
    void SetHpRegenInterval(const msec msec) { mHpRegenInterval = msec; }

    void SetReturningStatRegen();
    void SetStatRegenDefault();

    void Update(const msec delta);

private:
    ZoneMonster& mMonster;

    int32 mHpRegen;

    msec mAccumulatedDelta;
    msec mHpRegenInterval;
};

}