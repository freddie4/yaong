﻿#include "stdafx.h"
#include "ai_formation_manager.h"

namespace nxn
{

AIFormationManager::AIFormationManager()
    : mDirectionFront(FVector::ForwardVector)
    , mDirectionBack(-FVector::ForwardVector)
    , mDirectionLeft(-FVector::RightVector)
    , mDirectionRight(FVector::RightVector)
{
    Initialize();
}

void AIFormationManager::Initialize()
{
    BuildSurroundTarget();
    BuildNarrowRect();
    BuildRect();
    BuildDiamond();
    BuildTriangle();
    BuildReverseTriangle();
    BuildVic();
    BuildCraneWing();
    BuildDualVertical();
}

FVector AIFormationManager::GetPosition(const EGameX_FormationType& formationType,
    const int index, const FVector& pos, const float rotationYaw /*= 0.0f*/) const
{
    constexpr float distance = ONE_METER * 1.5;
    const FRotator rotation(0.0f, rotationYaw, 0.0f);

    const FVector& directionVector = GetDirectionVector(formationType, index);
    const FVector& rotatedDirectionVector = rotation.RotateVector(directionVector);
    const FVector& relationPos = distance * rotatedDirectionVector;

    return pos + relationPos;
}

FVector AIFormationManager::GetDirectionVector(const EGameX_FormationType& formationType,
    const int index) const
{
    FVector directionVector(FVector::ZeroVector);

    auto it = mFormations.find(formationType);
    if (it != mFormations.end())
    {
        const auto& directions = it->second;

        if (directions.empty() || index >= directions.size())
            return directionVector;

        directionVector = directions.at(index);
    }

    return directionVector;
}

void AIFormationManager::BuildSurroundTarget()
{
    // ---------------------
    // |   |   |   |   |   |
    // ---------------------
    // |   | 6 | 3 | 7 |   |
    // ---------------------
    // |   | 1 | X | 2 |   |
    // ---------------------
    // |   | 4 | 0 | 5 |   |
    // ---------------------
    // |   |   |   |   |   |
    // ---------------------
    std::vector<FVector> directions;
    directions.reserve(8);
    directions.emplace_back(mDirectionBack * 1.0f); // 0
    directions.emplace_back(mDirectionLeft * 1.0f); // 1
    directions.emplace_back(mDirectionRight * 1.0f); // 2
    directions.emplace_back(mDirectionFront * 1.0f); // 3
    directions.emplace_back((mDirectionBack * 1.0f) + (mDirectionLeft * 1.0f)); // 4
    directions.emplace_back((mDirectionBack * 1.0f) + (mDirectionRight * 1.0f)); // 5
    directions.emplace_back((mDirectionFront * 1.0f) + (mDirectionLeft * 1.0f)); // 6
    directions.emplace_back((mDirectionFront * 1.0f) + (mDirectionRight * 1.0f)); // 7

    for (auto& direction : directions)
    {
        direction = direction.GetSafeNormal();
    }

    mFormations.emplace(EGameX_FormationType::SurroundTarget, std::move(directions));
}

void AIFormationManager::BuildNarrowRect()
{
    // -------------
    // | 0 | 4 | 1 |
    // -------------
    // | 7 | X | 5 |
    // -------------
    // | 3 | 6 | 2 |
    // -------------
    std::vector<FVector> directions;
    directions.reserve(8);
    directions.emplace_back((mDirectionFront * 1.0f) + (mDirectionLeft * 1.0f)); // 0
    directions.emplace_back((mDirectionFront * 1.0f) + (mDirectionRight * 1.0f)); // 1
    directions.emplace_back((mDirectionBack * 1.0f) + (mDirectionRight * 1.0f)); // 2
    directions.emplace_back((mDirectionBack * 1.0f) + (mDirectionLeft * 1.0f)); // 3
    directions.emplace_back(mDirectionFront * 1.0f); // 4
    directions.emplace_back(mDirectionRight * 1.0f); // 5
    directions.emplace_back(mDirectionBack * 1.0f); // 6
    directions.emplace_back(mDirectionLeft * 1.0f); // 7

    mFormations.emplace(EGameX_FormationType::NarrowRect, std::move(directions));
}

void AIFormationManager::BuildRect()
{
    // ---------------------
    // | 0 |   | 4 |   | 1 |
    // ---------------------
    // |   |   |   |   |   |
    // ---------------------
    // | 7 |   | X |   | 5 |
    // ---------------------
    // |   |   |   |   |   |
    // ---------------------
    // | 3 |   | 6 |   | 2 |
    // ---------------------
    std::vector<FVector> directions;
    directions.reserve(8);
    directions.emplace_back((mDirectionFront * 2.0f) + (mDirectionLeft * 2.0f)); // 0
    directions.emplace_back((mDirectionFront * 2.0f) + (mDirectionRight * 2.0f)); // 1
    directions.emplace_back((mDirectionBack * 2.0f) + (mDirectionRight * 2.0f)); // 2
    directions.emplace_back((mDirectionBack * 2.0f) + (mDirectionLeft * 2.0f)); // 3
    directions.emplace_back(mDirectionFront * 2.0f); // 4
    directions.emplace_back(mDirectionRight * 2.0f); // 5
    directions.emplace_back(mDirectionBack * 2.0f); // 6
    directions.emplace_back(mDirectionLeft * 2.0f); // 7

    mFormations.emplace(EGameX_FormationType::Rect, std::move(directions));
}

void AIFormationManager::BuildDiamond()
{
    // ---------------------
    // |   |   | 0 |   |   |
    // ---------------------
    // |   | 7 |   | 4 |   |
    // ---------------------
    // | 3 |   | X |   | 1 |
    // ---------------------
    // |   | 6 |   | 5 |   |
    // ---------------------
    // |   |   | 2 |   |   |
    // ---------------------
    std::vector<FVector> directions;
    directions.reserve(8);
    directions.emplace_back(mDirectionFront * 2.0f); // 0
    directions.emplace_back(mDirectionRight * 2.0f); // 1
    directions.emplace_back(mDirectionBack * 2.0f); // 2
    directions.emplace_back(mDirectionLeft * 2.0f); // 3
    directions.emplace_back(mDirectionFront + mDirectionRight); // 4
    directions.emplace_back(mDirectionBack + mDirectionRight); // 5
    directions.emplace_back(mDirectionBack + mDirectionLeft); // 6
    directions.emplace_back(mDirectionFront + mDirectionLeft); // 7

    mFormations.emplace(EGameX_FormationType::Diamond, std::move(directions));
}

void AIFormationManager::BuildTriangle()
{
    // -----------------------------
    // |   |   |   | 0 |   |   |   |
    // -----------------------------
    // |   |   | 7 |   | 5 |   |   |
    // -----------------------------
    // |   | 4 |   | X |   | 3 |   |
    // -----------------------------
    // | 2 |   |   | 6 |   |   | 1 |
    // -----------------------------
    std::vector<FVector> directions;
    directions.reserve(8);
    directions.emplace_back(mDirectionFront * 2.0f); // 0
    directions.emplace_back(mDirectionBack + (mDirectionRight * 3.0f)); // 1
    directions.emplace_back(mDirectionBack + (mDirectionLeft * 3.0f)); // 2
    directions.emplace_back(mDirectionRight * 2.0f); // 3
    directions.emplace_back(mDirectionLeft * 2.0f); // 4
    directions.emplace_back(mDirectionFront + mDirectionRight); // 5
    directions.emplace_back(mDirectionBack); // 6
    directions.emplace_back(mDirectionFront + mDirectionLeft); // 7

    mFormations.emplace(EGameX_FormationType::Triangle, std::move(directions));
}

void AIFormationManager::BuildReverseTriangle()
{
    // -----------------------------
    // | 5 |   |   | 0 |   |   | 3 |
    // -----------------------------
    // |   | 7 |   | X |   | 6 |   |
    // -----------------------------
    // |   |   | 2 |   | 1 |   |   |
    // -----------------------------
    // |   |   |   | 4 |   |   |   |
    // -----------------------------
    std::vector<FVector> directions;
    directions.reserve(8);
    directions.emplace_back(mDirectionFront); // 0
    directions.emplace_back(mDirectionBack + mDirectionRight); // 1
    directions.emplace_back(mDirectionBack + mDirectionLeft); // 2
    directions.emplace_back(mDirectionFront + (mDirectionRight * 3.0f)); // 3
    directions.emplace_back(mDirectionBack * 2.0f); // 4
    directions.emplace_back(mDirectionFront + (mDirectionLeft * 3.0f)); // 5
    directions.emplace_back(mDirectionRight * 2.0f); // 6
    directions.emplace_back(mDirectionLeft * 2.0f); // 7

    mFormations.emplace(EGameX_FormationType::ReverseTriangle, std::move(directions));
}

void AIFormationManager::BuildVic()
{
    // -------------------------------------
    // |   |   |   |   | X |   |   |   |   |
    // -------------------------------------
    // |   |   |   | 1 |   | 0 |   |   |   |
    // -------------------------------------
    // |   |   | 3 |   |   |   | 2 |   |   |
    // -------------------------------------
    // |   | 5 |   |   |   |   |   | 4 |   |
    // -------------------------------------
    // | 7 |   |   |   |   |   |   |   | 6 |
    // -------------------------------------
    std::vector<FVector> directions;
    directions.reserve(8);
    directions.emplace_back((mDirectionBack + mDirectionRight) * 1.0f); // 0
    directions.emplace_back((mDirectionBack + mDirectionLeft) * 1.0f); // 1
    directions.emplace_back((mDirectionBack + mDirectionRight) * 2.0f); // 2
    directions.emplace_back((mDirectionBack + mDirectionLeft) * 2.0f); // 3
    directions.emplace_back((mDirectionBack + mDirectionRight) * 3.0f); // 4
    directions.emplace_back((mDirectionBack + mDirectionLeft) * 3.0f); // 5
    directions.emplace_back((mDirectionBack + mDirectionRight) * 4.0f); // 6
    directions.emplace_back((mDirectionBack + mDirectionLeft) * 4.0f); // 7

    mFormations.emplace(EGameX_FormationType::Vic, std::move(directions));
}

void AIFormationManager::BuildCraneWing()
{
    // -------------------------------------
    // | 7 |   |   |   |   |   |   |   | 6 |
    // -------------------------------------
    // |   | 5 |   |   |   |   |   | 4 |   |
    // -------------------------------------
    // |   |   | 3 |   |   |   | 2 |   |   |
    // -------------------------------------
    // |   |   |   | 1 |   | 0 |   |   |   |
    // -------------------------------------
    // |   |   |   |   | X |   |   |   |   |
    // -------------------------------------
    std::vector<FVector> directions;
    directions.reserve(8);
    directions.emplace_back((mDirectionFront + mDirectionRight) * 1.0f); // 0
    directions.emplace_back((mDirectionFront + mDirectionLeft) * 1.0f); // 1
    directions.emplace_back((mDirectionFront + mDirectionRight) * 2.0f); // 2
    directions.emplace_back((mDirectionFront + mDirectionLeft) * 2.0f); // 3
    directions.emplace_back((mDirectionFront + mDirectionRight) * 3.0f); // 4
    directions.emplace_back((mDirectionFront + mDirectionLeft) * 3.0f); // 5
    directions.emplace_back((mDirectionFront + mDirectionRight) * 4.0f); // 6
    directions.emplace_back((mDirectionFront + mDirectionLeft) * 4.0f); // 7

    mFormations.emplace(EGameX_FormationType::CraneWing, std::move(directions));
}

void AIFormationManager::BuildDualVertical()
{
    // ---------------------
    // |   |   | X |   |   |
    // ---------------------
    // |   | 1 |   | 0 |   |
    // ---------------------
    // |   | 3 |   | 2 |   |
    // ---------------------
    // |   | 5 |   | 4 |   |
    // ---------------------
    // |   | 7 |   | 6 |   |
    // ---------------------
    std::vector<FVector> directions;
    directions.reserve(8);
    directions.emplace_back((mDirectionBack * 1.0f) + mDirectionRight); // 0
    directions.emplace_back((mDirectionBack * 1.0f) + mDirectionLeft); // 1
    directions.emplace_back((mDirectionBack * 2.0f) + mDirectionRight); // 2
    directions.emplace_back((mDirectionBack * 2.0f) + mDirectionLeft); // 3
    directions.emplace_back((mDirectionBack * 3.0f) + mDirectionRight); // 4
    directions.emplace_back((mDirectionBack * 3.0f) + mDirectionLeft); // 5
    directions.emplace_back((mDirectionBack * 4.0f) + mDirectionRight); // 6
    directions.emplace_back((mDirectionBack * 4.0f) + mDirectionLeft); // 7

    mFormations.emplace(EGameX_FormationType::DualVertical, std::move(directions));
}

} // namespace nxn
