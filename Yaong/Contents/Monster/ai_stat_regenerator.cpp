﻿#include "stdafx.h"
#include "ai_stat_regenerator.h"

#include "game_data_util.h"
#include "p_gameX_zone_server_to_game_server.h"
#include "zone_server_app.h"

namespace nxn
{

StatRegenerator::StatRegenerator(ZoneMonster& monster)
    : mMonster(monster)
    , mHpRegen(0)
{
    SetStatRegenDefault();
}

void StatRegenerator::SetReturningStatRegen()
{
    if (!mMonster.IsReturningState())
    {
        return;
    }

	// TODO : FIX
    constexpr auto returningHpRegen = 300;
    const auto returningHpRegenInterval = msec(500);

    SetHpRegen(returningHpRegen);
    SetHpRegenInterval(returningHpRegenInterval);
}

void StatRegenerator::SetStatRegenDefault()
{
    const auto& gameDataTable = g_GameDataTable;
    const auto monsterId = mMonster.GetMonsterId();

    if (const auto characterStat = GetCharacterStat(monsterId, gameDataTable))
    {
        mHpRegen = characterStat.value().hpRegen;
    }
    else
    {
        LOG_ERROR(LOG_FILTER_MONSTER, "Failed to get characterStat - MonsterId : {}", monsterId.Get());
    }

    const auto defaultHpRegenInterval = msec(1000);
    mHpRegenInterval = defaultHpRegenInterval;
}

void StatRegenerator::Update(const msec delta)
{
    const auto stateGroup = mMonster.GetState().GetGroupType();

    if (stateGroup == +EGameX_UnitStateGroup::Resting
        || stateGroup == +EGameX_UnitStateGroup::Combat)
    {
        mAccumulatedDelta += delta;

        if (mAccumulatedDelta < mHpRegenInterval)
        {
            return;
        }

        mAccumulatedDelta -= mHpRegenInterval;

        const auto hp = mMonster.GetHp();
        const auto maxHp = mMonster.GetMaxHp();

        if (hp == maxHp || hp <= 0)
        {
            return;
        }

        const auto& gameDataTable = g_GameDataTable;
        const auto characterStat = GetCharacterStat(mMonster.GetMonsterId(), gameDataTable);
        if (!characterStat)
        {
            return;
        }

        PZ2S_MonsterStatRegen out;
        auto& pbUnitServerId = *out.mutable_unit_server_id();
        mMonster.GetUnitServerId().To(pbUnitServerId);
        out.set_hp_regen(mHpRegen);

        if (const auto spGameServerSession = g_spGameServerSession)
        {
            spGameServerSession->SendPacket(out);
        }
    }
}

}
