﻿#pragma once
#include "unit_server_id.h"

namespace nxn
{
class ZoneMonster;

class AggroManager
{
public:
    explicit AggroManager(ZoneMonster& zoneMonster);
    ~AggroManager() = default;

    struct AggroTag
    {
        AggroTag(const UnitServerId& unitServerId, const AggroPoint aggroPoint)
            : mUnitServerId(unitServerId), mAggroPoint(aggroPoint) {}

        UnitServerId mUnitServerId;
        AggroPoint mAggroPoint;
    };

    struct IndexUnitServerId {};
    struct IndexAggro {};

    using AggroMultiIndex =
        boost::multi_index::multi_index_container<AggroTag, boost::multi_index::indexed_by<

        // IndexUnitServerId
        boost::multi_index::hashed_unique
        <
        boost::multi_index::tag<IndexUnitServerId>,
        boost::multi_index::member<AggroTag, UnitServerId, &AggroTag::mUnitServerId>
        >,

        // IndexAggro
        boost::multi_index::ordered_non_unique
        <
        boost::multi_index::tag<IndexAggro>,
        boost::multi_index::member<AggroTag, AggroPoint, &AggroTag::mAggroPoint>
        >

        >>;

    bool HasAggroTarget() const;

    UnitServerId GetAggroTarget() const;

    AggroPoint GetAggro(const UnitServerId& unitServerId) const;
    float GetAggroPercent(const UnitServerId& unitServerId) const;
    const AggroMultiIndex& GetAggroList() const { return mAggroMultiIndex; }

    void AddAggro(const UnitServerId& unitServerId, const AggroPoint aggroPoint);
    void AddAggro(const UnitServerId& unitServerId, const SkillId skillId,
        const uint32 value);

    void TauntedBy(const UnitServerId& unitServerId);

    void RemoveAggro(const UnitServerId& unitServerId);

    void ResetAggro();

private:
    AggroPoint GetSumOfAllAggros() const;
    std::optional<AggroPoint> FindAggroPoint(const UnitServerId& unitServerId) const;
    void AddAggroPoint(const UnitServerId& unitServerId, const AggroPoint aggroPoint);
    void UpdateAggroPoint(const UnitServerId& unitServerId, const AggroPoint aggroPoint);

    ZoneMonster& mZoneMonster;
    AggroMultiIndex mAggroMultiIndex;
    uint32 mTauntedCount;
};

} // namespace nxn
