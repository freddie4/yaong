﻿#include "stdafx.h"
#include "ai_aggro_manager.h"

#include "zone_monster.h"

namespace nxn
{

AggroManager::AggroManager(ZoneMonster& zoneMonster)
    : mZoneMonster(zoneMonster)
    , mTauntedCount(0)
{
}

bool AggroManager::HasAggroTarget() const
{
    if (!mAggroMultiIndex.empty())
    {
        const auto combat = MonsterTrigger::combat;
        if (mZoneMonster.CanFire(combat))
        {
            mZoneMonster.Fire(combat);
        }
        return true;
    }

    const auto rest = MonsterTrigger::rest;
    if (mZoneMonster.CanFire(rest))
    {
        mZoneMonster.Fire(rest);
    }
    return false;
}

UnitServerId AggroManager::GetAggroTarget() const
{
    if (!mAggroMultiIndex.empty())
    {
        return (*mAggroMultiIndex.begin()).mUnitServerId;
    }

    return UnitServerId::INVALID;
}

AggroPoint AggroManager::GetAggro(const UnitServerId& unitServerId) const
{
    if (const auto aggro = FindAggroPoint(unitServerId))
    {
        return aggro.value();
    }
    return AggroPoint(0);
}

float AggroManager::GetAggroPercent(const UnitServerId& unitServerId) const
{
    if (const auto aggro = FindAggroPoint(unitServerId))
    {
        const auto aggroSum = GetSumOfAllAggros();

        if (aggroSum == AggroPoint(0))
        {
            return 0.0f;
        }

        return std::roundf(static_cast<float>(aggro.value().Get())
            / static_cast<float>(aggroSum.Get()) * 100);
    }

    return 0.0f;
}

void AggroManager::AddAggro(const UnitServerId& unitServerId,
    const AggroPoint aggroPoint)
{
    AddAggroPoint(unitServerId, aggroPoint);
}

void AggroManager::AddAggro(const UnitServerId& unitServerId,
    const SkillId skillId,
    const uint32 value)
{
    // TODO : Calculate AggroPoint by skillId, value
    AddAggroPoint(unitServerId, AggroPoint(value));
}

void AggroManager::TauntedBy(const UnitServerId& unitServerId)
{
    if (!mAggroMultiIndex.empty())
    {
        const auto tauntedAggroPoint = (*mAggroMultiIndex.begin()).mAggroPoint + AggroPoint(1);
        UpdateAggroPoint(unitServerId, tauntedAggroPoint);
    }
    else
    {
        AddAggroPoint(unitServerId, AggroPoint(1));
    }
}

void AggroManager::RemoveAggro(const UnitServerId& unitServerId)
{
    mAggroMultiIndex.erase(unitServerId);
}

void AggroManager::ResetAggro()
{
    mAggroMultiIndex.clear();
}

AggroPoint AggroManager::GetSumOfAllAggros() const
{
    auto aggroSum = AggroPoint(0);
    for (const auto& aggroData : mAggroMultiIndex)
    {
        aggroSum += aggroData.mAggroPoint;
    }

    return aggroSum;
}

std::optional<AggroPoint> AggroManager::FindAggroPoint(const UnitServerId& unitServerId) const
{
    auto& index = mAggroMultiIndex.get<IndexUnitServerId>();
    if (const auto& it = index.find(unitServerId); it != index.end())
    {
        return it->mAggroPoint;
    }

    return {};
}

void AggroManager::AddAggroPoint(const UnitServerId& unitServerId,
    const AggroPoint aggroPoint)
{
    auto& index = mAggroMultiIndex.get<IndexUnitServerId>();
    if (const auto& it = index.find(unitServerId); it != index.end())
    {
        mAggroMultiIndex.modify(it, [aggroPoint](AggroTag& tag)
        {
            tag.mAggroPoint += aggroPoint;
        });
    }
    else
    {
        mAggroMultiIndex.emplace(unitServerId, aggroPoint);
    }
}

void AggroManager::UpdateAggroPoint(const UnitServerId& unitServerId,
    const AggroPoint aggroPoint)
{
    auto& index = mAggroMultiIndex.get<IndexUnitServerId>();
    if (const auto& it = index.find(unitServerId); it != index.end())
    {
        mAggroMultiIndex.modify(it, [aggroPoint](AggroTag& tag)
        {
            tag.mAggroPoint += aggroPoint;
        });
    }
    else
    {
        mAggroMultiIndex.emplace(unitServerId, aggroPoint);
    }
}

} // namespace nxn
