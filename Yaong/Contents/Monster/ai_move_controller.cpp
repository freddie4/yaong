﻿#include "stdafx.h"
#include "ai_move_controller.h"

#include "navigation.h"
#include "p_gameX_zone_server_to_game_server.h"
#include "pb_convert_util.h"
#include "zone_monster.h"
#include "zone_server_app.h"

namespace nxn
{

AIMoveController::AIMoveController(ZoneMonster& zoneMonster)
    : mZoneMonster(zoneMonster)
    , mIsActive(true)
    , mIsChangedEndPos(false)
    , mEndPos()
    , mFollowPath()
    , mMoveAccumulatedDelta()
    , mMoveAccumulatedDistance()
{
}

bool AIMoveController::Update(const msec& delta)
{
    if (!mIsActive)
        return false;

    auto& zoneNavigation = g_ZoneNavigation;

    mMoveAccumulatedDelta += delta;
    const float moveSpeed = mZoneMonster.GetMoveSpeed();
    const float deltaDistance = static_cast<float>(delta.Get() * 0.001 * moveSpeed);
    mMoveAccumulatedDistance += deltaDistance;

    const msec updateInterval(250);
    if (mMoveAccumulatedDelta < updateInterval)
        return false;

    mMoveAccumulatedDelta -= updateInterval;

    if (!mEndPos.is_initialized())
        return false;

    const UnitServerId monsterUnitServerId(mZoneMonster.GetUnitServerId());
    const std::wstring mapName(mZoneMonster.GetMapName());
    const FVector currentPos(mZoneMonster.GetPos());

    if (mFollowPath.empty() || mIsChangedEndPos)
    {
        std::vector<FVector> path;
        if (!zoneNavigation.FindFollowPath(mapName, currentPos, mEndPos.value(), path))
        {
            LOG_ERROR(LOG_FILTER_MONSTER_AI, "Fail to find follow path."
                " {}, currentPos: {}, endPos: {}",
                monsterUnitServerId, currentPos, mEndPos.value());
            return false;
        }

        mFollowPath.swap(path);

        mMoveAccumulatedDelta = delta;
        mMoveAccumulatedDistance = deltaDistance;
        mIsChangedEndPos = false;
    }

    FVector movePos(FVector::ZeroVector);
    bool isArrived = false;
    if (!GetPositionFromFollowPath(mMoveAccumulatedDistance, movePos, isArrived))
    {
        LOG_ERROR(LOG_FILTER_MONSTER_AI, "Fail to find position from follow path.");
        ASSERT(false);
        return false;
    }

    if (isArrived)
    {
        mEndPos.reset();
    }

    auto spGameServerSession = g_spGameServerSession;

    const Timestamp64 currentTimestamp64 = GetTimestamp64();
    mZoneMonster.SetPos(currentTimestamp64, movePos);

    const FBox collisionAABB(mZoneMonster.GetCollisionAABB());
    const FBox sightAABB(mZoneMonster.GetSightAABB());
    const FVector displacement(movePos - currentPos);
    zoneNavigation.UpdateUnit(mapName, monsterUnitServerId, collisionAABB, sightAABB, displacement);

    {
        PZ2S_Move out;
        auto& pbOutUnitMove = *out.mutable_unit_move();
        auto& pbUnitServerId = *pbOutUnitMove.mutable_unit_server_id();
        monsterUnitServerId.To(pbUnitServerId);

        auto& pbMove = *pbOutUnitMove.mutable_move();
        auto& pbMoveVector = *pbMove.mutable_move_vector();
        ToPbMoveVector(currentPos, movePos, pbMoveVector);
        ASSERT(!pbMoveVector.is_dest_pos_normalized());

        pbMove.set_speed(moveSpeed);
        pbMove.set_timestamp64(currentTimestamp64.Get());
        spGameServerSession->SendPacket(out);
    }
    
    return true;
}

void AIMoveController::SetActive()
{
    mIsActive = true;
}

void AIMoveController::SetDisable()
{
    mIsActive = false;
}

void AIMoveController::SetEndPos(const FVector& endPos)
{
    if (mEndPos == endPos)
        return;

    mIsChangedEndPos = true;
    mEndPos = endPos;
}

bool AIMoveController::GetPositionFromFollowPath(const float distance, FVector& pos,
    bool& isArrived) const
{
    if (mFollowPath.empty())
        return false;

    const float totalDistance = STEP_SIZE * mFollowPath.size();
    if (distance >= totalDistance)
    {
        pos = mFollowPath[mFollowPath.size() - 1];
        isArrived = true;
        return true;
    }

    const uint32 currentStepIndex = static_cast<uint32>(std::floor(distance / STEP_SIZE));
    pos = mFollowPath[currentStepIndex];
    isArrived = false;
    return true;
}

} // namespace nxn