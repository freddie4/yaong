﻿#pragma once
#include "unit_server_id.h"

namespace nxn
{

class AIData
{
public:
    AIData() = default;
    virtual ~AIData() = default;
};

class MonsterAIData : public AIData
{
    using SteadyTimePoint = std::chrono::time_point<std::chrono::steady_clock>;

public:
    MonsterAIData() = default;
    ~MonsterAIData() override = default;

    std::vector<UnitServerId> mMemberMonsterIds;
    int32 mMemberNumber = 0;

    SteadyTimePoint mLastMoved = std::chrono::steady_clock::now();

    UnitServerId mLeaderId;

    SkillId mNextSkillId = SkillId(0);
};

} // namespace nxn
