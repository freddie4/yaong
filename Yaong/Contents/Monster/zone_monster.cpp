﻿#include "stdafx.h"
#include "zone_monster.h"

#include "ai_aggro_manager.h"
#include "ai_skill_controller.h"
#include "ai_move_controller.h"
#include "exec.h"
#include "p_gameX_zone_server_to_game_server.h"
#include "pb_convert_util.h"
#include "zone_server_app.h"

namespace nxn
{

ZoneMonster::ZoneMonster(const MonsterUnitData& unitData)
    : MonsterUnit(unitData)
    , mMonsterAI(*this)
    , mStateMachine(*this)
{
}

void ZoneMonster::From(const pb_gameX::MonsterUnitData& unitData)
{
    __super::From(unitData);

    auto& zoneUnitManager = g_ZoneUnitManager;

    const UnitServerId& unitServerId = GetUnitServerId();

    MonsterUnitSharedData* pMonsterUnitSharedData = nullptr;
    if (zoneUnitManager.GetMonsterUnitSharedData(unitServerId, pMonsterUnitSharedData))
    {
        MonsterUnitSharedData& unitSharedData = *pMonsterUnitSharedData;
        ToSharedData(unitSharedData);
    }
}

void ZoneMonster::SetPos(const Timestamp64& timestamp64, const FVector& pos)
{
    __super::SetPos(timestamp64, pos);

    auto& zoneUnitManager = g_ZoneUnitManager;

    const UnitServerId& unitServerId = GetUnitServerId();

    MonsterUnitSharedData* pMonsterUnitSharedData = nullptr;
    if (zoneUnitManager.GetMonsterUnitSharedData(unitServerId, pMonsterUnitSharedData))
    {
        pMonsterUnitSharedData->SetPosition(pos);
    }
}

void ZoneMonster::SetSpawnedPos(const FVector& pos)
{
    mMonsterAI.SetSpawnedPos(pos);
}

bool ZoneMonster::Initialize()
{
    {
        const auto& trigger = MonsterTrigger::init_data;
        if (!mStateMachine.CanFire(trigger))
            return false;

        mStateMachine.Fire(trigger);
    }

    if (!mMonsterAI.Initialize())
        return false;

    {
        const auto& trigger = MonsterTrigger::spawn;
        if (!mStateMachine.CanFire(trigger))
            return false;

        mStateMachine.Fire(trigger);
    }

    return true;
}

void ZoneMonster::Update(const msec& delta)
{
    if (!IsActiveState())
        return;

    mMonsterAI.Update(delta);
}

void ZoneMonster::PlaySkill(const SkillId& skillId, const InstanceKey& instanceKey,
    const FVector& casterPos, const FVector& destPos, const UnitServerId& targetServerId)
{
    UnitServerId unitServerId(GetUnitServerId());

    PZ2S_PlaySkill out;
    auto& pbUnitServerId = *out.mutable_unit_server_id();
    unitServerId.To(pbUnitServerId);

    auto& pbSkillToDest = *out.mutable_skill_to_dest();
    pbSkillToDest.set_skill_id(skillId.Get());

    auto& pbInstanceKey = *pbSkillToDest.mutable_instancekey();
    instanceKey.To(pbInstanceKey);

    const FVector& direction = FVector(destPos - casterPos).GetSafeNormal();

    auto& pbMoveVector = *pbSkillToDest.mutable_move_vector();
    ToPbMoveVector(casterPos, direction, pbMoveVector);
    ASSERT(pbMoveVector.is_dest_pos_normalized());

    auto& pbTargetServerId = *pbSkillToDest.mutable_target_server_id();
    targetServerId.To(pbTargetServerId);

    const Timestamp64& timestamp64 = GetTimestamp64();
    pbSkillToDest.set_timestamp64(timestamp64.Get());

    g_spGameServerSession->SendPacket(out);
}

void ZoneMonster::ExecuteExecs(const std::vector<Exec>& execs)
{
    const UnitServerId unitServerId(GetUnitServerId());

    PZ2S_ExecuteExec out;
    auto& pbUnitServerId = *out.mutable_unit_server_id();
    unitServerId.To(pbUnitServerId);

    auto& pbExecs = *out.mutable_execs();
    for (const auto& exec : execs)
    {
        auto& pbExec = *pbExecs.Add();
        exec.To(pbExec);
    }

    g_spGameServerSession->SendPacket(out);
}

Uuid ZoneMonster::GetUuid() const
{
    return mUnitData.GetMonsterUnitData().GetUuid();
}

StatContainer& ZoneMonster::GetStatContainer()
{
    MonsterUnitData* pUnitData;
    if (!mUnitData.GetMonsterUnitData(pUnitData))
    {
        ASSERT(false);
    }

    return pUnitData->GetStatContainer();
}

const StatContainer& ZoneMonster::GetStatContainer() const
{
    return mUnitData.GetMonsterUnitData().GetStatContainer();
}

MonsterState ZoneMonster::GetState() const
{
    return mStateMachine.GetState();
}

std::optional<EGameX_UnitType> ZoneMonster::GetCharacterUnitType()
{
    auto& gameDataTable = g_GameDataTable;

    gamedata::CharacterData characterData;
    if(gameDataTable.Get(GetMonsterId().Get(), characterData))
    {
        return characterData.GetEGameX_UnitType_CharacterUnitType();
    }

    LOG_ERROR(LOG_FILTER_MONSTER_AI, "Failed to Get GameData");
    return {};
}

std::optional<int64> ZoneMonster::GetMaxHp()
{
    auto& gameDataTable = g_GameDataTable;

    gamedata::CharacterData characterData;
    if (!gameDataTable.Get(GetMonsterId().Get(), characterData))
    {
        return {};
    }

    gamedata::CharacterStat characterStat;
    if (gameDataTable.Get(characterData.CharacterStatID, characterStat))
    {
        return characterStat.HP;
    }

    LOG_ERROR(LOG_FILTER_MONSTER_AI, "Failed to Get GameData");
    return {};
}

void ZoneMonster::Move(const FVector& pos)
{
    mMonsterAI.GetMoveController()->SetEndPos(pos);
}

void ZoneMonster::UseSkill(const SkillId skillId,
	const UnitServerId& unitServerId)
{
    mMonsterAI.GetSkillController()->UseSkill(skillId, unitServerId);
}

SkillId ZoneMonster::GetAvailableSkillId() const
{
    return mMonsterAI.GetSkillController()->GetAvailableSkillId();
}

UnitServerId ZoneMonster::GetAggroTargetUnitServerId() const
{
    return mMonsterAI.GetAggroManager()->GetAggroTarget();
}

void ZoneMonster::AddAggro(const UnitServerId& unitServerId,
	const AggroPoint aggroPoint)
{
    mMonsterAI.GetAggroManager()->AddAggro(unitServerId, aggroPoint);
}

void ZoneMonster::RemoveAggro(const UnitServerId& unitServerId)
{
    mMonsterAI.GetAggroManager()->RemoveAggro(unitServerId);
}

void ZoneMonster::ResetAggro()
{
    mMonsterAI.GetAggroManager()->ResetAggro();
}

bool ZoneMonster::HasAggroTarget() const
{
    return mMonsterAI.GetAggroManager()->HasAggroTarget();
}

void ZoneMonster::SetBehaviorTree(const std::string& treeName)
{
    mMonsterAI.SetBehaviorTree(treeName);
}

bool ZoneMonster::IsActiveState() const
{
    const auto state = mStateMachine.GetState();
    return IsActive(state);
}

bool ZoneMonster::IsRestState() const
{
    return mStateMachine.IsInState(MonsterState(EGameX_UnitStateResting::Rest));
}

bool ZoneMonster::IsMovingState() const
{
    const auto state = mStateMachine.GetState();
    return IsMoving(state);
}

bool ZoneMonster::IsCombatableState() const
{
    const auto state = mStateMachine.GetState();
    if (!IsCombatable(state))
        return false;

    const StatConditionId statConditionId(EGameX_StatConditionType::IsExcludedTargeting);

    const auto& statContainer = GetStatContainer();
    if (statContainer.FindStatCondition(statConditionId))
        return false;

    return true;
}

bool ZoneMonster::IsActiveAIState() const
{
    auto state = mStateMachine.GetState();
    return IsActiveAI(state);
}

bool ZoneMonster::IsDeadState() const
{
    const auto unitStateGroup = mStateMachine.GetState().GetGroupType();

    return +EGameX_UnitStateGroup::Dead == unitStateGroup;
}

bool ZoneMonster::IsCombatState() const
{
    const auto unitStateGroup = mStateMachine.GetState().GetGroupType();

    return +EGameX_UnitStateGroup::Combat == unitStateGroup;
}

bool ZoneMonster::IsReturningState() const
{
    const auto unitState = mStateMachine.GetState();
    const auto state = EGameX_UnitStateResting::_from_integral(unitState.GetState());

    return state == +EGameX_UnitStateResting::Returning;
}

void ZoneMonster::Fire(const MonsterTrigger& trigger)
{
    mStateMachine.Fire(trigger);
}

bool ZoneMonster::CanFire(const MonsterTrigger& trigger) const
{
    return mStateMachine.CanFire(trigger);
}

void ZoneMonster::UpdateState()
{
    const auto unitState = mUnitData.GetMonsterUnitData().GetUnitState();

    if (mStateMachine.IsInState(MonsterState(EGameX_UnitStateSpawning::Spawn)))
    {
        mStateMachine.Fire(MonsterTrigger::rest);
        return;
    }

    if (mStateMachine.IsInState(MonsterState(EGameX_UnitStateResting::Rest)) &&
        unitState == MonsterState(EGameX_UnitStateCombat::Stun))
    {
        mStateMachine.Fire(MonsterTrigger::stun);
        return;
    }

    if (mStateMachine.IsInState(MonsterState(EGameX_UnitStateCombat::Idle)) &&
        unitState == MonsterState(EGameX_UnitStateCombat::Stun))
    {
        mStateMachine.Fire(MonsterTrigger::stun);
        return;
    }

    if (mStateMachine.IsInState(MonsterState(EGameX_UnitStateCombat::Stun)) &&
        unitState == MonsterState(EGameX_UnitStateCombat::Idle))
    {
        mStateMachine.Fire(MonsterTrigger::idle);
        return;
    }

    if (GetHp() <= 0)
    {
		if(mStateMachine.CanFire(MonsterTrigger::dead))
		{
			mStateMachine.Fire(MonsterTrigger::dead);			
		}
        return;
    }
}

} // namespace nxn
