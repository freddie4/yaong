#pragma once
#include <Network/Packet/PacketHandlerManager.h>
#include <Network/Packet/PacketBuffer.h>
#include <Cryptography/CryptoBase.h>
#include "OverlappedIoContext.h"
#include "SessionManager.h"
#include "IocpExFunction.h"
#include "SessionData.h"
#include <MSWSock.h>

#ifdef USE_IOCP_CORE
namespace yaong::network
{
	constexpr int RECV_BUF_SIZE = 512;
	
	template <typename TSession>
	class Session : public std::enable_shared_from_this<TSession>
	{
	public:
		static PacketHandlerManager<TSession> _packetHandlerManager;

	public:
		explicit Session(const SessionData& sessionData,
			SessionManager<TSession>* sessionManager);
		virtual ~Session();

		virtual bool IsConnected() const { return _isConnected; }

		virtual int GetSessionId() const { return _sessionId; }

		virtual void SendPacket(google::protobuf::Message* message);
		virtual bool PostSend(char* buffer, const size_t bufSize);
		bool Broadcast(char* buffer, const size_t bufSize);

		virtual bool PreReceive();
		virtual bool PostReceive();
		void test();
		virtual void Disconnect(const DisconnectReason dr);
		virtual void DisconnectRequest(const DisconnectReason dr);

		virtual bool Initialize() = 0;

		virtual void SendCompletion(const DWORD transferred) = 0;
		virtual void RecvCompletion(const DWORD transferred) = 0;
		virtual void DisconnectCompletion(const DisconnectReason dr) = 0;

	protected:
		std::atomic_bool _isConnected = false;
		int _sessionId = 0;

		crypto::CryptoBase* _crypto = nullptr;

		SOCKET _socket = 0;
		SOCKADDR_IN _sockAddr = { 0 };

		SessionManager<TSession>* _sessionManager = nullptr;

		PacketBuffer* _recvBuf = nullptr;
		std::shared_ptr<IocpExFunction> _iocpEx = nullptr;
		HANDLE _completionPort = nullptr;
	};

	template <typename TSession>
	Session<TSession>::Session(const SessionData& sessionData,
		SessionManager<TSession>* sessionManager)
		: _sessionId(sessionData._sessionId)
		, _sessionManager(sessionManager)
		, _iocpEx(sessionData._iocpEx)
		, _completionPort(sessionData._completionPort)
	{
		_socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, nullptr, 0, WSA_FLAG_OVERLAPPED);
		_recvBuf = new PacketBuffer(RECV_BUF_SIZE);
	}

	template <typename TSession>
	Session<TSession>::~Session()
	{
		// delete _crypto;
		delete _recvBuf;
	}

	template <typename TSession>
	void Session<TSession>::SendPacket(google::protobuf::Message* message)
	{
	}

	template <typename TSession>
	bool Session<TSession>::PostSend(char* buffer, const size_t bufSize)
	{
		if(!_isConnected)
		{
			return false;
		}

		if(_crypto != nullptr)
		{
			_crypto->Encode(buffer, bufSize);
		}

		DWORD sendBytes = 0;
		DWORD flags = 0;

		auto sendContext = new OverlappedIoContext(
			[this, self = this->shared_from_this()](const std::size_t bytesTransferred)
		{
			if(IsConnected())
			{
				SendCompletion(bytesTransferred);
			}
		});

		sendContext->_wsaBuf.len = static_cast<ULONG>(bufSize);
		sendContext->_wsaBuf.buf = buffer;

		if(SOCKET_ERROR == ::WSASend(_socket, &sendContext->_wsaBuf, 1, &sendBytes, flags,
			sendContext, nullptr))
		{
			const auto error = ::WSAGetLastError();
			if(error != WSA_IO_PENDING)
			{
				LOG_F("WSASend error : {}", error);
				return false;
			}
		}

		return true;
	}

	template <typename TSession>
	bool Session<TSession>::Broadcast(char* buffer, const size_t bufSize)
	{
		_sessionManager->Broadcast();

		return true;
	}

	template <typename TSession>
	bool Session<TSession>::PreReceive()
	{
		if(!_isConnected)
		{
			return false;
		}

		DWORD recvBytes = 0;
		DWORD flags = 0;

		auto preRecvContext = new OverlappedIoContext(
			[this, self = this->shared_from_this()](const std::size_t bytesTransferred)
		{
			if(IsConnected())
			{
				PostReceive();
			}
		});

		if(SOCKET_ERROR == ::WSARecv(_socket, &preRecvContext->_wsaBuf, 1,
			&recvBytes, &flags, preRecvContext, nullptr))
		{
			const auto error = ::WSAGetLastError();
			if(error != WSA_IO_PENDING)
			{
				LOG_F("WSARecv error : {}", error);
				return false;
			}
		}

		return true;
	}

	template <typename TSession>
	bool Session<TSession>::PostReceive()
	{
		if(!_isConnected)
		{
			return false;
		}

		DWORD recvBytes = 0;
		DWORD flags = 0;

		auto recvContext = new OverlappedIoContext(
			[this, self = this->shared_from_this()](const std::size_t bytesTransferred)
		{
			if(!IsConnected())
			{
				LOG_F("Already Disconnected: {}", _sessionId);
				DisconnectRequest(DisconnectReason::CompletionError);
				return;
			}

			if(bytesTransferred == 0)
			{
				LOG_F("Disconnected - 0 bytes transferred : {}", _sessionId);
				DisconnectRequest(DisconnectReason::CompletionError);
				return;
			}

			_recvBuf->Write(bytesTransferred);

			if(_recvBuf->NotProcessedDataSize() < PACKET_HEADER_SIZE)
			{
				return;
			}

			const auto packetBodySize = *reinterpret_cast<unsigned short*>(_recvBuf->Head());
			const auto packetSize = packetBodySize + PACKET_HEADER_SIZE;

			if(static_cast<int>(_recvBuf->NotProcessedDataSize()) < packetSize)
			{
				LOG_F("Packet not received completely");
				PreReceive();
				return;
			}

			if(packetSize > MAX_PACKET_SIZE)
			{
				LOG_F("Packet is too large. Max : {}", MAX_PACKET_SIZE);
				DisconnectRequest(DisconnectReason::CompletionError);
				return;
			}

			std::array<char, MAX_PACKET_SIZE> recvArray = {};

			std::copy(_recvBuf->Head(), _recvBuf->Head() + packetSize, recvArray.data());

			if(_crypto != nullptr)
			{
				_crypto->Decode(recvArray.data() + PACKET_HEADER_SIZE, packetBodySize);
			}

			if(!_packetHandlerManager.Handle(static_cast<TSession&>(*this), std::move(recvArray)))
			{
				LOG_F("Packet Handle Failed Session : {}", _sessionId);
				DisconnectRequest(DisconnectReason::CompletionError);
				return;
			}

			_recvBuf->Remove(packetSize);

			PreReceive();
		});

		recvContext->_wsaBuf.len = static_cast<ULONG>(_recvBuf->RemainSize());
		recvContext->_wsaBuf.buf = _recvBuf->Tail();

		if(SOCKET_ERROR == ::WSARecv(_socket, &recvContext->_wsaBuf, 1,
			&recvBytes, &flags, recvContext, nullptr))
		{
			const auto error = ::WSAGetLastError();
			if(error != WSA_IO_PENDING)
			{
				LOG_F("WSARecv error : {}", error);
				return false;
			}
		}

		return true;
	}

	template <typename TSession>
	void Session<TSession>::Disconnect(const DisconnectReason dr)
	{
		if(!_isConnected)
		{
			return;
		}

		LINGER lingerOption;
		lingerOption.l_onoff = 1;
		lingerOption.l_linger = 0;

		// SO_LINGER : no TCP TIME_WAIT
		if(SOCKET_ERROR == ::setsockopt(_socket, SOL_SOCKET, SO_LINGER, 
			reinterpret_cast<char*>(&lingerOption), sizeof(LINGER)))
		{
			LOG_F("setsockopt linger option error : {}", ::WSAGetLastError());
		}

		LOG_F("Client Disconnected : IP = {}, PORT = {}",
			::inet_ntoa(_sockAddr.sin_addr), ::ntohs(_sockAddr.sin_port));

		::closesocket(_socket);

		_isConnected = false;
	}

	template <typename TSession>
	void Session<TSession>::DisconnectRequest(const DisconnectReason dr)
	{
		auto disconnectContext = new OverlappedIoContext(
		[dr, this, self = this->shared_from_this()](const std::size_t byteTransferred)
		{
			DisconnectCompletion(dr);
		});

		if(false == _iocpEx->DisconnectEx(_socket,
			disconnectContext, TF_REUSE_SOCKET, 0))
		{
			const auto error = WSAGetLastError();
			if(error != WSA_IO_PENDING)
			{
				LOG_F("DisconnectEx error : {}", error);
			}
		}
	}
}
#endif