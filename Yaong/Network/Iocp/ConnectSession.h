#pragma once
#include "Session.h"

#ifdef USE_IOCP_CORE
namespace yaong::network
{
	template <typename TSession>
	class ConnectSession : public Session<TSession>
	{
	public:
		explicit ConnectSession(const SessionData& sessionData,
			SessionManager<TSession>* sessionManager);
		virtual ~ConnectSession();

		bool Initialize() override;
				
		bool Connect();
		virtual bool ConnectCompletion();

		void DisconnectCompletion(const DisconnectReason dr) override;

	private:
		// TODO : Make EndPoint class
		unsigned short _connectPort = 6898;
		std::string _connectIp = "127.0.0.1";
	};

	template <typename TSession>
	ConnectSession<TSession>::ConnectSession(const SessionData& sessionData,
		SessionManager<TSession>* sessionManager)
		: Session<TSession>(sessionData, sessionManager)
	{
	}

	template <typename TSession>
	ConnectSession<TSession>::~ConnectSession()
	{
	}

	template <typename TSession>
	bool ConnectSession<TSession>::Initialize()
	{
		this->_sockAddr.sin_port = ::htons(_connectPort);
		this->_sockAddr.sin_family = AF_INET;
		this->_sockAddr.sin_addr.s_addr = ::inet_addr(_connectIp.data());

		if(SOCKET_ERROR == bind(this->_socket, reinterpret_cast<SOCKADDR*>(&this->_sockAddr)
			, sizeof(this->_sockAddr)))
		{
			LOG_F("bind error");
			return false;
		}

		const auto handle = CreateIoCompletionPort(
			reinterpret_cast<HANDLE>(this->_socket), this->_completionPort,
			reinterpret_cast<ULONG_PTR>(this), 0);

		if(handle != this->_completionPort)
		{
			LOG_F("CreateIoCompletionPort(client<->iocp) error : {}", ::WSAGetLastError());
			return false;
		}

		if(!Connect())
		{
			return false;
		}

		return true;
	}

	template <typename TSession>
	bool ConnectSession<TSession>::Connect()
	{
		if(this->IsConnected())
		{
			return false;
		}

		if(FALSE == ::connect(this->_socket, reinterpret_cast<sockaddr*>
			(&this->_sockAddr), sizeof(this->_sockAddr)))
		{
			LOG_F("Connect Failed : {}", ::WSAGetLastError());
			return false;
		}

		return ConnectCompletion;
	}

	template <typename TSession>
	bool ConnectSession<TSession>::ConnectCompletion()
	{
		if(SOCKET_ERROR == setsockopt(this->_socket, SOL_SOCKET, SO_UPDATE_CONNECT_CONTEXT,
			nullptr, 0))
		{
			LOG_F("SO_UPDATE_CONNECT_CONTEXT failed : {}", ::WSAGetLastError());
			return false;
		}

		int opt = 1;
		if(SOCKET_ERROR == setsockopt(this->_socket, IPPROTO_TCP, TCP_NODELAY, 
			reinterpret_cast<const char*>(&opt), sizeof(int)))
		{
			LOG_F("TCP_NODELAY error: {}", ::WSAGetLastError());
			return false;
		}

		if(!this->PreReceive())
		{
			LOG_F("PreReceive failed : {}", ::WSAGetLastError());
			this->DisconnectRequest(DisconnectReason::CompletionError);
			return false;
		}

		return true;
	}

	template <typename TSession>
	void ConnectSession<TSession>::DisconnectCompletion(const DisconnectReason dr)
	{
		LOG_F("Session Disconnected: Ip={}, Port={}",
			::inet_ntoa(this->_sockAddr.sin_addr), ::ntohs(this->_sockAddr.sin_port));

		while(Connect())
		{
			std::this_thread::sleep_for(std::chrono::seconds(1));
		}
	}
}
#endif