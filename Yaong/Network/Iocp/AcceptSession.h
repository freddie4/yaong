#pragma once
#include "Session.h"

#ifdef USE_IOCP_CORE
namespace yaong::network
{
	template <typename TSession>
	class AcceptSession : public Session<TSession>
	{
	public:
		explicit AcceptSession(const SessionData& sessionData,
			SessionManager<TSession>* sessionManager);
		virtual ~AcceptSession() = default;

		bool Initialize() override;

		bool PostAccept();
		virtual void AcceptCompletion();

		void DisconnectCompletion(const DisconnectReason dr) override;
	
	protected:
		inline static const int ACCEPTEX_OUTPUT_BUFSIZE = 64;
		inline static const DWORD LOCAL_ADDRESS_LENGTH = sizeof(SOCKADDR_IN) + 16;
		inline static const DWORD REMOTE_ADDRESS_LENGTH = sizeof(SOCKADDR_IN) + 16;

		std::array<char, ACCEPTEX_OUTPUT_BUFSIZE> _lpOutputBuffer = { 0 };

		SOCKET _listenSocket = 0;
	};

	template <typename TSession>
	AcceptSession<TSession>::AcceptSession(const SessionData& sessionData,
		SessionManager<TSession>* sessionManager)
		: Session<TSession>(sessionData, sessionManager)
		, _listenSocket(sessionData._listenSocket)
	{
	}

	template <typename TSession>
	bool AcceptSession<TSession>::Initialize()
	{
		return true;
	}

	template <typename TSession>
	bool AcceptSession<TSession>::PostAccept()
	{
		if(this->IsConnected())
		{
			return false;
		}

		auto acceptContext = new OverlappedIoContext(
		[this, self = this->shared_from_this()](const std::size_t bytesTransferred)
		{
			AcceptCompletion();
		});

		DWORD bytes = 0;

		if(FALSE == ::AcceptEx(_listenSocket, this->_socket,
			_lpOutputBuffer.data(), 0, LOCAL_ADDRESS_LENGTH, REMOTE_ADDRESS_LENGTH,
			&bytes, acceptContext))
		{
			const auto error = ::WSAGetLastError();
			if(error != WSA_IO_PENDING)
			{
				LOG_F("AcceptEx error : {}", error);
				return false;
			}
		}

		return true;
	}

	template <typename TSession>
	void AcceptSession<TSession>::AcceptCompletion()
	{
		auto resultOk = true;
		do
		{
			// TODO : Make Socket Option Class
			if(SOCKET_ERROR == ::setsockopt(this->_socket, SOL_SOCKET, SO_UPDATE_ACCEPT_CONTEXT,
				reinterpret_cast<char*>(&_listenSocket), sizeof(SOCKET)))
			{
				LOG_F("SO_UPDATE_ACCEPT_CONTEXT error: {}", ::WSAGetLastError());
				resultOk = false;
				break;
			}

			int opt = 1;
			if(SOCKET_ERROR == ::setsockopt(this->_socket, IPPROTO_TCP, TCP_NODELAY,
				reinterpret_cast<const char*>(&opt), sizeof(int)))
			{
				LOG_F("TCP_NODELAY error: {}", ::WSAGetLastError());
				resultOk = false;
				break;
			}

			int addrlen = sizeof(SOCKADDR_IN);
			if(SOCKET_ERROR == ::getpeername(this->_socket,
				reinterpret_cast<SOCKADDR*>(&this->_sockAddr), &addrlen))
			{
				LOG_F("getpeername error: {}", ::WSAGetLastError());
				resultOk = false;
				break;
			}

			const auto handle = ::CreateIoCompletionPort(
				reinterpret_cast<HANDLE>(this->_socket), this->_completionPort,
				reinterpret_cast<ULONG_PTR>(this), 0);
			if(handle != this->_completionPort)
			{
				const auto error = ::WSAGetLastError();

				if(error == WSA_INVALID_PARAMETER)
				{
					LOG_F("CreateIoCompletionPort already registered : {}", error);
				}
				else
				{
					LOG_F("CreateIoCompletionPort(client<->iocp) error : {}", error);

					resultOk = false;
					break;
				}
			}
		}
		while(false);


		if(!resultOk)
		{
			this->DisconnectRequest(DisconnectReason::CompletionError);
			return;
		}

		LOG_F("Client Connected : Ip={}, Port={}, SessionId={}", 
			inet_ntoa(this->_sockAddr.sin_addr), ntohs(this->_sockAddr.sin_port), this->_sessionId);

		this->_isConnected = true;

		if(!this->PreReceive())
		{
			LOG_F("PreReceive failed : {}", ::WSAGetLastError());
			this->DisconnectRequest(DisconnectReason::CompletionError);
		}
	}

	template <typename TSession>
	void AcceptSession<TSession>::DisconnectCompletion(const DisconnectReason dr)
	{
		LOG_F("Session Disconnected: Ip={}, Port={}", 
			inet_ntoa(this->_sockAddr.sin_addr), ntohs(this->_sockAddr.sin_port));

		this->_isConnected = false;
		PostAccept();
	}
}
#endif