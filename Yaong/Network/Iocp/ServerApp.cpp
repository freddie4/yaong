#include "stdafx.h"
#include "ServerApp.h"

#include "IocpExFunction.h"
#include <Utility/Logger/Logger.h>

#ifdef USE_IOCP_CORE
namespace yaong::network
{
	ServerApp::ServerApp()
	{
	}

	ServerApp::~ServerApp()
	{
	}

	bool ServerApp::Initialize()
	{
		_threadCount = std::thread::hardware_concurrency();

		if(!StartUp())
		{
			return false;
		}

		if(!Bind())
		{
			return false;
		}

		if(!Listen())
		{
			return false;
		}

		if(!SetIocpEx())
		{
			return false;
		}

		SessionData sessionData;
		sessionData._completionPort = _completionPort;
		sessionData._iocpEx = _iocpEx;
		sessionData._listenSocket = _listenSocket;

		if(!_sessionManager.Initialize(sessionData))
		{
			return false;
		}

		return true;
	}

	void ServerApp::Finalize()
	{
		::closesocket(_listenSocket);

		for(auto i = 0; i < static_cast<int>(_threadVec.size()); ++i)
		{
			::PostQueuedCompletionStatus(_completionPort, 0, 0, nullptr);
		}

		for(auto& i : _threadVec)
		{
			if(i.joinable())
			{
				i.join();
			}
		}

		::CloseHandle(_completionPort);
	}

	bool ServerApp::Run()
	{
		StartIoThreads();
		StartAccept();

		while(true)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}
	}

	bool ServerApp::StartIoThreads()
	{
		for(unsigned int i = 0; i < _threadCount; ++i)
		{
			_threadVec.emplace_back(std::thread
			(std::bind(&ServerApp::IoWorkerThread, this)));
		}

		return true;
	}

	bool ServerApp::StartAccept()
	{
		return _sessionManager.StartAccept();
	}

	void ServerApp::IoWorkerThread()
	{
		DWORD dwTransferred = 0;
		LPOVERLAPPED lpOverlapped = nullptr;
		ULONG_PTR completionKey = 0;

		while(true)
		{
			const auto result = ::GetQueuedCompletionStatus(
				_completionPort,
				&dwTransferred,
				reinterpret_cast<PULONG_PTR>(&completionKey),
				&lpOverlapped, INFINITE);

			if(!lpOverlapped)
			{
				break;
			}

			auto context = static_cast<OverlappedIoContext*>(lpOverlapped);

			if(result == false || dwTransferred == 0)
			{
				if(::WSAGetLastError() == WAIT_TIMEOUT)
				{
					continue;
				}
			}

			context->Completion(dwTransferred);
			delete context;
		}
	}

	bool ServerApp::StartUp()
	{
		WSADATA wsa;
		if(::WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
		{
			return false;
		}

		_completionPort = ::CreateIoCompletionPort(INVALID_HANDLE_VALUE, nullptr, 0, 0);
		if(nullptr == _completionPort)
		{
			LOG_F("CreateIoCompletionPort failed");
			return false;
		}

		_listenSocket = ::WSASocketW(AF_INET, SOCK_STREAM, IPPROTO_TCP,
			nullptr, 0, WSA_FLAG_OVERLAPPED);
		if(_listenSocket == INVALID_SOCKET)
		{
			LOG_F("WSASocket is INVALID_SOCKET");
			return false;
		}

		const auto handle = ::CreateIoCompletionPort(reinterpret_cast
			<HANDLE>(_listenSocket), _completionPort, 0, _threadCount);
		if(handle != _completionPort)
		{
			LOG_F("listen socket IOCP register error: {}", ::WSAGetLastError());
			return false;
		}

		return true;
	}

	bool ServerApp::Bind()
	{
		SOCKADDR_IN servAddr;
		ZeroMemory(&servAddr, sizeof(servAddr));
		servAddr.sin_family = AF_INET;
		servAddr.sin_port = ::htons(_serverPort);
		servAddr.sin_addr.s_addr = ::inet_addr(_serverIP.data());

		if(SOCKET_ERROR == ::bind(_listenSocket, reinterpret_cast<SOCKADDR*>(&servAddr)
			, sizeof(servAddr)))
		{
			LOG_F("bind error");
			return false;
		}

		return true;
	}

	bool ServerApp::Listen()
	{
		if(SOCKET_ERROR == ::listen(_listenSocket, MAX_LISTEN_CLIENTS))
		{
			return false;
		}

		return true;
	}

	bool ServerApp::SetIocpEx()
	{
		_iocpEx = std::make_shared<IocpExFunction>(_listenSocket);

		if(false == _iocpEx->Initialize())
		{
			LOG_F("IocpExFunction Init failed!");
			return false;
		}

		return true;
	}
}
#endif