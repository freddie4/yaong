#include "stdafx.h"
#include "GameServerApp.h"

#ifdef USE_IOCP_CORE
namespace yaong::network
{
	bool GameServerApp::Initialize()
	{
		if(!ServerApp::Initialize())
		{
			return false;
		}

		if(!SetListenSocketOption())
		{
			return false;
		}

		return true;
	}

	void GameServerApp::Finalize()
	{
		ServerApp::Finalize();
	}

	bool GameServerApp::Run()
	{
		if(!ServerApp::Run())
		{
			return false;
		}

		return true;
	}

	bool GameServerApp::SetListenSocketOption()
	{
		// SO_REUSEADDR : no time-wait
		auto opt = 1;
		if(SOCKET_ERROR == ::setsockopt(_listenSocket, SOL_SOCKET, SO_REUSEADDR,
			reinterpret_cast<const char*>(&opt), sizeof(int)))
		{
			LOG_F("SO_REUSEADDR failed");
			return false;
		}

		return true;
	}
}
#endif