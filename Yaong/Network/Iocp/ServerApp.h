#pragma once
#include <Network/Session/ClientSession.h>

#ifdef USE_IOCP_CORE
namespace yaong::network
{
	constexpr unsigned int MAX_LISTEN_CLIENTS = 10;
	constexpr unsigned int MAX_IOCP_SESSION = 10;
	constexpr unsigned int THREAD_COUNT = 8;

	class IocpExFunction;

	class ServerApp
	{
	public:
		ServerApp();
		virtual ~ServerApp();

		virtual bool Initialize();
		virtual void Finalize();
		virtual bool Run();
		
		HANDLE GetCompletionPort() const { return _completionPort; }
		SOCKET GetListenSocket() const { return _listenSocket; }
		SOCKET* GetListenSocketPtr() { return &_listenSocket; }
		auto GetIocpExFp() const { return _iocpEx; }

	protected:
		virtual bool SetListenSocketOption() = 0;

	protected:
		bool StartIoThreads();
		bool StartAccept();
		void IoWorkerThread();

		bool StartUp();
		bool Bind();
		bool Listen();
		bool SetIocpEx();
			
		const unsigned short _serverPort = 6898;
		const std::string _serverIP = "127.0.0.1";

		HANDLE _completionPort = nullptr;
		SOCKET _listenSocket = 0;
		unsigned int _threadCount = THREAD_COUNT;

		std::vector<std::thread> _threadVec;

		std::shared_ptr<IocpExFunction> _iocpEx = nullptr;

		SessionManager<ClientSession> _sessionManager;
	};
}
#endif