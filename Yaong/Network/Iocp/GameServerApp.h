#pragma once
#include "ServerApp.h"
#include <Utility/Singleton.h>

#ifdef USE_IOCP_CORE
namespace yaong::network
{
	class ClientSession;

	class GameServerApp : public ServerApp
	{
		friend class utility::Singleton<GameServerApp>;
	public:
		GameServerApp() = default;
		virtual ~GameServerApp() = default;

		bool Initialize() override;
		void Finalize() override;
		bool Run() override;

		bool SetListenSocketOption() override;

	private:
	};
}

#define gGameServerApp yaong::utility::Singleton<GameServerApp>::Instance()
#endif