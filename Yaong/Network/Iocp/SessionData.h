#pragma once
#include <WinSock2.h>
#include <memory>

#ifdef USE_IOCP_CORE
namespace yaong::network
{
	class IocpExFunction;

	struct SessionData
	{
		int _sessionId;
		SOCKET _listenSocket;
		HANDLE _completionPort;
		std::shared_ptr<IocpExFunction> _iocpEx;
	};
}
#endif
