#pragma once
#include "SessionData.h"

#ifdef USE_IOCP_CORE
namespace yaong::network
{
	class ServerApp;

	using SessionId = unsigned short;

	template <typename TSession>
	class SessionManager
	{
		using SessionMap = std::unordered_map<int, std::shared_ptr<TSession>>;

	public:
		explicit SessionManager();
		virtual ~SessionManager() = default;

		auto GetSession(const SessionId sessionId);

		bool Initialize(SessionData& sessionData);
		bool StartAccept();

		bool Broadcast();

	private:
		bool CreateSessions(SessionData& sessionData);

		std::mutex _lock;

		unsigned short _sessionId = 0;
		unsigned short _maxSessionCount = 1;

		SessionMap _sessionMap;
	};

	template <typename TSession>
	SessionManager<TSession>::SessionManager()
	{
	}

	template <typename TSession>
	auto SessionManager<TSession>::GetSession(const SessionId sessionId)
	{
		std::scoped_lock<std::mutex> lock(_lock);

		const auto found = _sessionMap.find(sessionId);
		return found == _sessionMap.end() ? nullptr : found;
	}

	template <typename TSession>
	bool SessionManager<TSession>::Initialize(SessionData& sessionData)
	{
		return CreateSessions(sessionData);
	}

	template <typename TSession>
	bool SessionManager<TSession>::StartAccept()
	{
		for(auto& session : _sessionMap)
		{
			if(!session.second->PostAccept())
			{
				LOG_F("Accept Failed SessionId: {}", session.second->GetSessionId());
				return false;
			}
		}

		return true;
	}

	template <typename TSession>
	bool SessionManager<TSession>::Broadcast()
	{
		// TODO : Impl
		return true;
	}

	template <typename TSession>
	bool SessionManager<TSession>::CreateSessions(SessionData& sessionData)
	{
		for(auto i = 0; i < _maxSessionCount; ++i)
		{
			sessionData._sessionId = _sessionId++;
			const auto session = std::make_shared<TSession>(sessionData ,this);

			if(!session->Initialize())
			{
				LOG_F("Session Init Failed : {}", session->GetSessionId());
			}

			_sessionMap[sessionData._sessionId] = session;
		}

		if(_maxSessionCount != _sessionId)
		{
			LOG_F("Sessions not created successfully : {}", typeid(TSession).name());
			return false;
		}

		return true;
	}
}
#endif