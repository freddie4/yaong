#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <MSWSock.h>
#include <Utility/Logger/Logger.h>

#ifdef USE_IOCP_CORE
namespace yaong::network
{
	constexpr int WSA_IOCTL_SUCCESS = 0;

	class IocpExFunction final
	{
	public:
		IocpExFunction(const SOCKET listenSocket)
			:_listenSocket(listenSocket)
		{
		}
		~IocpExFunction() = default;

		SOCKET GetListenSocket() const { return _listenSocket; }

		bool Initialize()
		{
			DWORD dwBytes = 0;

			// DisconnectEx
			GUID guidDisconnectEx = WSAID_DISCONNECTEX;
			if(WSA_IOCTL_SUCCESS != WSAIoctl(_listenSocket, SIO_GET_EXTENSION_FUNCTION_POINTER,
				&guidDisconnectEx, sizeof(guidDisconnectEx),
				&_lpfnDisconnectEx, sizeof(_lpfnDisconnectEx),
				&dwBytes, nullptr, nullptr))
			{
				LOG_F("WSAIoctl error : {}", WSAGetLastError());
				return false;
			}

			// ConnectEx
			GUID guidConnectEx = WSAID_CONNECTEX;
			if(WSA_IOCTL_SUCCESS != WSAIoctl(_listenSocket, SIO_GET_EXTENSION_FUNCTION_POINTER,
				&guidConnectEx, sizeof(guidConnectEx),
				&_lpfnConnectEx, sizeof(_lpfnConnectEx),
				&dwBytes, nullptr, nullptr))
			{
				LOG_F("WSAIoctl error : {}", WSAGetLastError());
				return false;
			}

			// AcceptEx
			GUID guidAcceptEx = WSAID_ACCEPTEX;
			if(WSA_IOCTL_SUCCESS != WSAIoctl(_listenSocket, SIO_GET_EXTENSION_FUNCTION_POINTER,
				&guidAcceptEx, sizeof(guidAcceptEx),
				&_lpfnAcceptEx, sizeof(_lpfnAcceptEx),
				&dwBytes, nullptr, nullptr))
			{
				LOG_F("WSAIoctl error : {}", WSAGetLastError());
				return false;
			}

			return true;
		}

		bool DisconnectEx(SOCKET socket, LPOVERLAPPED lpOverlapped, DWORD dwFlags, DWORD reserved)
		{
			return _lpfnDisconnectEx(socket, lpOverlapped, dwFlags, reserved) != 0;
		}

		bool ConnetEx(SOCKET socket, const struct sockaddr* name, int namelen, PVOID lpSendBuffer,
			DWORD dwSendDataLength, LPDWORD lpdwBytesSent, LPOVERLAPPED lpOverlapped)
		{
			return _lpfnConnectEx(socket, name, namelen, lpSendBuffer, dwSendDataLength,
				lpdwBytesSent, lpOverlapped) != 0;
		}

		bool AcceptEx(SOCKET listenSocket, SOCKET sessionSocket, PVOID lpOutBuffer, DWORD dwReceiveDataLength,
			DWORD dwLocalAddressLength, DWORD dwRemoteAddressLength, LPDWORD lpdwBytesReceived, LPOVERLAPPED lpOverlapped)
		{
			return _lpfnAcceptEx(listenSocket, sessionSocket, lpOutBuffer, dwReceiveDataLength,
				dwLocalAddressLength, dwRemoteAddressLength, lpdwBytesReceived, lpOverlapped) != 0;
		}

	private:
		SOCKET _listenSocket = 0;

		LPFN_CONNECTEX _lpfnConnectEx = nullptr;
		LPFN_DISCONNECTEX _lpfnDisconnectEx = nullptr;
		LPFN_ACCEPTEX _lpfnAcceptEx = nullptr;
	};
}
#endif