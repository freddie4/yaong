#pragma once

#ifdef USE_IOCP_CORE
namespace yaong::network
{
	enum class DisconnectReason
	{
		None,
		RecvZero,
		CompletionError,
		AlreadyDisconnected,
	};

	struct OverlappedIoContext : OVERLAPPED
	{
		explicit OverlappedIoContext(std::function<void(std::size_t)> completion)
			: _completion(std::move(completion))
		{
			std::memset(static_cast<LPOVERLAPPED>(this), 0, sizeof(OVERLAPPED));
			std::memset(&_wsaBuf, 0, sizeof(WSABUF));
		}
		virtual ~OverlappedIoContext() = default;

		void Completion(const std::size_t bytesTransferred)
		{
			_completion(bytesTransferred);
		}

		std::function<void(std::size_t)> _completion;
		WSABUF _wsaBuf;
	};
}
#endif