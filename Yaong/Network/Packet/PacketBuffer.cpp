#include "stdafx.h"
#include "PacketBuffer.h"

namespace yaong::network
{
	PacketBuffer::PacketBuffer(const size_t bufSize)
		:_size(bufSize)
		, _remainSize(_size)
	{
		_buf = new char[_size];
		std::memset(_buf, 0, _size);
		_head = _buf;
		_tail = _buf;
	}

	PacketBuffer::~PacketBuffer()
	{
		_head = nullptr;
		_tail = nullptr;
		delete[] _buf;
	}

	void PacketBuffer::Write(const size_t size)
	{
		// 버퍼에 쓰기 작업이 완료하면(RecvCompletion) Write를 호출
		// 버퍼가 꽉차면 새로운 버퍼를 만들고, 처리되지 않은 데이터를 옮긴다.

		_tail += size;
		_currentSize += size;
		_remainSize -= size;

		if (_remainSize <= _changeSize)
		{
			ReAlloc();
		}
	}

	void PacketBuffer::Remove(const size_t size)
	{
		// 버퍼에서 패킷처리가 완료되면 Remove 호출. 처리완료한 패킷바이트는 그대로 남겨둠
		// 패킷 처리할때 버퍼에서 패킷내용 복사해가야함. ReAlloc할때 delete됨

		_head += size;
		_processed += size;
	}

	void PacketBuffer::ReAlloc()
	{
		// 기존에 남은 데이터 복사해둠
		char* temp = _head;
		char* tempBuf = _buf;
		const auto remainDataSize = _currentSize - _processed;

		// 버퍼 새로 할당, 남은 데이터 복사
		_buf = new char[_size];
		std::memcpy(_buf, temp, remainDataSize);

		// 새로 할당한 버퍼 초기화
		Reset(remainDataSize);

		// 기존 버퍼 삭제
		delete[] tempBuf;
	}

	void PacketBuffer::Reset(const size_t size)
	{
		_head = _buf;
		_tail = _buf + size;
		std::memset(_tail, 0, _size - size);
		_currentSize = size;
		_processed = 0;
		_remainSize = _size - size;
	}
}