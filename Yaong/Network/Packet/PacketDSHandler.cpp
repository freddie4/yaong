#include "stdafx.h"
#include "PacketDSHandler.h"

#include <ProtoDefine/PacketSCDefine.pb.h>
#include <Network/Asio/ServerApp.h>
#include <Network/Session/ClientSession.h>
#include <PacketDeclare/PacketSCDeclare.h>

namespace yaong::network
{
	IMPLEMENT_HANDLER(DBServerSession, PacketDS_AuthRes)
	{
		PACKET_HANDLER_LOG;

		PacketSC_AuthRes sendPacket;
		sendPacket.set_error(packet.error());

		if(const auto clientSession =
			gServerApp->GetClientSessionManager().GetSession(packet.session_id()))
		{
			clientSession->SendPacket(sendPacket);
		}
	}

	IMPLEMENT_HANDLER(DBServerSession, PacketDS_JoinRes)
	{
		PACKET_HANDLER_LOG;

		PacketSC_JoinRes sendPacket;
		sendPacket.set_error(packet.error());

		if(const auto clientSession =
			gServerApp->GetClientSessionManager().GetSession(packet.session_id()))
		{
			clientSession->SendPacket(sendPacket);
		}
	}
}
