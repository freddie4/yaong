#pragma once

// WSARecv 걸때 tail, remainSize로 걸어야함
// ReAlloc 하기전에 꼭 패킷 처리하는 Session으로 기존 포인터 전해줘야함
// 버퍼에서 읽을땐 head로 읽는다
// 버퍼에 쓸땐 tail에 쓴다

namespace yaong::network
{
	constexpr int DEFAULT_RECV_BUF_SIZE = 256;

	class PacketBuffer
	{
	public:
		explicit PacketBuffer(const size_t bufSize = DEFAULT_RECV_BUF_SIZE);
		~PacketBuffer();

		char* Data() const { return _buf; }
		char* Head() const { return _head; }
		char* Tail() const { return _tail; }

		size_t Size() const { return _size; }
		size_t RemainSize() const { return _remainSize; }
		size_t CurrentSize() const { return _currentSize; }
		size_t NotProcessedDataSize() const { return _currentSize - _processed; }

		void Write(const size_t size);
		void Remove(const size_t size);

	private:
		void ReAlloc();
		void Reset(const size_t size);

	private:
		std::shared_mutex _mutex;

		size_t _size = DEFAULT_RECV_BUF_SIZE;
		size_t _currentSize = 0;
		size_t _remainSize = DEFAULT_RECV_BUF_SIZE;
		size_t _processed = 0;

		char* _buf = nullptr;
		char* _head = nullptr;
		char* _tail = nullptr;
		const size_t _changeSize = 0;
	};

}
