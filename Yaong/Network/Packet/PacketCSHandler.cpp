#include "stdafx.h"
#include "PacketCSHandler.h"

#include <ProtoDefine/PacketCSDefine.pb.h>
#include <ProtoDefine/PacketSDDefine.pb.h>
#include <Network/Asio/ServerApp.h>
#include <Network/Session/DBServerSession.h>
#include <Utility/Logger/Logger.h>
#include <Contents/Player/PlayerManager.h>
#include <PacketDeclare/PacketSDDeclare.h>

namespace yaong::network
{
	IMPLEMENT_HANDLER(ClientSession, PacketCS_Connect)
	{
		PACKET_HANDLER_LOG;
	}

	IMPLEMENT_HANDLER(ClientSession, PacketCS_Chat)
	{
		PACKET_HANDLER_LOG;

		const auto& mesasge = packet.message();
	}

	IMPLEMENT_HANDLER(ClientSession, PacketCS_Move)
	{
		PACKET_HANDLER_LOG;
	}

	IMPLEMENT_HANDLER(ClientSession, PacketCS_DummyConnect)
	{
		PACKET_HANDLER_LOG;
	}

	IMPLEMENT_HANDLER(ClientSession, PacketCS_NewPlayer)
	{
		PACKET_HANDLER_LOG;
	}

	IMPLEMENT_HANDLER(ClientSession, PacketCS_GenerateTest)
	{
		PACKET_HANDLER_LOG;
	}

	IMPLEMENT_HANDLER(ClientSession, PacketCS_Auth)
	{
		PACKET_HANDLER_LOG;

		PacketSD_Auth sendPacket;
		sendPacket.set_account_id(packet.account_id());
		sendPacket.set_password(packet.password());
		sendPacket.set_session_id(session.GetSessionId());
		sendPacket.set_ip(session.GetRemoteIp());

		gServerApp->GetDBServerSession()->SendPacket(sendPacket);
	}

	IMPLEMENT_HANDLER(ClientSession, PacketCS_Join)
	{
		PACKET_HANDLER_LOG;

		PacketSD_Join sendPacket;
		sendPacket.set_session_id(session.GetSessionId());
		sendPacket.set_account_id(packet.account_id());
		sendPacket.set_password(packet.password());

		gServerApp->GetDBServerSession()->SendPacket(sendPacket);
	}
}
