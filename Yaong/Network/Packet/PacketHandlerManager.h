#pragma once
#include "PacketHeader.h"
#include <Utility/Logger/Logger.h>

#define DECLARE_HANDLER(TSession, TPacket)\
template <typename TSession, typename TPacket>\
class PacketHandler;\
template <>\
class PacketHandler<TSession, TPacket>\
{\
public:\
	inline static void Handle(TSession& session, TPacket&& packet);\
};

#define IMPLEMENT_INITIALIZE(TSession)\
template <>\
PacketHandlerManager<TSession> Session<TSession>::_packetHandlerManager;\
static void Initialize(PacketHandlerManager<TSession>& packetHandlerManager)

#define REGISTER_HANDLER(TPacket)\
packetHandlerManager.Register<TPacket>(TPacket::PROTOCOL_NUMBER);

#define IMPLEMENT_HANDLER(TSession, TPacket)\
void PacketHandler<TSession, TPacket>::Handle(TSession& session, TPacket&& packet)

#define PACKET_HANDLER_LOG \
LOG_("Packet Received[{}]", packet.GetPacketName(), packet.ShortDebugString())

namespace yaong::network
{
	using PacketArray = std::array<char, MAX_PACKET_SIZE>;	

	template <typename TSession, typename TPacket>
	class PacketHandler;

	template <typename TSession>
	class PacketParserBase
	{
	public:
		PacketParserBase() = default;
		virtual ~PacketParserBase() = default;

		virtual bool Handle(TSession& session, PacketArray&& packet) = 0;
	};

	template <typename TSession, typename TPacket>
	class PacketParser : PacketParserBase<TSession>
	{
	public:
		PacketParser() = default;
		virtual ~PacketParser() = default;

		bool Handle(TSession& session, PacketArray&& packetArray) override
		{
			TPacket packet;

			const auto packetSize = *reinterpret_cast<unsigned short*>(packetArray.data());

			if(!packet.ParseFromArray(packetArray.data() + PACKET_HEADER_SIZE,
				packetSize))
			{
				LOG_F("ParseFromArray failed. size : {}", packetSize);
				return false;
			}

			session.GetHeartbeat().UpdateReceivedTime();
			PacketHandler<TSession, TPacket>::Handle(session, std::move(packet));
			return true;
		}
	};

	template <typename TSession>
	class PacketHandlerManager
	{
	public:
		PacketHandlerManager()
		{
			Clear();
			Initialize(*this);
		}

		~PacketHandlerManager()
		{
			Clear();
		}

		template <typename TPacket>
		bool Register(const ProtocolNumber protocolNumber)
		{
			if(protocolNumber > _packetParsers.size())
			{
				LOG_F("Invalid ProtocolNumber : {}", protocolNumber);
				return false;
			}

			if(_packetParsers[protocolNumber])
			{
				LOG_F("Already registered protocolNumber : {}", protocolNumber);
				return false;
			}

			PacketParserBase<TSession>* packetParser = 
				reinterpret_cast<PacketParserBase<TSession>*>(
				new PacketParser<TSession, TPacket>());

			_packetParsers[protocolNumber].reset(packetParser);
			
			return true;
		}

		bool Handle(TSession& session, PacketArray&& packetArray)
		{
			const auto packetNumber = reinterpret_cast<PacketHeader*>
				(packetArray.data())->_packetNumber;

			const auto packetParser = _packetParsers[packetNumber].get();
			if(!packetParser)
			{
				LOG_F("PacketParser Not Found : {}", packetNumber);
				return false;
			}

			return packetParser->Handle(session, std::move(packetArray));
		}

		void Clear() noexcept
		{
			for(auto& iter : _packetParsers)
			{
				iter.reset();
			}
		}

	private:
		std::array<std::unique_ptr<PacketParserBase<TSession>>,
			std::numeric_limits<ProtocolNumber>::max()> _packetParsers;
	};
}
