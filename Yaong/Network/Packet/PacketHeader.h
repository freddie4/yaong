#pragma once

namespace yaong::network
{
	constexpr int PACKET_HEADER_SIZE = sizeof(unsigned short) +
		sizeof(unsigned short);

	constexpr unsigned short MAX_PACKET_SIZE = 256;

	struct PacketHeader
	{
		unsigned short _packetSize = 0;
		unsigned short _packetNumber = 0;
	};
}


