#include "PacketProcessor.h"

#include <Packet/PacketHeader.h>
#include <Logic/PacketTasks.h>
#include <Logic/PacketTaskFactory.h>
#include <Logic/Define.h>
#include <Packet/PacketCSDefine.pb.h>

namespace yaong
{
	using namespace google;
	using namespace network;

	bool PacketProcessor::Initialize()
	{		
		_packetTaskFactory = std::make_shared<PacketTaskFactory>();

		if(_packetTaskFactory == nullptr)
		{
			return false;
		}

		// Register PacketTasks here.
		_packetTaskFactory->RegisterPacketTask<PacketTaskCSConnect>(FROM_ENUM(packetCS::ProtocolNumber::EPacketCSConnect));
		_packetTaskFactory->RegisterPacketTask<PacketTaskCSChat>(FROM_ENUM(packetCS::ProtocolNumber::EPacketCSChat));
		_packetTaskFactory->RegisterPacketTask<PacketTaskCSMove>(FROM_ENUM(packetCS::ProtocolNumber::EPacketCSMove));
		_packetTaskFactory->RegisterPacketTask<PacketTaskCSConnectDummy>(FROM_ENUM(packetCS::ProtocolNumber::EPacketCSDummyConnect));

		_thread = std::thread(std::bind(&PacketProcessor::Run, this));

		return true;
	}

	void PacketProcessor::Finalize()
	{
		if(_thread.joinable())
		{
			_thread.join();
		}
	}

	void PacketProcessor::PushTask(const std::shared_ptr<PacketTask>& task)
	{
		std::lock_guard<std::mutex> lock(_queueLock);

		_taskQueue.emplace(task);
	}

	void PacketProcessor::Run()
	{
		while(true)
		{
			std::lock_guard<std::mutex> lock(_queueLock);

			if(!_taskQueue.empty())
			{
				const auto task = _taskQueue.front();
				_taskQueue.pop();

				task->TaskProcess();
			}			
		}
	}	
}
