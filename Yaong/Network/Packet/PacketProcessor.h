#pragma once
#include <Utility/Singleton.h>
#include <memory>
#include <mutex>
#include <queue>

namespace yaong
{
	namespace network
	{
		struct PacketHeader;
		class Session;
		using IocpSessionPtr = std::shared_ptr<Session>;
	}

	class PacketTask;
	class PacketTaskFactory;
	using TaskQueue = std::queue <std::shared_ptr<PacketTask>>;

	class PacketProcessor final
	{
		friend class utility::Singleton<PacketProcessor>;

	public:
		bool Initialize();
		void Finalize();

		void PushTask(const std::shared_ptr<PacketTask>& task);

		std::shared_ptr<PacketTaskFactory> GetPacketTaskFactory() const { return _packetTaskFactory; }

	private:
		void Run();

	private:
		PacketProcessor() = default;
		~PacketProcessor() = default;

	private:
		std::mutex _queueLock;
		TaskQueue _taskQueue;
		std::shared_ptr<PacketTaskFactory> _packetTaskFactory = nullptr;
		std::thread _thread;
	};
}

#define gPacketProcessor yaong::utility::Singleton<PacketProcessor>::Instance()
