#include "PacketTasks.h"

#include <Packet/PacketHeader.h>
#include <Packet/PacketCSDefine.pb.h>
#include <Packet/PacketSCDefine.pb.h>
#include <Utility/Logger/Logger.h>
#include <Network/Iocp/IocpClientSession.h>
#include <Contents/Player/Player.h>
#include <Contents/Player/PlayerInfo.h>
#include <Contents/Player/PlayerManager.h>
#include "Define.h"
#include <Math/Vector.h>

namespace yaong
{
	using namespace network;
	using namespace packetCS;
	using namespace packetSC;

	void PacketTaskCSConnect::TaskProcess()
	{
		PacketCSConnect recvPacket;
		DeSerialize(&recvPacket);

		LOG_F("Packet Received[{}] : {}", _packet->_packetNumber, recvPacket.name().data());

		/*PacketSCConnect sendPacket;
		sendPacket.set_name(recvPacket.name().data());
		sendPacket.set_playerid(_sessionPtr->GetSessionID());
		const auto sendBuffer = MakePacket(&sendPacket, FROM_ENUM(EPacketSCConnect));

		_sessionPtr->PostSend(sendBuffer.first, sendBuffer.second);*/

		// Set PlayerInfo
		const auto characterMap = gPlayerManager->GetCharacterMap();
		const PlayerInfo info;
		/*gPlayerManager->SetPlayer(_sessionPtr->GetSessionID(), info);*/

		// Send World Characters Info
		for(auto& iter : *characterMap)
		{
			PacketSCNewPlayer packet;
			const auto playerInfo = iter.second;

			/*packet.set_sessionid(iter.first);
			packet.set_x(playerInfo.x);
			packet.set_y(playerInfo.y);
			packet.set_z(playerInfo.z);
			packet.set_yaw(playerInfo.yaw);
			packet.set_roll(playerInfo.roll);
			packet.set_pitch(playerInfo.pitch);
			packet.set_vx(playerInfo.vx);
			packet.set_vy(playerInfo.vy);
			packet.set_vz(playerInfo.vz);*/
			// set hp

			/*const auto sendBuffer2 = MakePacket(&packet, FROM_ENUM(PacketNumber::NEWPLAYER));
			_sessionPtr->PostSend(sendBuffer2.first, sendBuffer2.second);*/
		}
	}

	void PacketTaskCSChat::TaskProcess()
	{
		PacketCSChat recvPacket;
		DeSerialize(&recvPacket);

		const auto& message = recvPacket.message();

		LOG_F("Packet Received[{}] : {}", _packet->_packetNumber, message.data());
		
		PacketSCChat sendPacket;
		sendPacket.set_name(recvPacket.name().data());
		sendPacket.set_message(message.data());
		const auto sendBuffer = MakePacket(&sendPacket, FROM_ENUM(EPacketSCChat));

		_sessionPtr->Broadcast(sendBuffer.first, sendBuffer.second);
	}

	void PacketTaskCSMove::TaskProcess()
	{
		PacketCSMove recvPacket;
		DeSerialize(&recvPacket);

		LOG_F("Packet Received[{}] - {} : {} {} {}", _packet->_packetNumber,
			recvPacket.playerid(), recvPacket.x(), recvPacket.y(), recvPacket.z());

		auto player = gPlayerManager->GetPlayer(recvPacket.playerid());
		player->SetPos({ recvPacket.x(), recvPacket.y(), recvPacket.z() });

		PacketSCMove sendPacket;
		sendPacket.set_playerid(recvPacket.playerid());
		sendPacket.set_x(recvPacket.x());
		sendPacket.set_y(recvPacket.y());
		sendPacket.set_z(recvPacket.z());
		const auto sendBuffer = MakePacket(&sendPacket, FROM_ENUM(EPacketSCMove));

		_sessionPtr->Broadcast(sendBuffer.first, sendBuffer.second);		
	}

	void PacketTaskCSConnectDummy::TaskProcess()
	{
		PacketCSDummyConnect recvPacket;
		DeSerialize(&recvPacket);

		LOG_F("Packet Received[{}] - {} : {} {} {}", _packet->_packetNumber,
			recvPacket.playerid(), recvPacket.x(), recvPacket.y(), recvPacket.z())

		PlayerInfo info;
		
		info.x = recvPacket.x();
		info.y = recvPacket.y();
		info.z = recvPacket.z();
		info.playerId = recvPacket.playerid();
		
		gPlayerManager->CreatePlayer(info);

		const auto clientSession = std::static_pointer_cast<IocpClientSession>(_sessionPtr);
		clientSession->SetPlayerId(info.playerId);
	}
}
