#pragma once
#include <Network/Asio/Session.h>

namespace yaong::network
{
	class DBServerSession : public Session<DBServerSession>
	{
	public:
		explicit DBServerSession(boost::asio::io_context& ioContext);
		virtual ~DBServerSession() = default;

		bool Connect(const boost::asio::ip::tcp::endpoint& endpoint);
	};
}
