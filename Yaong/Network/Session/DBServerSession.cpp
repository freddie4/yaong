#include "stdafx.h"
#include "DBServerSession.h"

namespace yaong::network
{
	DBServerSession::DBServerSession(boost::asio::io_context& ioContext)
		: Session<DBServerSession>(ioContext, 0, nullptr)
	{
	}

	bool DBServerSession::Connect(const boost::asio::ip::tcp::endpoint& endpoint)
	{
		boost::system::error_code errorCode;
		_socket.connect(endpoint, errorCode);

		if(errorCode)
		{
			LOG_F("Failed to connect DB Server : {} {} {}",
				errorCode.message(), endpoint.address().to_string(), endpoint.port());
			return false;
		}

		LOG_F("Connected to DB Server");
		return true;
	}
}
