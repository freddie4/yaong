#include "stdafx.h"
#include "ClientSession.h"

namespace yaong::network
{
	ClientSession::ClientSession(boost::asio::io_context& ioContext,
		const unsigned short sessionId,
		SessionManager<ClientSession>* sessionManager)
		: Session<ClientSession>(ioContext, sessionId, sessionManager)
	{
	}
}
