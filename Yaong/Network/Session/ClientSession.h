#pragma once
#include <Network/Asio/Session.h>

namespace yaong::network
{
	class ClientSession : public Session<ClientSession>
	{
	public:
		explicit ClientSession(boost::asio::io_context& ioContext,
			const unsigned short sessionId,
			SessionManager<ClientSession>* sessionManager);
		virtual ~ClientSession() = default;

		void SetPlayerId(const int playerId) { _playerId = playerId; }
		int GetPlayerId() const { return _playerId; }

	private:
		int _playerId;
	};
}
