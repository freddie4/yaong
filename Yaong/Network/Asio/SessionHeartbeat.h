#pragma once

namespace yaong::network
{
	class SessionHeartbeat
	{
	public:
		SessionHeartbeat();
		~SessionHeartbeat() = default;

		bool IsRequiredHeartbeat() const;

		auto GetConnectingTime() const;

		void UpdateReceivedTime();

	private:
		const std::chrono::time_point<std::chrono::steady_clock> _createdTimePoint;
		std::chrono::time_point<std::chrono::steady_clock> _lastReceivedTimePoint;
	};
}