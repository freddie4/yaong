#pragma once
#include <Network/Session/ClientSession.h>
#include <Utility/Singleton.h>

namespace yaong::network
{
	class DBServerSession;

	class ServerApp
	{
		friend class utility::Singleton<ServerApp>;
	public:
		ServerApp();
		virtual ~ServerApp();

		virtual bool Initialize();
		void Finalize();
		virtual bool Run();

		bool PostAccept();

		auto& GetClientSessionManager() { return _clientSessionManager; }
		auto GetDBServerSession() const { return _dbServerSession; }

	private:
		bool ConnectToDBServer();
		void CloseAcceptors();

	protected:
		boost::thread_group _threadGroup;

		boost::asio::io_context _ioContext;

		boost::asio::ip::tcp::acceptor _clientAcceptor;
		SessionManager<ClientSession> _clientSessionManager;

		std::shared_ptr<DBServerSession> _dbServerSession;
	};
}

#define gServerApp yaong::utility::Singleton<ServerApp>::Instance()