#pragma once
#include <Utility/IdPool.h>

namespace yaong::network
{
	template <typename TSession>
	class SessionManager
	{
		static constexpr unsigned int MAX_SESSIONID_COUNT = 5000;

		using SessionMap = std::unordered_map<SessionId, std::shared_ptr<TSession>>;
		using SessionIdPool = utility::IdPool<SessionId, 1, MAX_SESSIONID_COUNT>;

	public:
		SessionManager() = default;
		virtual ~SessionManager() = default;

		auto GetSession(const SessionId sessionId);
		SessionMap& GetSessionMap() const { return _sessionMap; }

		template <typename TPacket>
		void Broadcast(const TPacket& packet);

		std::shared_ptr<TSession> CreateSession(boost::asio::io_context& ioContext);
		void RemoveSession(const SessionId sessionId);

		std::mutex _lock;

		SessionIdPool _sessionIdPool;
		SessionMap _sessionMap;
	};

	template <typename TSession>
	auto SessionManager<TSession>::GetSession(const SessionId sessionId)
	{
		std::scoped_lock<std::mutex> lock(_lock);

		const auto found = _sessionMap.find(sessionId);
		return found == _sessionMap.end() ? nullptr : found->second;
	}

	template <typename TSession>
	template <typename TPacket>
	void SessionManager<TSession>::Broadcast(const TPacket& packet)
	{
		if constexpr(std::is_base_of_v<google::protobuf::Message, TPacket>)
		{
			for(auto& session : _sessionMap)
			{
				session.second->SendPacket(packet);
			}
		}		
	}

	template <typename TSession>
	std::shared_ptr<TSession> SessionManager<TSession>::CreateSession(
		boost::asio::io_context& ioContext)
	{
		std::scoped_lock<std::mutex> lock(_lock);

		if(const auto sessionId = _sessionIdPool.Get())
		{
			const auto session = std::make_shared<TSession>(ioContext, sessionId.value(), this);
			_sessionMap[sessionId.value()] = session;
			
			return session;
		}

		return nullptr;
	}

	template <typename TSession>
	void SessionManager<TSession>::RemoveSession(const SessionId sessionId)
	{
		std::scoped_lock<std::mutex> lock(_lock);

		if(_sessionMap.find(sessionId) == _sessionMap.end())
		{
			LOG_F("SessionId is not in SessionMap : {}", sessionId);
			return;
		}

		_sessionMap.erase(sessionId);
	}
}
