#pragma once
#include <Network/Packet/PacketHandlerManager.h>
#include <Network/Asio/SessionManager.h>
#include <Network/Asio/SessionHeartbeat.h>
#include <Network/Packet/PacketBuffer.h>
#include <Cryptography/RC4.h>

namespace yaong::network
{
	template <typename TSession>
	class Session : public std::enable_shared_from_this<TSession>
	{
	public:
		static PacketHandlerManager<TSession> _packetHandlerManager;

		explicit Session(boost::asio::io_context& ioContext,
			const unsigned short sessionId,
			SessionManager<TSession>* sessionManager);
		virtual ~Session();

		template <typename TPacket>
		void SendPacket(const TPacket& packet);

		template <typename TPacket>
		void Broadcast(const TPacket& packet);

		bool PostReceive();

		boost::asio::ip::tcp::socket& GetSocket() { return _socket; }
		auto GetSessionId() const { return _sessionId; }
		auto GetRemoteIp() const { return _socket.remote_endpoint().address().to_string(); }
		SessionHeartbeat& GetHeartbeat() { return _heartbeat; }

	protected:
		void Shutdown(
			boost::asio::socket_base::shutdown_type shutdownType =
			boost::asio::socket_base::shutdown_type::shutdown_both) noexcept;

	protected:
		boost::asio::ip::tcp::socket _socket;
		SessionManager<TSession>* _sessionManager;
		SessionHeartbeat _heartbeat;

		SessionId _sessionId;
		PacketBuffer _recvBuf;
		crypto::CryptoBase* _crypto = nullptr;

		std::atomic<bool> _isConnected = false;
		std::mutex _lock;
	};

	template <typename TSession>
	Session<TSession>::Session(boost::asio::io_context& ioContext,
		const unsigned short sessionId,
		SessionManager<TSession>* sessionManager)
		: _socket(ioContext)
		, _sessionManager(sessionManager)
		, _sessionId(sessionId)
	{
		/*_crypto = new crypto::Rc4();*/
	}

	template <typename TSession>
	Session<TSession>::~Session()
	{
		if(_socket.is_open())
		{
			Shutdown();
		}

		if(_crypto)
		{
			delete _crypto;
		}
	}

	template <typename TSession>
	template <typename TPacket>
	void Session<TSession>::SendPacket(const TPacket& packet)
	{
		if constexpr(!std::is_base_of_v<google::protobuf::Message, TPacket>)
		{
			return;
		}

		std::scoped_lock<std::mutex> lock(_lock);

		if(!_socket.is_open())
		{
			Shutdown(boost::asio::socket_base::shutdown_type::shutdown_send);
			return;
		}

		const auto packetBodySize = packet.ByteSizeLong();
		const auto packetSize = PACKET_HEADER_SIZE + packetBodySize;

		const auto sendBuffer = new char[packetSize];

		PacketHeader header = { packetBodySize, TPacket::PROTOCOL_NUMBER };
		std::copy(sendBuffer, sendBuffer + PACKET_HEADER_SIZE, reinterpret_cast<char*>(&header));

		if(!packet.SerializeToArray(sendBuffer + PACKET_HEADER_SIZE,
			packetSize - packetBodySize))
		{
			LOG_F("Failed to SerializeToArray : No : {}, Size : {}", 
				TPacket::PROTOCOL_NUMBER, packetSize);
			delete[] sendBuffer;
			return;
		}

		if(_crypto)
		{
			_crypto->Encode(sendBuffer + PACKET_HEADER_SIZE, packetBodySize);
		}

		boost::asio::async_write(_socket,
			boost::asio::buffer(sendBuffer, packetSize),
			[this, sendBuffer, self = this->shared_from_this()](
				const boost::system::error_code& errorCode, const std::size_t bytesTransferred)
		{
			delete[] sendBuffer;

			if(!_socket.is_open())
			{
				LOG_F("SendCompletion-Session is Already closed : {}", _sessionId);
				Shutdown(boost::asio::socket_base::shutdown_type::shutdown_send);
				return;
			}

			const auto remoteEndPoint = _socket.remote_endpoint();
			const auto remoteEndPointInfo = fmt::format("{} {}", 
				remoteEndPoint.address().to_string(), remoteEndPoint.port());

			if(bytesTransferred == 0)
			{
				LOG_F("Disconnected : 0 byte transferred. {} {}",
					remoteEndPointInfo, _sessionId);
				Shutdown(boost::asio::socket_base::shutdown_type::shutdown_send);
				return;
			}

			if(errorCode)
			{
				auto shutdownType =
					boost::asio::socket_base::shutdown_type::shutdown_send;

				if(errorCode == boost::asio::error::eof ||
					errorCode == boost::asio::error::connection_reset)
				{
					shutdownType = boost::asio::socket_base::shutdown_both;
					LOG_F("Session Disconnected : {} {}",
						remoteEndPointInfo, _sessionId);
				}
				else if(errorCode == boost::asio::error::connection_aborted)
				{
					LOG_F("Session Connected aborted : {} {}",
						remoteEndPointInfo, _sessionId);
				}
				else
				{
					LOG_F("SendCompletion error : {} {} {}",
						errorCode.message(), remoteEndPointInfo, _sessionId);
				}

				Shutdown(shutdownType);
				return;
			}
		});
	}

	template <typename TSession>
	template <typename TPacket>
	void Session<TSession>::Broadcast(const TPacket& packet)
	{
		if constexpr(std::is_base_of_v<google::protobuf::Message, TPacket>)
		{
			_sessionManager->Broadcast(packet);
		}		
	}

	template <typename TSession>
	bool Session<TSession>::PostReceive()
	{
		if(!_socket.is_open())
		{
			return false;
		}

		_socket.async_read_some(boost::asio::buffer(_recvBuf.Tail(), _recvBuf.RemainSize()),
			[this, self = this->shared_from_this()]
		(const boost::system::error_code& errorCode, const std::size_t bytesTransferred)
		{
			if(!_socket.is_open())
			{
				LOG_F("Already Disconnected : {}", _sessionId);
				return;
			}

			std::string remoteEndPointInfo;
			boost::system::error_code remoteEndpointErrorCode;
			const auto& remoteEndPoint = _socket.remote_endpoint(remoteEndpointErrorCode);
			if(!remoteEndpointErrorCode)
			{
				remoteEndPointInfo = fmt::format("{} {}", remoteEndPoint.address().to_string(),
					remoteEndPoint.port());
			}

			if(bytesTransferred == 0)
			{
				LOG_F("Disconnected - 0 bytes transferred : {}", _sessionId);
				Shutdown(boost::asio::socket_base::shutdown_type::shutdown_receive);
				return;
			}

			if(errorCode)
			{
				auto shutdownType = 
					boost::asio::socket_base::shutdown_type::shutdown_receive;

				if(errorCode == boost::asio::error::eof ||
					errorCode == boost::asio::error::connection_reset)
				{
					LOG_F("Disconnected : {} {}", remoteEndPointInfo, _sessionId);
					shutdownType = boost::asio::socket_base::shutdown_type::shutdown_both;
				}
				else if(errorCode == boost::asio::error::connection_aborted)
				{
					LOG_F("Connection aborted : {} {}", remoteEndPointInfo, _sessionId);
				}
				else
				{
					LOG_F("Receive Completion error : {} {} {}", errorCode.message(),
						remoteEndPointInfo, _sessionId);
				}

				Shutdown(shutdownType);
				return;
			}

			_recvBuf.Write(bytesTransferred);

			if(_recvBuf.NotProcessedDataSize() < PACKET_HEADER_SIZE)
			{
				LOG_F("Received size < Packet Header Size : {}", _sessionId);
				return;
			}

			const auto packetSize = *reinterpret_cast<unsigned short*>(_recvBuf.Head())
				+ PACKET_HEADER_SIZE;

			PacketArray recvArray = {};
			std::copy(_recvBuf.Head(), _recvBuf.Head() + packetSize, recvArray.data());

			if(_crypto)
			{
				_crypto->Decode(recvArray.data() + PACKET_HEADER_SIZE, packetSize
					- PACKET_HEADER_SIZE);
			}

			if(!_packetHandlerManager.Handle(static_cast<TSession&>(*this), 
				std::move(recvArray)))
			{
				LOG_F("Packet Handler Failed : {}", _sessionId);
				Shutdown(boost::asio::socket_base::shutdown_type::shutdown_receive);
				return;
			}

			_recvBuf.Remove(packetSize);

			PostReceive();
		});

		return true;
	}

	template <typename TSession>
	void Session<TSession>::Shutdown(
		boost::asio::socket_base::shutdown_type shutdownType) noexcept
	{
		std::scoped_lock<std::mutex> lock(_lock);

		_isConnected = false;

		{
			boost::system::error_code errorCode;
			const auto& remoteEndpoint = _socket.remote_endpoint(errorCode);

			if(errorCode)
			{
				LOG_F("Session Disconnected : {} {}", errorCode.message(), _sessionId);
			}

			LOG_F("Session Disconnected : {} {} {} {}", FROM_ENUM(shutdownType),
				remoteEndpoint.address().to_string(), remoteEndpoint.port(),
				_sessionId);
		}

		{
			boost::system::error_code errorCode;
			_socket.shutdown(shutdownType, errorCode);

			if(errorCode)
			{
				LOG_F("Session Shutdown : {} {}", errorCode.message(), _sessionId);
			}
			else
			{
				LOG_F("Session Shutdown : {} {}", FROM_ENUM(shutdownType), _sessionId);
			}
		}

		{
			boost::system::error_code errorCode;
			_socket.close(errorCode);

			if(errorCode)
			{
				LOG_F("Session Close : {} {}", errorCode.message(), _sessionId);
			}
			else
			{
				LOG_F("Session Close : {}", _sessionId);
			}

			_sessionManager->RemoveSession(_sessionId);
		}
	}
}
