#include "stdafx.h"
#include "ServerApp.h"

#include "Config.h"
#include <Network/Session/DBServerSession.h>

namespace yaong::network
{
	ServerApp::ServerApp()
		: _clientAcceptor(_ioContext)
	{
	}

	ServerApp::~ServerApp()
	{
		Finalize();
	}

	bool ServerApp::Initialize()
	{
		_dbServerSession = std::make_shared<DBServerSession>(_ioContext);

		if(!ConnectToDBServer())
		{
			return false;
		}

		try
		{
			boost::asio::ip::tcp::endpoint endPoint(
				boost::asio::ip::make_address(Config::GameServerIp), Config::GameServerPort);

			_clientAcceptor.open(endPoint.protocol());
			_clientAcceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
			_clientAcceptor.bind(endPoint);
			_clientAcceptor.listen();

			PostAccept();
		}
		catch(const boost::system::error_code& errorCode)
		{
			LOG_F("Acceptor init falied : {}", errorCode.message());
			return false;
		}
		catch(const std::exception& ex)
		{
			LOG_F("Server init failed : {}", ex.what());
			return false;
		}

		return true;
	}

	void ServerApp::Finalize()
	{
		_threadGroup.join_all();

		CloseAcceptors();

		if(!_ioContext.stopped())
		{
			_ioContext.stop();
		}
	}

	bool ServerApp::Run()
	{
		for(unsigned i = 0; i < std::thread::hardware_concurrency(); ++i)
			_threadGroup.create_thread(boost::bind(&boost::asio::io_context::run, &_ioContext));

		while(true)
		{
			std::this_thread::sleep_for(std::chrono::seconds(1));
		}
	}

	bool ServerApp::PostAccept()
	{
		auto clientSession = _clientSessionManager.CreateSession(_ioContext);
		if(!clientSession)
		{
			LOG_F("Failed to create session");
			return false;
		}

		auto& socket = clientSession->GetSocket();
		_clientAcceptor.async_accept(socket,
			[this, session = std::move(clientSession)]
		(const boost::system::error_code& errorCode)
		{
			if(errorCode)
			{
				LOG_F("PostAccept failed : {}", errorCode.message());
				_clientSessionManager.RemoveSession(session->GetSessionId());
				return;
			}

			if(!session->PostReceive())
			{
				LOG_F("PostReceive failed");
				_clientSessionManager.RemoveSession(session->GetSessionId());
				return;
			}

			boost::system::error_code acceptErrorCode;
			const auto& remoteEndpoint =
				session->GetSocket().remote_endpoint(acceptErrorCode);

			if(acceptErrorCode)
			{
				LOG_F("Remote Endpoint error : {}", acceptErrorCode.message());
				return;
			}

			LOG_F("ClientSession Connected : {} {} {}",
				remoteEndpoint.address().to_string(), remoteEndpoint.port(), session->GetSessionId());

			PostAccept();
		});

		return true;
	}

	bool ServerApp::ConnectToDBServer()
	{
		const boost::asio::ip::tcp::endpoint endpoint(
			boost::asio::ip::make_address(Config::DBServerIp), Config::DBServerPort);

		if(!_dbServerSession->Connect(endpoint))
		{
			return false;
		}

		_dbServerSession->PostReceive();
		return true;
	}

	void ServerApp::CloseAcceptors()
	{
		boost::system::error_code errorCode;
		_clientAcceptor.close(errorCode);
		if(errorCode)
		{
			LOG_F("Acceptor close error : {}", errorCode.message());
		}
	}
}
