#include "stdafx.h"
#include "SessionHeartbeat.h"

namespace yaong::network
{
	SessionHeartbeat::SessionHeartbeat()
		: _createdTimePoint(std::chrono::steady_clock::now())
		, _lastReceivedTimePoint(std::chrono::steady_clock::now())
	{
	}

	bool SessionHeartbeat::IsRequiredHeartbeat() const
	{
		constexpr auto interval = std::chrono::milliseconds(5000);
		return interval < std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::steady_clock::now() - _lastReceivedTimePoint);
	}

	void SessionHeartbeat::UpdateReceivedTime()
	{
		_lastReceivedTimePoint = std::chrono::steady_clock::now();
	}

	auto SessionHeartbeat::GetConnectingTime() const
	{
		return std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::steady_clock::now() - _createdTimePoint).count();
	}
}
