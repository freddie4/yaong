#pragma once

#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <windows.h>
#include <WinSock2.h>
#undef max
#undef min

//#define _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS
#define _SILENCE_CXX17_ITERATOR_BASE_CLASS_DEPRECATION_WARNING
#define _SILENCE_CXX17_ALLOCATOR_VOID_DEPRECATION_WARNING
#define _SILENCE_CXX17_OLD_ALLOCATOR_MEMBERS_DEPRECATION_WARNING

#include <Utility/MacroDefine.h>
#include <Utility/DataTypeDefine.h>

#include <numeric>
#include <limits>

#include <bitset>
#include <any>
#include <array>
#include <vector>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>
#include <tuple>
#include <random>
#include <queue>
#include <initializer_list>
#include <ctime>
#include <iostream>
#include <mutex>
#include <shared_mutex>
#include <functional>

#include <string>
#include <codecvt>

#include <type_traits>

#include <memory>
#include <atomic>
#include <thread>

#include <chrono>
#include <typeinfo>

#include <utility>
#include <optional>

#include <boost/asio.hpp>
#include <boost/thread.hpp>