#include "stdafx.h"
#pragma warning(disable:4800)

#include <Network/Asio/ServerApp.h>
#include <Utility/Logger/Logger.h>
#include <Utility/MiniDump.h>
#include <Utility/MemoryLeakDetector.h>

using namespace yaong;
using namespace network;
using namespace utility;

int main()
{
	MemoryLeakDetector memoryLeakDetector;
	
	static MiniDump miniDump;

	if(false == miniDump.Initialize())
	{
		LOG_F("MiniDump init failed");
		return 0;
	}

	auto gameServerApp = gServerApp;

	if(!gameServerApp->Initialize())
	{
		LOG_F("GameServerApp Init Failed");
		return 0;
	}

	gameServerApp->Run();

	return 0;
}