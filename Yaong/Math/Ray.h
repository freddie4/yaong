#pragma once
#include "Vector.h"

namespace reactphysics3d
{

struct Ray
{
    FVector point1;
    FVector point2;
    float maxFraction;

    Ray(const FVector& p1, const FVector& p2, float maxFrac = float(1.0))
        : point1(p1), point2(p2), maxFraction(maxFrac)
    {}

    Ray(const Ray& ray)
        : point1(ray.point1), point2(ray.point2), maxFraction(ray.maxFraction)
    {}

    ~Ray()
    {}

    Ray& operator=(const Ray& ray)
    {
        if (&ray != this)
        {
            point1 = ray.point1;
            point2 = ray.point2;
            maxFraction = ray.maxFraction;
        }

        return *this;
    }
};

} // namespace reactphysics3d
