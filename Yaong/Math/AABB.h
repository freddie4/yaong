#pragma once
#include <array>
#include "Ray.h"

namespace reactphysics3d
{

class AABB
{
public:
    AABB();
    AABB(const FVector& minCoordinates, const FVector& maxCoordinates);
    AABB(const AABB& aabb);
    ~AABB();

    AABB& operator=(const AABB& aabb);

    FVector GetCenter() const;

    const FVector& GetMin() const;
    void SetMin(const FVector& min);

    const FVector& GetMax() const;
    void SetMax(const FVector& max);

    // Return the size of the AABB in the three dimension x, y and z
    FVector GetExtent() const;

    // Inflate each side of the AABB by a given size
    void Inflate(float dx, float dy, float dz);

    // Return true if the current AABB is overlapping with the AABB in argument
    bool TestCollision(const AABB& aabb) const;

    float GetVolume() const;

    // Merge the AABB in parameter with the current one
    void MergeWithAABB(const AABB& aabb);

    // Replace the current AABB with a new AABB that is the union of two AABBs in parameters
    void MergeTwoAABBs(const AABB& aabb1, const AABB& aabb2);

    // Return true if the current AABB contains the AABB given in parameter
    bool Contains(const AABB& aabb) const;

    // Return true if a point is inside the AABB
    bool Contains(const FVector& point) const;

    // Return true if the AABB of a triangle intersects the AABB
    bool TestCollisionTriangleAABB(const std::array<FVector, 3>& trianglePoints) const;

    // Return true if the ray intersects the AABB
    bool TestRayIntersect(const Ray& ray) const;

    // Create and return an AABB for a triangle
    static AABB CreateAABBForTriangle(const std::array<FVector, 3>& trianglePoints);

    friend class DynamicAABBTree;

private:
    FVector mMinCoordinates;
    FVector mMaxCoordinates;
};

} // namespace reactphysics3d