﻿#pragma once

#include "MemoryAllocator.h"
#include <cstdlib>

namespace reactphysics3d
{

// Class DefaultAllocator
/**
* This class represents a default memory allocator that uses default malloc/free methods
*/
class DefaultAllocator : public MemoryAllocator
{
public:
    /// Destructor
    ~DefaultAllocator() override = default;

    /// Assignment operator
    DefaultAllocator& operator=(DefaultAllocator& allocator) = default;

    /// Allocate memory of a given size (in bytes) and return a pointer to the
    /// allocated memory.
    void* allocate(size_t size) override
    {
        return malloc(size);
    }

    /// Release previously allocated memory.
    void release(void* pointer, size_t size) override
    {
        free(pointer);
    }
};

} // namespace reactphysics3d