﻿#pragma once

#include "DefaultAllocator.h"
#include "PoolAllocator.h"

namespace reactphysics3d
{
// Declarations
class MemoryAllocator;

// Class MemoryManager
/**
* The memory manager is used to store the different memory allocators that are used
* by the library.
*/
class MemoryManager
{
private:
    /// Default malloc/free memory allocator
    static DefaultAllocator mDefaultAllocator;

    /// Pointer to the base memory allocator to use
    static MemoryAllocator* mBaseAllocator;

    /// Memory pool allocator
    PoolAllocator mPoolAllocator;

public:
    /// Memory allocation types
    enum class AllocationType
    {
        Base,   // Base memory allocator
        Pool,   // Memory pool allocator
    };

    /// Constructor
    MemoryManager() = default;

    /// Destructor
    ~MemoryManager() = default;

    /// Allocate memory of a given type
    void* allocate(AllocationType allocationType, size_t size);

    /// Release previously allocated memory.
    void release(AllocationType allocationType, void* pointer, size_t size);

    /// Return the pool allocator
    PoolAllocator& getPoolAllocator();

    /// Return the base memory allocator
    static MemoryAllocator& getBaseAllocator();

    /// Set the base memory allocator
    static void setBaseAllocator(MemoryAllocator* memoryAllocator);
};

// Allocate memory of a given type
inline void* MemoryManager::allocate(AllocationType allocationType, size_t size) {

    switch (allocationType)
    {
    case AllocationType::Base: return mBaseAllocator->allocate(size);
    case AllocationType::Pool: return mPoolAllocator.allocate(size);
    }

    return nullptr;
}

// Release previously allocated memory.
inline void MemoryManager::release(AllocationType allocationType, void* pointer, size_t size) {

    switch (allocationType)
    {
    case AllocationType::Base: mBaseAllocator->release(pointer, size); break;
    case AllocationType::Pool: mPoolAllocator.release(pointer, size); break;
    }
}

// Return the pool allocator
inline PoolAllocator& MemoryManager::getPoolAllocator() {
    return mPoolAllocator;
}

// Return the base memory allocator
inline MemoryAllocator& MemoryManager::getBaseAllocator() {
    return *mBaseAllocator;
}

// Set the base memory allocator
inline void MemoryManager::setBaseAllocator(MemoryAllocator* memoryAllocator) {
    mBaseAllocator = memoryAllocator;
}

} // namespace reactphysics3d