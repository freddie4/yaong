﻿#include "stdafx.h"
#include "MemoryManager.h"

namespace reactphysics3d
{

DefaultAllocator MemoryManager::mDefaultAllocator;
MemoryAllocator* MemoryManager::mBaseAllocator = &mDefaultAllocator;

} // namespace reactphysics3d
