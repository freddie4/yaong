﻿#pragma once
#include "AABB.h"

namespace reactphysics3d
{

class MemoryAllocator;

/// In the broad-phase collision detection (dynamic AABB tree), the AABBs are
/// also inflated in direction of the linear motion of the body by mutliplying the
/// followin constant with the linear velocity and the elapsed time between two frames.
constexpr float DYNAMIC_TREE_AABB_LIN_GAP_MULTIPLIER = float(1.7);

struct TreeNode
{
    const static int32 NULL_TREE_NODE = -1;

    union
    {
        int32 parentId;
        int32 nextNodeId;
    };

    // A node is either a leaf (has data) or is an internal node (has children)
    union
    {
        // Left and right child of the node(children[0] = left child)
        int32 children[2];

        // Two pieces of data stored at that node (in cast the node is a leaf)
        union 
        {
            int32 dataInt[2];
            void* dataPointer;
        };
    };

    // Height of the node int the tree
    int16 height;

    // Fat axis aligned bounding box (AABB) corresponding to the node
    AABB aabb;

    bool IsLeaf() const { return (height == 0); }
};

// Class DynamicAABBTreeOverlapCallback
/**
* Overlapping callback method that has to be used as parameter of the
* reportAllShapesOverlappingWithNode() method.
*/
class DynamicAABBTreeOverlapCallback
{
public:
    // Called when a overlapping node has been found during the call to
    // DynamicAABBTree:reportAllShapesOverlappingWithAABB()
    virtual void NotifyOverlappingNode(int32 nodeId) = 0;

    virtual ~DynamicAABBTreeOverlapCallback() = default;
};

// Class DynamicAABBTreeRaycastCallback
/**
* Raycast callback in the Dynamic AABB Tree called when the AABB of a leaf
* node is hit by the ray.
*/
class DynamicAABBTreeRaycastCallback
{
public:
    // Called when the AABB of a leaf node is hit by a ray
    virtual float RaycastBroadPhaseShape(int32 nodeId, const Ray& ray) = 0;

    virtual ~DynamicAABBTreeRaycastCallback() = default;
};

class DynamicAABBTree
{
public:
    explicit DynamicAABBTree(MemoryAllocator& allocator, float extraAABBGap = 0.0f);
    ~DynamicAABBTree();

    DynamicAABBTree(const DynamicAABBTree& rhs) = delete;
    DynamicAABBTree& operator= (const DynamicAABBTree& rhs) = delete;

    int32 AddObject(const AABB& aabb, int32 data1, int32 data2);
    int32 AddObject(const AABB& aabb, void* data);
    void RemoveObject(int32 nodeId);
    bool UpdateObject(int32 nodeId, const AABB& newAABB, const FVector& displacement,
        bool forceReinsert = false);

    const AABB& GetFatAABB(int32 nodeId) const;
    int32* GetNodeDataInt(int32 nodeId) const;
    void* GetNodeDataPointer(int32 nodeId) const;
    AABB GetRootAABB() const;

    void ReportAllShapesOverlappingWithAABB(const AABB& aabb,
        DynamicAABBTreeOverlapCallback& callback) const;

    void Raycast(const Ray& ray, DynamicAABBTreeRaycastCallback& callback) const;

    int32 ComputeHeight();
    void Reset();

private:
    void Init();

    int32 AllocateNode();
    void ReleaseNode(int32 nodeId);

    void InsertLeafNode(int32 nodeId);
    void RemoveLeafNode(int32 nodeId);

    int32 BalanceSubTreeAtNode(int32 nodeId);

    int32 ComputeHeight(int32 nodeId);

    int32 AddObjectInternal(const AABB& aabb);

    MemoryAllocator& mAllocator;
    TreeNode* mNodes;
    int32 mRootNodeId;
    int32 mFreeNodeId;
    int32 mNbAllocatedNodes;
    int32 mNbNodes;
    float mExtraAABBGap;
};

} // namespace reactphysics3d
