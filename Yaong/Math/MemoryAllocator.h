﻿#pragma once

namespace reactphysics3d
{

// Class MemoryAllocator
/**
* Abstract class with the basic interface of all the derived memory allocators
*/
class MemoryAllocator
{
public:
    /// Constructor
    MemoryAllocator() = default;

    /// Destructor
    virtual ~MemoryAllocator() = default;

    /// Assignment operator
    MemoryAllocator& operator=(MemoryAllocator& allocator) = default;

    /// Allocate memory of a given size (in bytes) and return a pointer to the
    /// allocated memory.
    virtual void* allocate(size_t size) = 0;

    /// Release previously allocated memory.
    virtual void release(void* pointer, size_t size) = 0;
};

} // namespace reactphysics3d
