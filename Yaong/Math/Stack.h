﻿#pragma once

#include "MemoryAllocator.h"

namespace reactphysics3d
{

// Class Stack
/**
* This class represents a simple generic stack with an initial capacity. If the number
* of elements exceeds the capacity, the heap will be used to allocated more memory.
*/
template<typename T, uint32 capacity>
class Stack {

private:

    // -------------------- Attributes -------------------- //

    /// Reference to the memory allocator
    MemoryAllocator& mAllocator;

    /// Initial array that contains the elements of the stack
    T mInitArray[capacity];

    /// Pointer to the first element of the stack
    T* mElements;

    /// Number of elements in the stack
    uint32 mNbElements;

    /// Number of allocated elements in the stack
    uint32 mNbAllocatedElements;

public:

    // -------------------- Methods -------------------- //

    /// Constructor
    Stack(MemoryAllocator& allocator)
        : mAllocator(allocator), mElements(mInitArray), mNbElements(0), mNbAllocatedElements(capacity) {

    }

    /// Destructor
    ~Stack() {

        // If elements have been allocated on the heap
        if (mInitArray != mElements) {

            // Release the memory allocated on the heap
            mAllocator.release(mElements, mNbAllocatedElements * sizeof(T));
        }
    }

    /// Push an element into the stack
    void push(const T& element);

    /// Pop an element from the stack (remove it from the stack and return it)
    T pop();

    /// Return the number of elments in the stack
    uint32 getNbElements() const;

};

// Push an element into the stack
template<typename T, uint32 capacity>
inline void Stack<T, capacity>::push(const T& element) {

    // If we need to allocate more elements
    if (mNbElements == mNbAllocatedElements) {
        T* oldElements = mElements;
        uint32 oldNbAllocatedElements = mNbAllocatedElements;
        mNbAllocatedElements *= 2;
        mElements = static_cast<T*>(mAllocator.allocate(mNbAllocatedElements * sizeof(T)));
        assert(mElements);
        memcpy_s(mElements, mNbAllocatedElements * sizeof(T),
            oldElements, mNbElements * sizeof(T));
        if (oldElements != mInitArray) {
            mAllocator.release(oldElements, oldNbAllocatedElements * sizeof(T));
        }
    }

    mElements[mNbElements] = element;
    mNbElements++;
}

// Pop an element from the stack (remove it from the stack and return it)
template<typename T, uint32 capacity>
inline T Stack<T, capacity>::pop() {
    assert(mNbElements > 0);
    mNbElements--;
    const T poped = mElements[mNbElements];
    mElements[mNbElements] = T();
    return poped;
}

// Return the number of elments in the stack
template<typename T, uint32 capacity>
inline uint32 Stack<T, capacity>::getNbElements() const {
    return mNbElements;
}

} // namespace reactphysics3d
