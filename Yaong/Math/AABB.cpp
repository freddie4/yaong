﻿#include "stdafx.h"
#include "AABB.h"
#include <algorithm>

namespace
{
/// Return the minimum value among three values
inline float min3(float a, float b, float c) {
    return std::min(std::min(a, b), c);
}

/// Return the maximum value among three values
inline float max3(float a, float b, float c) {
    return std::max(std::max(a, b), c);
}

} // namespace unnamed

namespace reactphysics3d
{

AABB::AABB()
    : mMinCoordinates(FVector::ZeroVector)
    , mMaxCoordinates(FVector::ZeroVector)
{
}

AABB::AABB(const FVector& minCoordinates, const FVector& maxCoordinates)
    : mMinCoordinates(minCoordinates)
    , mMaxCoordinates(maxCoordinates)
{
}


AABB::AABB(const AABB& aabb)
    : mMinCoordinates(aabb.mMinCoordinates)
    , mMaxCoordinates(aabb.mMaxCoordinates)
{
}

AABB::~AABB()
{
}

AABB& AABB::operator=(const AABB& aabb)
{
    if (this != &aabb)
    {
        mMinCoordinates = aabb.mMinCoordinates;
        mMaxCoordinates = aabb.mMaxCoordinates;
    }

    return *this;
}

FVector AABB::GetCenter() const
{
    return (mMinCoordinates + mMaxCoordinates) * 0.5f;
}

const FVector& AABB::GetMin() const
{
    return mMinCoordinates;
}

void AABB::SetMin(const FVector& min)
{
    mMinCoordinates = min;
}

const FVector& AABB::GetMax() const
{
    return mMaxCoordinates;
}

void AABB::SetMax(const FVector& max)
{
    mMaxCoordinates = max;
}

FVector AABB::GetExtent() const
{
    return mMaxCoordinates - mMinCoordinates;
}

void AABB::Inflate(float dx, float dy, float dz)
{
    mMaxCoordinates += FVector(dx, dy, dz);
    mMinCoordinates -= FVector(dx, dy, dz);
}

bool AABB::TestCollision(const AABB& aabb) const
{
    if (mMaxCoordinates.X < aabb.mMinCoordinates.X ||
        aabb.mMaxCoordinates.X < mMinCoordinates.X) return false;
    if (mMaxCoordinates.Y < aabb.mMinCoordinates.Y ||
        aabb.mMaxCoordinates.Y < mMinCoordinates.Y) return false;
    if (mMaxCoordinates.Z < aabb.mMinCoordinates.Z ||
        aabb.mMaxCoordinates.Z < mMinCoordinates.Z) return false;
    return true;
}

float AABB::GetVolume() const
{
    const FVector diff = mMaxCoordinates - mMinCoordinates;
    return (diff.X * diff.Y * diff.Z);
}

void AABB::MergeWithAABB(const AABB& aabb)
{
    mMinCoordinates.X = std::min(mMinCoordinates.X, aabb.mMinCoordinates.X);
    mMinCoordinates.Y = std::min(mMinCoordinates.Y, aabb.mMinCoordinates.Y);
    mMinCoordinates.Z = std::min(mMinCoordinates.Z, aabb.mMinCoordinates.Z);

    mMaxCoordinates.X = std::max(mMaxCoordinates.X, aabb.mMaxCoordinates.X);
    mMaxCoordinates.Y = std::max(mMaxCoordinates.Y, aabb.mMaxCoordinates.Y);
    mMaxCoordinates.Z = std::max(mMaxCoordinates.Z, aabb.mMaxCoordinates.Z);
}

void AABB::MergeTwoAABBs(const AABB& aabb1, const AABB& aabb2)
{
    mMinCoordinates.X = std::min(aabb1.mMinCoordinates.X, aabb2.mMinCoordinates.X);
    mMinCoordinates.Y = std::min(aabb1.mMinCoordinates.Y, aabb2.mMinCoordinates.Y);
    mMinCoordinates.Z = std::min(aabb1.mMinCoordinates.Z, aabb2.mMinCoordinates.Z);

    mMaxCoordinates.X = std::max(aabb1.mMaxCoordinates.X, aabb2.mMaxCoordinates.X);
    mMaxCoordinates.Y = std::max(aabb1.mMaxCoordinates.Y, aabb2.mMaxCoordinates.Y);
    mMaxCoordinates.Z = std::max(aabb1.mMaxCoordinates.Z, aabb2.mMaxCoordinates.Z);
}

bool AABB::Contains(const AABB& aabb) const
{
    bool isInside = true;
    isInside = isInside && mMinCoordinates.X <= aabb.mMinCoordinates.X;
    isInside = isInside && mMinCoordinates.Y <= aabb.mMinCoordinates.Y;
    isInside = isInside && mMinCoordinates.Z <= aabb.mMinCoordinates.Z;

    isInside = isInside && mMaxCoordinates.X >= aabb.mMaxCoordinates.X;
    isInside = isInside && mMaxCoordinates.Y >= aabb.mMaxCoordinates.Y;
    isInside = isInside && mMaxCoordinates.Z >= aabb.mMaxCoordinates.Z;
    return isInside;
}

bool AABB::Contains(const FVector& point) const
{
    const float MACHINE_EPSILON = std::numeric_limits<float>::epsilon();

    return (point.X >= mMinCoordinates.X - MACHINE_EPSILON && point.X <= mMaxCoordinates.X + MACHINE_EPSILON &&
            point.Y >= mMinCoordinates.Y - MACHINE_EPSILON && point.Y <= mMaxCoordinates.Y + MACHINE_EPSILON &&
            point.Z >= mMinCoordinates.Z - MACHINE_EPSILON && point.Z <= mMaxCoordinates.Z + MACHINE_EPSILON);
}

bool AABB::TestCollisionTriangleAABB(const std::array<FVector, 3>& trianglePoints) const
{
    if (min3(trianglePoints[0].X, trianglePoints[1].X, trianglePoints[2].X) > mMaxCoordinates.X) return false;
    if (min3(trianglePoints[0].Y, trianglePoints[1].Y, trianglePoints[2].Y) > mMaxCoordinates.Y) return false;
    if (min3(trianglePoints[0].Z, trianglePoints[1].Z, trianglePoints[2].Z) > mMaxCoordinates.Z) return false;

    if (max3(trianglePoints[0].X, trianglePoints[1].X, trianglePoints[2].X) < mMinCoordinates.X) return false;
    if (max3(trianglePoints[0].Y, trianglePoints[1].Y, trianglePoints[2].Y) < mMinCoordinates.Y) return false;
    if (max3(trianglePoints[0].Z, trianglePoints[1].Z, trianglePoints[2].Z) < mMinCoordinates.Z) return false;

    return true;
}

bool AABB::TestRayIntersect(const Ray& ray) const
{
    const FVector point2 = ray.point1 + ray.maxFraction * (ray.point2 - ray.point1);
    const FVector e = mMaxCoordinates - mMinCoordinates;
    const FVector d = point2 - ray.point1;
    const FVector m = ray.point1 + point2 - mMinCoordinates - mMaxCoordinates;

    // Test if the AABB face normals are separating axis
    float adx = std::abs(d.X);
    if (std::abs(m.X) > e.X + adx) return false;
    float ady = std::abs(d.Y);
    if (std::abs(m.Y) > e.Y + ady) return false;
    float adz = std::abs(d.Z);
    if (std::abs(m.Z) > e.Z + adz) return false;

    // Add in an epsilon term to counteract arithmetic errors when segment is
    // (near) parallel to a coordinate axis (see text for detail)
    const float MACHINE_EPSILON = std::numeric_limits<float>::epsilon();
    adx += MACHINE_EPSILON;
    ady += MACHINE_EPSILON;
    adz += MACHINE_EPSILON;

    // Test if the cross products between face normals and ray direction are
    // separating axis
    if (std::abs(m.Y * d.Z - m.Z * d.Y) > e.Y * adz + e.Z * ady) return false;
    if (std::abs(m.Z * d.X - m.X * d.Z) > e.X * adz + e.Z * adx) return false;
    if (std::abs(m.X * d.Y - m.Y * d.X) > e.X * ady + e.Y * adx) return false;

    // No separating axis has been found
    return true;
}

AABB AABB::CreateAABBForTriangle(const std::array<FVector, 3>& trianglePoints)
{
    FVector minCoords(trianglePoints[0].X, trianglePoints[0].Y, trianglePoints[0].Z);
    FVector maxCoords(trianglePoints[0].X, trianglePoints[0].Y, trianglePoints[0].Z);

    if (trianglePoints[1].X < minCoords.X) minCoords.X = trianglePoints[1].X;
    if (trianglePoints[1].Y < minCoords.Y) minCoords.Y = trianglePoints[1].Y;
    if (trianglePoints[1].Z < minCoords.Z) minCoords.Z = trianglePoints[1].Z;

    if (trianglePoints[2].X < minCoords.X) minCoords.X = trianglePoints[2].X;
    if (trianglePoints[2].Y < minCoords.Y) minCoords.Y = trianglePoints[2].Y;
    if (trianglePoints[2].Z < minCoords.Z) minCoords.Z = trianglePoints[2].Z;

    if (trianglePoints[1].X > maxCoords.X) maxCoords.X = trianglePoints[1].X;
    if (trianglePoints[1].Y > maxCoords.Y) maxCoords.Y = trianglePoints[1].Y;
    if (trianglePoints[1].Z > maxCoords.Z) maxCoords.Z = trianglePoints[1].Z;

    if (trianglePoints[2].X > maxCoords.X) maxCoords.X = trianglePoints[2].X;
    if (trianglePoints[2].Y > maxCoords.Y) maxCoords.Y = trianglePoints[2].Y;
    if (trianglePoints[2].Z > maxCoords.Z) maxCoords.Z = trianglePoints[2].Z;

    return AABB(minCoords, maxCoords);
}

} // namespace reactphysics3d

