#pragma once

#include "MathUtility.h"

namespace EAxis
{
    enum Type
    {
        None,
        X,
        Y,
        Z,
    };
}