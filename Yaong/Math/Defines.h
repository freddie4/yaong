#pragma once

typedef signed char int8;
typedef signed short int16;
typedef signed int int32;
typedef signed __int64 int64;

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef unsigned __int64 uint64;

#undef  PI
#define PI 					(3.1415926535897932f)
#define SMALL_NUMBER		(1.e-8f)
#define KINDA_SMALL_NUMBER	(1.e-4f)
#define BIG_NUMBER			(3.4e+38f)
#define EULERS_NUMBER       (2.71828182845904523536f)

#define INV_PI			(0.31830988618f)
#define HALF_PI			(1.57079632679f)

#define DELTA			(0.00001f)

#define DECLARE_VECTOR_REGISTER(X, Y, Z, W) { X, Y, Z, W }

#define MIN_uint8		((unsigned char)	0x00)
#define	MIN_usigned short		((unsigned short)	0x0000)
#define	MIN_uint		((unsigned int)	0x00000000)
#define MIN_uint64		((unsigned long long)	0x0000000000000000)
#define MIN_int8		((signed char)		-128)
#define MIN_signed short		((signed short)	-32768)
#define MIN_int		((signed int)	0x80000000)
#define MIN_int64		((signed long long)	0x8000000000000000)

#define MAX_uint8		((unsigned char)	0xff)
#define MAX_unsigned short		((unsigned short)	0xffff)
#define MAX_uint		((unsigned int)	0xffffffff)
#define MAX_uint64		((unsigned long long)	0xffffffffffffffff)
#define MAX_int8		((signed char)		0x7f)
#define MAX_signed short		((signed short)	0x7fff)
#define MAX_int		((signed int)	0x7fffffff)
#define MAX_int64		((signed long long)	0x7fffffffffffffff)

#define MIN_flt			(1.175494351e-38F)			/* min positive value */
#define MAX_flt			(3.402823466e+38F)
#define MIN_dbl			(2.2250738585072014e-308)	/* min positive value */
#define MAX_dbl			(1.7976931348623158e+308)	

#define THRESH_POINT_ON_PLANE			(0.10f)		/* Thickness of plane for front/back/inside test */
#define THRESH_POINT_ON_SIDE			(0.20f)		/* Thickness of polygon side's side-plane for point-inside/outside/on side test */
#define THRESH_POINTS_ARE_SAME			(0.00002f)	/* Two points are same if within this distance */
#define THRESH_POINTS_ARE_NEAR			(0.015f)	/* Two points are near if within this distance and can be combined if imprecise math is ok */
#define THRESH_NORMALS_ARE_SAME			(0.00002f)	/* Two normal points are same if within this distance */
/* Making this too large results in incorrect CSG classification and disaster */
#define THRESH_VECTORS_ARE_NEAR			(0.0004f)	/* Two vectors are near if within this distance and can be combined if imprecise math is ok */
                                                    /* Making this too large results in lighting problems due to inaccurate texture coordinates */
#define THRESH_SPLIT_POLY_WITH_PLANE	(0.25f)		/* A plane splits a polygon in half */
#define THRESH_SPLIT_POLY_PRECISELY		(0.01f)		/* A plane exactly splits a polygon */
#define THRESH_ZERO_NORM_SQUARED		(0.0001f)	/* Size of a unit normal that is considered "zero", squared */
#define THRESH_NORMALS_ARE_PARALLEL		(0.999845f)	/* Two unit vectors are parallel if abs(A dot B) is greater than or equal to this. This is roughly cosine(1.0 degrees). */
#define THRESH_NORMALS_ARE_ORTHOGONAL	(0.017455f)	/* Two unit vectors are orthogonal (perpendicular) if abs(A dot B) is less than or equal this. This is roughly cosine(89.0 degrees). */


#define THRESH_VECTOR_NORMALIZED		(0.01f)		/** Allowed error for a normalized vector (against squared magnitude) */
#define THRESH_QUAT_NORMALIZED			(0.01f)		/** Allowed error for a normalized quaternion (against squared magnitude) */

#define RAND_MAX 0x7fff

#define GCC_ALIGN(n) __attribute__((aligned(n)))

#define FORCEINLINE __forceinline

#define CORE_API

enum { INDEX_NONE = -1 };