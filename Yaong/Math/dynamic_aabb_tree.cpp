﻿#include "stdafx.h"
#include "dynamic_aabb_tree.h"

#include "Stack.h"
#include <Utility/Logger/Logger.h>

namespace reactphysics3d
{

DynamicAABBTree::DynamicAABBTree(MemoryAllocator& allocator, float extraAABBGap /*= 0.0f*/)
    : mAllocator(allocator)
    , mNodes(nullptr)
    , mRootNodeId()
    , mFreeNodeId()
    , mNbAllocatedNodes()
    , mNbNodes()
    , mExtraAABBGap(extraAABBGap)
{
    Init();
}

DynamicAABBTree::~DynamicAABBTree()
{
    mAllocator.release(mNodes, mNbAllocatedNodes * sizeof(TreeNode));
}

int32 DynamicAABBTree::AddObject(const AABB& aabb, int32 data1, int32 data2)
{
    int32 nodeId = AddObjectInternal(aabb);

    mNodes[nodeId].dataInt[0] = data1;
    mNodes[nodeId].dataInt[1] = data2;

    return nodeId;
}

int32 DynamicAABBTree::AddObject(const AABB& aabb, void* data)
{
    int32 nodeId = AddObjectInternal(aabb);

    mNodes[nodeId].dataPointer = data;

    return nodeId;
}

void DynamicAABBTree::RemoveObject(int32 nodeId)
{
    assert(nodeId >= 0 && nodeId < mNbAllocatedNodes);
    assert(mNodes[nodeId].IsLeaf());

    RemoveLeafNode(nodeId);
    ReleaseNode(nodeId);
}

bool DynamicAABBTree::UpdateObject(int32 nodeId, const AABB& newAABB, const FVector& displacement,
    bool forceReinsert /*= false*/)
{
    assert(nodeId >= 0 && nodeId < mNbAllocatedNodes);
    assert(mNodes[nodeId].IsLeaf());
    assert(mNodes[nodeId].height >= 0);

    if (!forceReinsert && mNodes[nodeId].aabb.Contains(newAABB))
        return false;

    // If the new AABB is outside the fat AABB, we remove the corresponding node
    RemoveLeafNode(nodeId);

    // Compute the fat AABB by inflating the AABB with a constant gap
    mNodes[nodeId].aabb = newAABB;
    const FVector gap(mExtraAABBGap, mExtraAABBGap, mExtraAABBGap);
    mNodes[nodeId].aabb.mMinCoordinates -= gap;
    mNodes[nodeId].aabb.mMaxCoordinates += gap;

    // Inflate the fat AABB in direction of the linear motion of the AABB
    if (displacement.X < float(0.0f))
        mNodes[nodeId].aabb.mMinCoordinates.X += DYNAMIC_TREE_AABB_LIN_GAP_MULTIPLIER * displacement.X;
    else
        mNodes[nodeId].aabb.mMaxCoordinates.X += DYNAMIC_TREE_AABB_LIN_GAP_MULTIPLIER * displacement.X;

    if (displacement.Y < float(0.0f))
        mNodes[nodeId].aabb.mMinCoordinates.Y += DYNAMIC_TREE_AABB_LIN_GAP_MULTIPLIER * displacement.Y;
    else
        mNodes[nodeId].aabb.mMaxCoordinates.Y += DYNAMIC_TREE_AABB_LIN_GAP_MULTIPLIER * displacement.Y;

    if (displacement.Z < float(0.0f))
        mNodes[nodeId].aabb.mMinCoordinates.Z += DYNAMIC_TREE_AABB_LIN_GAP_MULTIPLIER * displacement.Z;
    else
        mNodes[nodeId].aabb.mMaxCoordinates.Z += DYNAMIC_TREE_AABB_LIN_GAP_MULTIPLIER * displacement.Z;

    assert(mNodes[nodeId].aabb.Contains(newAABB));

    // Reinsert the node into the tree
    InsertLeafNode(nodeId);

    return true;
}

const AABB& DynamicAABBTree::GetFatAABB(int32 nodeId) const
{
    assert(nodeId >= 0 && nodeId < mNbAllocatedNodes);
    return mNodes[nodeId].aabb;
}

int32* DynamicAABBTree::GetNodeDataInt(int32 nodeId) const
{
    assert(nodeId >= 0 && nodeId < mNbAllocatedNodes);
    assert(mNodes[nodeId].IsLeaf());
    return mNodes[nodeId].dataInt;
}

void* DynamicAABBTree::GetNodeDataPointer(int32 nodeId) const
{
    assert(nodeId >= 0 && nodeId < mNbAllocatedNodes);
    assert(mNodes[nodeId].IsLeaf());
    return mNodes[nodeId].dataPointer;
}

AABB DynamicAABBTree::GetRootAABB() const
{
    return GetFatAABB(mRootNodeId);
}

void DynamicAABBTree::ReportAllShapesOverlappingWithAABB(const AABB& aabb,
    DynamicAABBTreeOverlapCallback& callback) const
{
    constexpr uint32 stackSize = 256;
    Stack<int32, stackSize> stack(mAllocator);
    stack.push(mRootNodeId);

    // While there are still nodes to visit
    while (stack.getNbElements() > 0)
    {
        const auto nodeIdToVisit = stack.pop();

        if (nodeIdToVisit == TreeNode::NULL_TREE_NODE)
            continue;

        if (nodeIdToVisit >= mNbAllocatedNodes)
        {
            static uint32 loggingCount;

            const auto isLogging = (loggingCount++ % 100 == 0);
            if (isLogging)
            {
                /*LOG_WARN(LOG_FILTER_DYNAMIC_AABB_TREE, "nodeIdToVisit was out of range."
                    " nodeIdToVisit: {}, mNbAllocatedNodes: {}, stackCount: {}, loggingCount: {}",
                    nodeIdToVisit, mNbAllocatedNodes, stack.getNbElements(), loggingCount);*/

                LOG_F("nodeIdToVisit was out of range");
            }

            continue;
        }

        const TreeNode* nodeToVisit = mNodes + nodeIdToVisit;

        if (aabb.TestCollision(nodeToVisit->aabb))
        {
            if (nodeToVisit->IsLeaf())
            {
                // Notify the broad-phase about a new potential overlapping pair
                callback.NotifyOverlappingNode(nodeIdToVisit);
            }
            else
            {
                // We need to visit its children
                stack.push(nodeToVisit->children[0]);
                stack.push(nodeToVisit->children[1]);
            }
        }
    }
}

void DynamicAABBTree::Raycast(const Ray& ray, DynamicAABBTreeRaycastCallback& callback) const
{
    float maxFraction = ray.maxFraction;

    Stack<int32, 128> stack(mAllocator);
    stack.push(mRootNodeId);

    // Walk through the tree from the root looking for proxy shapes
    // that overlap with the ray AABB
    while (stack.getNbElements() > 0)
    {
        int32 nodeId = stack.pop();

        if (nodeId == TreeNode::NULL_TREE_NODE)
            continue;

        const TreeNode* node = mNodes + nodeId;

        Ray rayTemp(ray.point1, ray.point2, maxFraction);

        if (!node->aabb.TestRayIntersect(rayTemp))
            continue;

        if (node->IsLeaf())
        {
            float hitFraction = callback.RaycastBroadPhaseShape(nodeId, rayTemp);

            if (hitFraction == float(0.0f))
                return;

            if (hitFraction > float(0.0f))
            {
                if (hitFraction < maxFraction)
                    maxFraction = hitFraction;
            }
        }
        else
        {
            stack.push(node->children[0]);
            stack.push(node->children[1]);
        }
    }
}

int32 DynamicAABBTree::ComputeHeight()
{
    return ComputeHeight(mRootNodeId);
}

int32 DynamicAABBTree::AddObjectInternal(const AABB& aabb)
{
    int32 nodeId = AllocateNode();

    // Create the fat aabb to use in the tree
    const FVector gap(mExtraAABBGap, mExtraAABBGap, mExtraAABBGap);
    mNodes[nodeId].aabb.SetMin(aabb.GetMin() - gap);
    mNodes[nodeId].aabb.SetMax(aabb.GetMax() + gap);

    // Set the height of the node in the tree
    mNodes[nodeId].height = 0;

    // Insert the new leaf node in the tree
    InsertLeafNode(nodeId);
    assert(mNodes[nodeId].IsLeaf());

    assert(nodeId >= 0);

    return nodeId;
}

void DynamicAABBTree::Reset()
{
    // Free the allocated memory for the nodes
    mAllocator.release(mNodes, mNbAllocatedNodes * sizeof(TreeNode));

    // Initialize the tree
    Init();
}

void DynamicAABBTree::Init()
{
    mRootNodeId = TreeNode::NULL_TREE_NODE;
    mNbNodes = 0;
    mNbAllocatedNodes = 8;

    // Allocate memory for the nodes of the tree
    mNodes = static_cast<TreeNode*>(mAllocator.allocate(mNbAllocatedNodes * sizeof(TreeNode)));
    assert(mNodes);

    // cppcheck-suppress memsetClassFloat
    std::memset(mNodes, 0, mNbAllocatedNodes * sizeof(TreeNode));

    // Initialize the allocated nodes
    for (int32 i = 0; i < mNbAllocatedNodes - 1; ++i)
    {
        mNodes[i].aabb = AABB();
        mNodes[i].nextNodeId = i + 1;
        mNodes[i].height = -1;
    }

    mNodes[mNbAllocatedNodes - 1].nextNodeId = TreeNode::NULL_TREE_NODE;
    mNodes[mNbAllocatedNodes - 1].height = -1;
    mFreeNodeId = 0;
}

int32 DynamicAABBTree::AllocateNode()
{
    if (mFreeNodeId == TreeNode::NULL_TREE_NODE)
    {
        assert(mNbNodes == mNbAllocatedNodes);

        // Allocate more nodes in the tree
        uint32 oldNbAllocatedNodes = mNbAllocatedNodes;
        mNbAllocatedNodes *= 2;
        TreeNode* oldNodes = mNodes;
        mNodes = static_cast<TreeNode*>(mAllocator.allocate(mNbAllocatedNodes * sizeof(TreeNode)));
        assert(mNodes);
        memcpy_s(mNodes, mNbAllocatedNodes * sizeof(TreeNode),
            oldNodes, mNbNodes * sizeof(TreeNode));
        mAllocator.release(oldNodes, oldNbAllocatedNodes * sizeof(TreeNode));

        // Initialize the allocated nodes
        for (int32 i = mNbNodes; i < mNbAllocatedNodes - 1; ++i)
        {
            mNodes[i].nextNodeId = i + 1;
            mNodes[i].height = -1;
        }

        mNodes[mNbAllocatedNodes - 1].nextNodeId = TreeNode::NULL_TREE_NODE;
        mNodes[mNbAllocatedNodes - 1].height = -1;
        mFreeNodeId = mNbNodes;
    }

    // Get the next free node
    int32 freeNodeId = mFreeNodeId;
    mFreeNodeId = mNodes[freeNodeId].nextNodeId;
    mNodes[freeNodeId].parentId = TreeNode::NULL_TREE_NODE;
    mNodes[freeNodeId].height = 0;
    ++mNbNodes;

    return freeNodeId;
}

void DynamicAABBTree::ReleaseNode(int32 nodeId)
{
    assert(mNbNodes > 0);
    assert(nodeId >= 0 && nodeId < mNbAllocatedNodes);
    assert(mNodes[nodeId].height >= 0);
    mNodes[nodeId].nextNodeId = mFreeNodeId;
    mNodes[nodeId].height = -1;
    mFreeNodeId = nodeId;
    --mNbNodes;
}

void DynamicAABBTree::InsertLeafNode(int32 nodeId)
{
    if (mRootNodeId == TreeNode::NULL_TREE_NODE)
    {
        mRootNodeId = nodeId;
        mNodes[mRootNodeId].parentId = TreeNode::NULL_TREE_NODE;
        return;
    }

    assert(mRootNodeId != TreeNode::NULL_TREE_NODE);

    // Find the best sibling node for the new node
    AABB newNodeAABB = mNodes[nodeId].aabb;
    int32 currentNodeId = mRootNodeId;
    while (!mNodes[currentNodeId].IsLeaf())
    {
        int32 leftChild = mNodes[currentNodeId].children[0];
        int32 rightChild = mNodes[currentNodeId].children[1];

        // Compute the merged AABB
        float volumeAABB = mNodes[currentNodeId].aabb.GetVolume();
        AABB mergedAABBs;
        mergedAABBs.MergeTwoAABBs(mNodes[currentNodeId].aabb, newNodeAABB);
        float mergedVolume = mergedAABBs.GetVolume();

        // Compute the cost of making the current node the sibling of the new node
        float costS = float(2.0f) * mergedVolume;

        // Compute the minimum cost of pushing the new node further down the tree
        float costI = float(2.0f) * (mergedVolume - volumeAABB);

        // Compute the cost of descending into the left child
        float costLeft;
        AABB currentAndLeftAABB;
        currentAndLeftAABB.MergeTwoAABBs(newNodeAABB, mNodes[leftChild].aabb);

        if (mNodes[leftChild].IsLeaf())
        {
            costLeft = currentAndLeftAABB.GetVolume() + costI;
        }
        else
        {
            float leftChildVolume = mNodes[leftChild].aabb.GetVolume();
            costLeft = costI + currentAndLeftAABB.GetVolume() - leftChildVolume;
        }

        // Compute the cost of descending into the right child
        float costRight;
        AABB currentAndRightAABB;
        currentAndRightAABB.MergeTwoAABBs(newNodeAABB, mNodes[rightChild].aabb);

        if (mNodes[rightChild].IsLeaf())
        {
            costRight = currentAndRightAABB.GetVolume() + costI;
        }
        else
        {
            float rightChildVolume = mNodes[rightChild].aabb.GetVolume();
            costRight = costI + currentAndRightAABB.GetVolume() - rightChildVolume;
        }

        // If the cost of making the current node a sibling of the new node is smaller than
        // the cost of going down into the left or right child
        if (costS < costLeft && costS < costRight) break;

        // It is cheaper to go down into a child of the current node, choose the best child
        if (costLeft < costRight)
        {
            currentNodeId = leftChild;
        }
        else
        {
            currentNodeId = rightChild;
        }
    }

    int32 siblingNode = currentNodeId;

    // Create a new parent for the new node and the sibling node
    int32 oldParentNode = mNodes[siblingNode].parentId;
    int32 newParentNode = AllocateNode();
    mNodes[newParentNode].parentId = oldParentNode;
    mNodes[newParentNode].aabb.MergeTwoAABBs(mNodes[siblingNode].aabb, newNodeAABB);
    mNodes[newParentNode].height = mNodes[siblingNode].height + 1;
    assert(mNodes[newParentNode].height > 0);

    if (oldParentNode != TreeNode::NULL_TREE_NODE)
    {
        assert(!mNodes[oldParentNode].IsLeaf());
        if (mNodes[oldParentNode].children[0] == siblingNode)
        {
            mNodes[oldParentNode].children[0] = newParentNode;
        }
        else
        {
            mNodes[oldParentNode].children[1] = newParentNode;
        }

        mNodes[newParentNode].children[0] = siblingNode;
        mNodes[newParentNode].children[1] = nodeId;
        mNodes[siblingNode].parentId = newParentNode;
        mNodes[nodeId].parentId = newParentNode;
    }
    else
    {
        mNodes[newParentNode].children[0] = siblingNode;
        mNodes[newParentNode].children[1] = nodeId;
        mNodes[siblingNode].parentId = newParentNode;
        mNodes[nodeId].parentId = newParentNode;
        mRootNodeId = newParentNode;
    }

    // Move up in the tree to change the AABBs that have changed
    currentNodeId = mNodes[nodeId].parentId;
    assert(!mNodes[currentNodeId].IsLeaf());
    while (currentNodeId != TreeNode::NULL_TREE_NODE)
    {
        // Balance the sub-tree of the current node if it is not balanced
        currentNodeId = BalanceSubTreeAtNode(currentNodeId);
        assert(mNodes[nodeId].IsLeaf());

        assert(!mNodes[currentNodeId].IsLeaf());
        int leftChild = mNodes[currentNodeId].children[0];
        int rightChild = mNodes[currentNodeId].children[1];
        assert(leftChild != TreeNode::NULL_TREE_NODE);
        assert(rightChild != TreeNode::NULL_TREE_NODE);

        // Recompute the height of the node in the tree
        mNodes[currentNodeId].height =
            std::max(mNodes[leftChild].height, mNodes[rightChild].height) + 1;
        assert(mNodes[currentNodeId].height > 0);

        // Recompute the AABB of the node
        mNodes[currentNodeId].aabb.MergeTwoAABBs(mNodes[leftChild].aabb, mNodes[rightChild].aabb);

        currentNodeId = mNodes[currentNodeId].parentId;
    }

    assert(mNodes[nodeId].IsLeaf());
}

void DynamicAABBTree::RemoveLeafNode(int32 nodeId)
{
    assert(nodeId >= 0 && nodeId < mNbAllocatedNodes);
    assert(mNodes[nodeId].IsLeaf());

    if (mRootNodeId == nodeId)
    {
        mRootNodeId = TreeNode::NULL_TREE_NODE;
        return;
    }

    int32 parentNodeId = mNodes[nodeId].parentId;
    int32 grandParentNodeId = mNodes[parentNodeId].parentId;
    int32 siblingNodeId;

    if (mNodes[parentNodeId].children[0] == nodeId)
    {
        siblingNodeId = mNodes[parentNodeId].children[1];
    }
    else
    {
        siblingNodeId = mNodes[parentNodeId].children[0];
    }

    if (grandParentNodeId != TreeNode::NULL_TREE_NODE)
    {
        // Destroy the parent node
        if (mNodes[grandParentNodeId].children[0] == parentNodeId)
        {
            mNodes[grandParentNodeId].children[0] = siblingNodeId;
        }
        else
        {
            assert(mNodes[grandParentNodeId].children[1] == parentNodeId);
            mNodes[grandParentNodeId].children[1] = siblingNodeId;
        }

        mNodes[siblingNodeId].parentId = grandParentNodeId;
        ReleaseNode(parentNodeId);

        // Now, we need to recompute the AABBs of the node on the path back to the root
        // and make sure that the tree is still balanced
        int32 currentNodeId = grandParentNodeId;
        while (currentNodeId != TreeNode::NULL_TREE_NODE)
        {
            // Balance the current sub-tree if necessary
            currentNodeId = BalanceSubTreeAtNode(currentNodeId);

            assert(!mNodes[currentNodeId].IsLeaf());

            // Get the two children of the current node
            int32 leftChildId = mNodes[currentNodeId].children[0];
            int32 rightChildId = mNodes[currentNodeId].children[1];

            // Recompute the AABB and the height of the current node
            mNodes[currentNodeId].aabb.MergeTwoAABBs(
                mNodes[leftChildId].aabb, mNodes[rightChildId].aabb);
            mNodes[currentNodeId].height =
                std::max(mNodes[leftChildId].height, mNodes[rightChildId].height) + 1;
            assert(mNodes[currentNodeId].height > 0);

            currentNodeId = mNodes[currentNodeId].parentId;
        }
    }
    else
    {
        // The sibling node becomes the new root node
        mRootNodeId = siblingNodeId;
        mNodes[siblingNodeId].parentId = TreeNode::NULL_TREE_NODE;
        ReleaseNode(parentNodeId);
    }
}

int32 DynamicAABBTree::BalanceSubTreeAtNode(int32 nodeId)
{
    assert(nodeId != TreeNode::NULL_TREE_NODE);

    TreeNode* nodeA = mNodes + nodeId;

    if (nodeA->IsLeaf() || nodeA->height < 2)
    {
        return nodeId;
    }

    int32 nodeBId = nodeA->children[0];
    int32 nodeCId = nodeA->children[1];
    assert(nodeBId >= 0 && nodeBId < mNbAllocatedNodes);
    assert(nodeCId >= 0 && nodeCId < mNbAllocatedNodes);
    TreeNode* nodeB = mNodes + nodeBId;
    TreeNode* nodeC = mNodes + nodeCId;

    // Compute the factor of the left and right sub-trees
    int32 balanceFactor = nodeC->height - nodeB->height;

    if (balanceFactor > 1)
    {
        assert(!nodeC->IsLeaf());

        int32 nodeFId = nodeC->children[0];
        int32 nodeGId = nodeC->children[1];
        assert(nodeFId >= 0 && nodeFId < mNbAllocatedNodes);
        assert(nodeGId >= 0 && nodeGId < mNbAllocatedNodes);
        TreeNode* nodeF = mNodes + nodeFId;
        TreeNode* nodeG = mNodes + nodeGId;

        nodeC->children[0] = nodeId;
        nodeC->parentId = nodeA->parentId;
        nodeA->parentId = nodeCId;

        if (nodeC->parentId != TreeNode::NULL_TREE_NODE)
        {
            if (mNodes[nodeC->parentId].children[0] == nodeId)
            {
                mNodes[nodeC->parentId].children[0] = nodeCId;
            }
            else
            {
                assert(mNodes[nodeC->parentId].children[1] == nodeId);
                mNodes[nodeC->parentId].children[1] = nodeCId;
            }
        }
        else
        {
            mRootNodeId = nodeCId;
        }

        assert(!nodeC->IsLeaf());
        assert(!nodeA->IsLeaf());

        if (nodeF->height > nodeG->height)
        {
            nodeC->children[1] = nodeFId;
            nodeA->children[1] = nodeGId;
            nodeG->parentId = nodeId;

            // Recompute the AABB of node A and C
            nodeA->aabb.MergeTwoAABBs(nodeB->aabb, nodeG->aabb);
            nodeC->aabb.MergeTwoAABBs(nodeA->aabb, nodeF->aabb);

            // Recompute the height of node A and C
            nodeA->height = std::max(nodeB->height, nodeG->height) + 1;
            nodeC->height = std::max(nodeA->height, nodeF->height) + 1;
            assert(nodeA->height > 0);
            assert(nodeC->height > 0);
        }
        else
        {
            nodeC->children[1] = nodeGId;
            nodeA->children[1] = nodeFId;
            nodeF->parentId = nodeId;

            // Recompute the AABB of node A and C
            nodeA->aabb.MergeTwoAABBs(nodeB->aabb, nodeF->aabb);
            nodeC->aabb.MergeTwoAABBs(nodeA->aabb, nodeG->aabb);

            // Recompute the height of node A and C
            nodeA->height = std::max(nodeB->height, nodeF->height) + 1;
            nodeC->height = std::max(nodeA->height, nodeG->height) + 1;
            assert(nodeA->height > 0);
            assert(nodeC->height > 0);
        }

        // Return the new root of the sub-tree
        return nodeCId;
    }

    if (balanceFactor < -1)
    {
        assert(!nodeB->IsLeaf());

        int32 nodeFId = nodeB->children[0];
        int32 nodeGId = nodeB->children[1];
        assert(nodeFId >= 0 && nodeFId < mNbAllocatedNodes);
        assert(nodeGId >= 0 && nodeGId < mNbAllocatedNodes);
        TreeNode* nodeF = mNodes + nodeFId;
        TreeNode* nodeG = mNodes + nodeGId;

        nodeB->children[0] = nodeId;
        nodeB->parentId = nodeA->parentId;
        nodeA->parentId = nodeBId;

        if (nodeB->parentId != TreeNode::NULL_TREE_NODE)
        {
            if (mNodes[nodeB->parentId].children[0] == nodeId)
            {
                mNodes[nodeB->parentId].children[0] = nodeBId;
            }
            else
            {
                assert(mNodes[nodeB->parentId].children[1] == nodeId);
                mNodes[nodeB->parentId].children[1] = nodeBId;
            }
        }
        else
        {
            mRootNodeId = nodeBId;
        }

        assert(!nodeB->IsLeaf());
        assert(!nodeA->IsLeaf());

        if (nodeF->height > nodeG->height)
        {
            nodeB->children[1] = nodeFId;
            nodeA->children[0] = nodeGId;
            nodeG->parentId = nodeId;

            // Recompute the AABB of node A and B
            nodeA->aabb.MergeTwoAABBs(nodeC->aabb, nodeG->aabb);
            nodeB->aabb.MergeTwoAABBs(nodeA->aabb, nodeF->aabb);

            // Recompute the height of node A and B
            nodeA->height = std::max(nodeC->height, nodeG->height) + 1;
            nodeB->height = std::max(nodeA->height, nodeF->height) + 1;
            assert(nodeA->height > 0);
            assert(nodeB->height > 0);
        }
        else
        {
            nodeB->children[1] = nodeGId;
            nodeA->children[0] = nodeFId;
            nodeF->parentId = nodeId;

            // Recompute the AABB of node A and B
            nodeA->aabb.MergeTwoAABBs(nodeC->aabb, nodeF->aabb);
            nodeB->aabb.MergeTwoAABBs(nodeA->aabb, nodeG->aabb);

            // Recompute the height of node A and B
            nodeA->height = std::max(nodeC->height, nodeF->height) + 1;
            nodeB->height = std::max(nodeA->height, nodeG->height) + 1;
            assert(nodeA->height > 0);
            assert(nodeB->height > 0);
        }

        // Return the new root of the sub-tree
        return nodeBId;
    }

    // If the sub-tree is balanced, return the current root node
    return nodeId;
}

int32 DynamicAABBTree::ComputeHeight(int32 nodeId)
{
    assert(nodeId >= 0 && nodeId < mNbAllocatedNodes);
    TreeNode* node = mNodes + nodeId;

    if (node->IsLeaf())
        return 0;

    // Compute the height of the left and right sub-tree
    int32 leftHeight = ComputeHeight(node->children[0]);
    int32 rightHeight = ComputeHeight(node->children[1]);

    return 1 + std::max(leftHeight, rightHeight);
}

} // namespace reactphysics3d
