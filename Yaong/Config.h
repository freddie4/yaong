#pragma once

namespace yaong
{
	class Config
	{
	public:
		static constexpr int GameServerPort = 6898;
		static constexpr std::string_view GameServerIp = "127.0.0.1";

		static constexpr int DBServerPort = 6899;
		static constexpr std::string_view DBServerIp = "127.0.0.1";
	};
}