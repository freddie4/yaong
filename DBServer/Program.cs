﻿using System;
using log4net;
using SuperSocket.SocketBase.Config;
using DBServer.Network;

namespace DBServer
{
    class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            var appServer = new DBAppServer();
            if (!appServer.Initialize())
            {
                Log.Error("Fail to init DB Server");
                return;
            }

            var config = new ServerConfig
            {
                Ip = "Any",
                Port = 6899,
                MaxRequestLength = 2097152
            };

            if (!appServer.Setup(new RootConfig(), config))
            {
                Log.Error("Fail to setup config");
                return;
            }

            if (!appServer.Start())
            {
                Log.Error("Fail to Start DB Server");
                return;
            }

            Console.WriteLine("The server started successfully, press key 'q' to stop it!");
            Log.Info("The DBServer is started.");

            while (Console.ReadKey().KeyChar != 'q')
            {
                Console.WriteLine();
            }

            Log.Info("The DBServer is stopped.");
            appServer.Stop();
        }
    }
}
