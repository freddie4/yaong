﻿
using System;
using System.Data;
using System.Data.SqlClient;
using DBServer.Config;
using log4net;

namespace DBServer.Network
{
    public partial class PacketSDHandler
    {
        public void Register()
        {
            PacketHandlerManager.Register<PacketSD.Auth>(PacketSD.ProtocolNumber.Eauth);
        }
    }

    public partial class PacketHandler<TSession, TProtobuf>
    {
        private static readonly ILog Log = LogManager.GetLogger("PacketHandler");

        private IDbConnection GetConnection(string connectionName)
        {
            return DBAppServer.GetConnection(connectionName);
        }

        private void Handle(GameServerSession session, PacketSD.Auth packet)
        {
            var jsonTokenId = Guid.NewGuid();
            var error = new Error.ErrorNumber();

            try
            {
                using (var connection = GetConnection(DBServerConfig.GameDBName))
                using (var transaction = connection.BeginTransaction())
                {
                    var sql = new SqlCommand("SELECT * FROM accounts " +
                                             $"WHERE account_id = {packet.AccountId} " +
                                             $"and password = {packet.Password}");
                    var reader = sql.ExecuteReader();

                    if (reader.HasRows)
                    {
                        if (reader.Read())
                        {
                            // Auth success
                        }
                    }
                    else
                    {
                        error = Error.ErrorNumber.AuthAccountOrPasswordIsWrong;
                        throw new Exception($"Account or password is wrong : " +
                                            $"{packet.AccountId} ," +
                                            $"{packet.Password}");
                    }

                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (error == Error.ErrorNumber.None)
                    error = Error.ErrorNumber.Exception;
                Log.Error(ex.ToString());
            }

            var sendPacket = new PacketDS.AuthRes
            {
                SessionId = packet.SessionId,
                AccountId = packet.AccountId,
                JsonTokenId = jsonTokenId.ToString(),
                Error = error
            };

            session.SendPacket(sendPacket);
        }

        private void Handle(GameServerSession session, PacketSD.Join packet)
        {
            var error = new Error.ErrorNumber();

            try
            {
                using (var connection = GetConnection(DBServerConfig.GameDBName))
                using (var transaction = connection.BeginTransaction())
                {
                    var sqlSelect = new SqlCommand("SELECT * FROM accounts " +
                                             $"WHERE account_id = {packet.AccountId}");
                    var reader = sqlSelect.ExecuteReader();

                    if (reader.HasRows)
                    {
                        if (reader.Read())
                        {
                            error = Error.ErrorNumber.JoinAccountIdAlreadyExists;
                            throw new Exception("AccountId already exist : " +
                                                $"{packet.AccountId}");
                        }
                    }
                    else
                    {
                        var sqlInsert = new SqlCommand("INSERT INTO accounts (account_id, password)" +
                                                       $"VALUES ({packet.AccountId}, {packet.Password})");

                        var result = sqlInsert.ExecuteNonQuery();

                        if (result == -1)
                        {
                            error = Error.ErrorNumber.Exception;
                            throw new Exception("Insert failed : " +
                                                $"{packet.AccountId}");
                        }
                    }

                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (error == Error.ErrorNumber.None)
                    error = Error.ErrorNumber.Exception;
                Log.Error(ex.ToString());
            }

            var sendPacket = new PacketDS.JoinRes
            {
                SessionId = packet.SessionId,
                AccountId = packet.AccountId,
                Password = packet.Password,
                Error = error
            };

            session.SendPacket(sendPacket);
        }
    }
}
