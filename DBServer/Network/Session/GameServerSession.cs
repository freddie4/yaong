﻿using System;
using System.Configuration;
using DBServer.Config;
using Google.Protobuf;
using log4net;
using SuperSocket.SocketBase;

namespace DBServer.Network
{
    public class GameServerSession : AppSession<GameServerSession, CustomRequestInfo>
    {
        public void SendPacket(IMessage packet)
        {
            var sendBuffer = CustomRequestInfo.Serialize(packet);
            Send(sendBuffer, 0, sendBuffer.Length);
        }
    }

    public partial class PacketSDHandler : SessionHandlerBase<GameServerSession>
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(PacketSDHandler));

        public bool IsDbConnectable()
        {
            try
            {
                using (var game = DBAppServer.GetConnection(
                    DBServerConfig.GameDBName))
                {
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }

            return true;
        }

        public override bool Initialize()
        {
            if (!IsDbConnectable())
                return false;

            Register();

            return true;
        }

        public override void Cleanup()
        {
            base.Cleanup();
        }
    }
}
