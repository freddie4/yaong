// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: PacketDSDefine.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace PacketDS {

  /// <summary>Holder for reflection information generated from PacketDSDefine.proto</summary>
  public static partial class PacketDSDefineReflection {

    #region Descriptor
    /// <summary>File descriptor for PacketDSDefine.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static PacketDSDefineReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "ChRQYWNrZXREU0RlZmluZS5wcm90bxIIUGFja2V0RFMaEUVycm9yRGVmaW5l",
            "LnByb3RvImsKB0F1dGhSZXMSEgoKc2Vzc2lvbl9pZBgBIAEoBRISCgphY2Nv",
            "dW50X2lkGAIgASgJEhUKDWpzb25fdG9rZW5faWQYAyABKAkSIQoFZXJyb3IY",
            "BCABKA4yEi5FcnJvci5FcnJvck51bWJlciJmCgdKb2luUmVzEhIKCnNlc3Np",
            "b25faWQYASABKAUSEgoKYWNjb3VudF9pZBgCIAEoCRIQCghwYXNzd29yZBgD",
            "IAEoCRIhCgVlcnJvchgEIAEoDjISLkVycm9yLkVycm9yTnVtYmVyKiwKDlBy",
            "b3RvY29sTnVtYmVyEgwKCEVBdXRoUmVzEAASDAoIRUpvaW5SZXMQAVAAYgZw",
            "cm90bzM="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { global::Error.ErrorDefineReflection.Descriptor, },
          new pbr::GeneratedClrTypeInfo(new[] {typeof(global::PacketDS.ProtocolNumber), }, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::PacketDS.AuthRes), global::PacketDS.AuthRes.Parser, new[]{ "SessionId", "AccountId", "JsonTokenId", "Error" }, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::PacketDS.JoinRes), global::PacketDS.JoinRes.Parser, new[]{ "SessionId", "AccountId", "Password", "Error" }, null, null, null)
          }));
    }
    #endregion

  }
  #region Enums
  public enum ProtocolNumber {
    [pbr::OriginalName("EAuthRes")] EauthRes = 0,
    [pbr::OriginalName("EJoinRes")] EjoinRes = 1,
  }

  #endregion

  #region Messages
  public sealed partial class AuthRes : pb::IMessage<AuthRes> {
    private static readonly pb::MessageParser<AuthRes> _parser = new pb::MessageParser<AuthRes>(() => new AuthRes());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<AuthRes> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::PacketDS.PacketDSDefineReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public AuthRes() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public AuthRes(AuthRes other) : this() {
      sessionId_ = other.sessionId_;
      accountId_ = other.accountId_;
      jsonTokenId_ = other.jsonTokenId_;
      error_ = other.error_;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public AuthRes Clone() {
      return new AuthRes(this);
    }

    /// <summary>Field number for the "session_id" field.</summary>
    public const int SessionIdFieldNumber = 1;
    private int sessionId_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int SessionId {
      get { return sessionId_; }
      set {
        sessionId_ = value;
      }
    }

    /// <summary>Field number for the "account_id" field.</summary>
    public const int AccountIdFieldNumber = 2;
    private string accountId_ = "";
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public string AccountId {
      get { return accountId_; }
      set {
        accountId_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }

    /// <summary>Field number for the "json_token_id" field.</summary>
    public const int JsonTokenIdFieldNumber = 3;
    private string jsonTokenId_ = "";
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public string JsonTokenId {
      get { return jsonTokenId_; }
      set {
        jsonTokenId_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }

    /// <summary>Field number for the "error" field.</summary>
    public const int ErrorFieldNumber = 4;
    private global::Error.ErrorNumber error_ = 0;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Error.ErrorNumber Error {
      get { return error_; }
      set {
        error_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as AuthRes);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(AuthRes other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (SessionId != other.SessionId) return false;
      if (AccountId != other.AccountId) return false;
      if (JsonTokenId != other.JsonTokenId) return false;
      if (Error != other.Error) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (SessionId != 0) hash ^= SessionId.GetHashCode();
      if (AccountId.Length != 0) hash ^= AccountId.GetHashCode();
      if (JsonTokenId.Length != 0) hash ^= JsonTokenId.GetHashCode();
      if (Error != 0) hash ^= Error.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (SessionId != 0) {
        output.WriteRawTag(8);
        output.WriteInt32(SessionId);
      }
      if (AccountId.Length != 0) {
        output.WriteRawTag(18);
        output.WriteString(AccountId);
      }
      if (JsonTokenId.Length != 0) {
        output.WriteRawTag(26);
        output.WriteString(JsonTokenId);
      }
      if (Error != 0) {
        output.WriteRawTag(32);
        output.WriteEnum((int) Error);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (SessionId != 0) {
        size += 1 + pb::CodedOutputStream.ComputeInt32Size(SessionId);
      }
      if (AccountId.Length != 0) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(AccountId);
      }
      if (JsonTokenId.Length != 0) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(JsonTokenId);
      }
      if (Error != 0) {
        size += 1 + pb::CodedOutputStream.ComputeEnumSize((int) Error);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(AuthRes other) {
      if (other == null) {
        return;
      }
      if (other.SessionId != 0) {
        SessionId = other.SessionId;
      }
      if (other.AccountId.Length != 0) {
        AccountId = other.AccountId;
      }
      if (other.JsonTokenId.Length != 0) {
        JsonTokenId = other.JsonTokenId;
      }
      if (other.Error != 0) {
        Error = other.Error;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 8: {
            SessionId = input.ReadInt32();
            break;
          }
          case 18: {
            AccountId = input.ReadString();
            break;
          }
          case 26: {
            JsonTokenId = input.ReadString();
            break;
          }
          case 32: {
            error_ = (global::Error.ErrorNumber) input.ReadEnum();
            break;
          }
        }
      }
    }

  }

  public sealed partial class JoinRes : pb::IMessage<JoinRes> {
    private static readonly pb::MessageParser<JoinRes> _parser = new pb::MessageParser<JoinRes>(() => new JoinRes());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<JoinRes> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::PacketDS.PacketDSDefineReflection.Descriptor.MessageTypes[1]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public JoinRes() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public JoinRes(JoinRes other) : this() {
      sessionId_ = other.sessionId_;
      accountId_ = other.accountId_;
      password_ = other.password_;
      error_ = other.error_;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public JoinRes Clone() {
      return new JoinRes(this);
    }

    /// <summary>Field number for the "session_id" field.</summary>
    public const int SessionIdFieldNumber = 1;
    private int sessionId_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int SessionId {
      get { return sessionId_; }
      set {
        sessionId_ = value;
      }
    }

    /// <summary>Field number for the "account_id" field.</summary>
    public const int AccountIdFieldNumber = 2;
    private string accountId_ = "";
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public string AccountId {
      get { return accountId_; }
      set {
        accountId_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }

    /// <summary>Field number for the "password" field.</summary>
    public const int PasswordFieldNumber = 3;
    private string password_ = "";
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public string Password {
      get { return password_; }
      set {
        password_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }

    /// <summary>Field number for the "error" field.</summary>
    public const int ErrorFieldNumber = 4;
    private global::Error.ErrorNumber error_ = 0;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Error.ErrorNumber Error {
      get { return error_; }
      set {
        error_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as JoinRes);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(JoinRes other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (SessionId != other.SessionId) return false;
      if (AccountId != other.AccountId) return false;
      if (Password != other.Password) return false;
      if (Error != other.Error) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (SessionId != 0) hash ^= SessionId.GetHashCode();
      if (AccountId.Length != 0) hash ^= AccountId.GetHashCode();
      if (Password.Length != 0) hash ^= Password.GetHashCode();
      if (Error != 0) hash ^= Error.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (SessionId != 0) {
        output.WriteRawTag(8);
        output.WriteInt32(SessionId);
      }
      if (AccountId.Length != 0) {
        output.WriteRawTag(18);
        output.WriteString(AccountId);
      }
      if (Password.Length != 0) {
        output.WriteRawTag(26);
        output.WriteString(Password);
      }
      if (Error != 0) {
        output.WriteRawTag(32);
        output.WriteEnum((int) Error);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (SessionId != 0) {
        size += 1 + pb::CodedOutputStream.ComputeInt32Size(SessionId);
      }
      if (AccountId.Length != 0) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(AccountId);
      }
      if (Password.Length != 0) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Password);
      }
      if (Error != 0) {
        size += 1 + pb::CodedOutputStream.ComputeEnumSize((int) Error);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(JoinRes other) {
      if (other == null) {
        return;
      }
      if (other.SessionId != 0) {
        SessionId = other.SessionId;
      }
      if (other.AccountId.Length != 0) {
        AccountId = other.AccountId;
      }
      if (other.Password.Length != 0) {
        Password = other.Password;
      }
      if (other.Error != 0) {
        Error = other.Error;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 8: {
            SessionId = input.ReadInt32();
            break;
          }
          case 18: {
            AccountId = input.ReadString();
            break;
          }
          case 26: {
            Password = input.ReadString();
            break;
          }
          case 32: {
            error_ = (global::Error.ErrorNumber) input.ReadEnum();
            break;
          }
        }
      }
    }

  }

  #endregion

}

#endregion Designer generated code
