﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Google.Protobuf;
using log4net;

namespace DBServer.Network
{
    internal interface IPacketHandlerBase<in TSession>
    {
        bool Handle(TSession session, CustomRequestInfo requestInfo);
    }

    public partial class PacketHandler<TSession, TProtobuf> : IPacketHandlerBase<TSession>
        where TProtobuf : IMessage<TProtobuf>, new()
    {
        private readonly MessageParser<TProtobuf> Parser = new MessageParser<TProtobuf>(() => new TProtobuf());

        bool IPacketHandlerBase<TSession>.Handle(TSession session, CustomRequestInfo requestInfo)
        {
            TProtobuf protobuf;

            try
            {
                protobuf = Parser.ParseFrom(requestInfo.Body);
            }
            catch
            {
                Debug.Assert(false);
                return false;
            }

            Handle((dynamic)session, (dynamic)protobuf);
            return true;
        }
    }

    public class PacketHandlerManager<TSession>
    {
        internal Dictionary<int, IPacketHandlerBase<TSession>> PacketHandlers { get; }
            = new Dictionary<int, IPacketHandlerBase<TSession>>();

        public void Register<TProtobuf>(Enum protocolNumber)
            where TProtobuf : IMessage<TProtobuf>, new()
        {
            var typename = protocolNumber.GetType().Name;
            Debug.Assert(typename == "ProtocolNumber",
                $"Fail to register protocol number : {typename}");

            var pbNumber = (ushort)Convert.ToInt32(protocolNumber);
            PacketHandlers.Add(pbNumber, new PacketHandler<TSession, TProtobuf>());
        }
    }

    public class SessionHandlerBase<TSession>
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(SessionHandlerBase<TSession>));

        protected PacketHandlerManager<TSession> PacketHandlerManager =
            new PacketHandlerManager<TSession>();

        public virtual bool Initialize() { return false; }

        public virtual void Cleanup() { }

        public void RequestHandler(TSession session, CustomRequestInfo requestInfo)
        {
            if (!PacketHandlerManager.PacketHandlers.TryGetValue(requestInfo.PacketNumber,
                out var packetHandler))
            {
                Log.WarnFormat($"PacketHandler not found : {requestInfo.PacketNumber}");
                Debug.Assert(false);
                return;
            }

            packetHandler.Handle(session, requestInfo);
        }
    }
}
