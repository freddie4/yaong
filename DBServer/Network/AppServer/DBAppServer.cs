﻿using System.Configuration;
using System.Data;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;

namespace DBServer.Network
{
    class DBAppServer : AppServer<GameServerSession, CustomRequestInfo>
    {
        private readonly PacketSDHandler SessionHandler = new PacketSDHandler();

        public DBAppServer()
            : base(new DefaultReceiveFilterFactory<ReceiveFilter, CustomRequestInfo>())
        {
        }

        public static IDbConnection GetConnection(string dbName)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[dbName].ConnectionString;
            var connection = new MySql.Data.MySqlClient.MySqlConnection(connectionString);
            connection.Open();
            return connection;
        }

        public bool Initialize()
        {
            NewSessionConnected += ConnectCompletion;
            SessionClosed += DisconnectCompletion;
            NewRequestReceived += SessionHandler.RequestHandler;

            return SessionHandler.Initialize();
        }

        public override void Stop()
        {
            SessionHandler.Cleanup();
            base.Stop();
        }

        void ConnectCompletion(GameServerSession session)
        {

        }

        void DisconnectCompletion(GameServerSession session, CloseReason reason)
        {
        }
    }
}
