﻿using System;
using System.Diagnostics;
using Google.Protobuf;
using SuperSocket.Common;
using SuperSocket.Facility.Protocol;
using SuperSocket.SocketBase.Protocol;

namespace DBServer.Network
{
    public class CustomRequestInfo : BinaryRequestInfo
    {
        public static int PacketHeaderSize = 4;
        public ushort PacketSize { get; }
        public ushort PacketNumber { get; }

        public CustomRequestInfo(ushort packetSize, ushort packetNumber, byte[] body)
            : base(null, body)
        {
            PacketSize = packetSize;
            PacketNumber = packetNumber;
        }

        public static byte[] Serialize(IMessage packet)
        {
            var packetEnumName = "E" + packet.Descriptor.Name;

            if (!Enum.TryParse(packetEnumName, true,
                out PacketDS.ProtocolNumber protocolNumber))
            {
                Debug.Assert(false, $"Fail to parse packet number : {packetEnumName}");
            }

            var packetSize = (ushort) (PacketHeaderSize + packet.CalculateSize());
            var packetNumber = (ushort) protocolNumber;

            var sendBuffer = new byte[packetSize];
            BitConverter.GetBytes(packetSize).CopyTo(sendBuffer, 0);
            BitConverter.GetBytes(packetNumber).CopyTo(sendBuffer, 2);
            packet.ToByteArray().CopyTo(sendBuffer, 4);

            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(sendBuffer, 0, 2);
                Array.Reverse(sendBuffer, 2, 2);
            }

            return sendBuffer;
        }
    }

    public class ReceiveFilter : FixedHeaderReceiveFilter<CustomRequestInfo>
    {
        public ReceiveFilter()
            : base(CustomRequestInfo.PacketHeaderSize)
        {
        }

        protected override int GetBodyLengthFromHeader(byte[] header, int offset, int length)
        {
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(header, offset, 2);

            var packetSize = BitConverter.ToUInt16(header, offset);
            return packetSize - CustomRequestInfo.PacketHeaderSize;
        }

        protected override CustomRequestInfo ResolveRequestInfo(ArraySegment<byte> header,
            byte[] bodyBuffer, int offset, int length)
        {
            if (!BitConverter.IsLittleEndian)
            {
                Array.Reverse(header.Array ??
                    throw new InvalidOperationException("header is null"), 0, 2);
                Array.Reverse(header.Array, 2, 2);
            }

            return new CustomRequestInfo(BitConverter.ToUInt16(header.Array ??
                throw new InvalidOperationException("header is null"), 0),
                BitConverter.ToUInt16(header.Array, 0 + 2),
                bodyBuffer.CloneRange(offset, length));
        }
    }
}
