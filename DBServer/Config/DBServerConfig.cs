﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBServer.Config
{
    public static class DBServerConfig
    {
        public static readonly string GameDBName =
            ConfigurationManager.AppSettings["GameDBName"];
    }
}
