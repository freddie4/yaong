set SRC_DIR=D:\Yaong1\PacketDefine\Proto
set DST_CPP_PACKET_DEFINE_DIR=D:\Yaong1\PacketDefine\ProtoDefine
set DST_DB_SERVER_DIR=D:\Yaong1\DBServer\Network\ProtoDefine
set DST_DUMMY_CLIENT_DIR=D:\Yaong1\DummyClient\Packet

protoc -I=%SRC_DIR% --cpp_out=%DST_CPP_PACKET_DEFINE_DIR% %SRC_DIR%/PacketCSDefine.proto
protoc -I=%SRC_DIR% --csharp_out=%DST_DUMMY_CLIENT_DIR% %SRC_DIR%/PacketCSDefine.proto

protoc -I=%SRC_DIR% --cpp_out=%DST_CPP_PACKET_DEFINE_DIR% %SRC_DIR%/PacketSCDefine.proto
protoc -I=%SRC_DIR% --csharp_out=%DST_DUMMY_CLIENT_DIR% %SRC_DIR%/PacketSCDefine.proto

protoc -I=%SRC_DIR% --cpp_out=%DST_CPP_PACKET_DEFINE_DIR% %SRC_DIR%/PacketDSDefine.proto
protoc -I=%SRC_DIR% --csharp_out=%DST_DB_SERVER_DIR% %SRC_DIR%/PacketDSDefine.proto

protoc -I=%SRC_DIR% --cpp_out=%DST_CPP_PACKET_DEFINE_DIR% %SRC_DIR%/PacketSDDefine.proto
protoc -I=%SRC_DIR% --csharp_out=%DST_DB_SERVER_DIR% %SRC_DIR%/PacketSDDefine.proto

protoc -I=%SRC_DIR% --cpp_out=%DST_CPP_PACKET_DEFINE_DIR% %SRC_DIR%/ErrorDefine.proto
protoc -I=%SRC_DIR% --csharp_out=%DST_DB_SERVER_DIR% %SRC_DIR%/ErrorDefine.proto