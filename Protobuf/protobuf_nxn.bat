set SRC_DIR=C:\Users\NXN-013\source\repos\yaong\PacketDefine\Proto
set DST_CPP_PACKET_DEFINE_DIR=C:\Users\NXN-013\source\repos\yaong\PacketDefine\ProtoDefine
set DST_DB_SERVER_DIR=C:\Users\NXN-013\source\repos\yaong\DBServer\Network\ProtoDefine
set DST_CLIENT_DIR=G:\Yaong1\Client\Packet
set DST_DUMMY_CLIENT_DIR=C:\Users\NXN-013\source\repos\yaong\DummyClient\Packet
set UNREAL_CLIENT_DIR="G:\Unreal engine 4\Projects\Yaong\Source\Packet"

protoc -I=%SRC_DIR% --cpp_out=%DST_CPP_PACKET_DEFINE_DIR% %SRC_DIR%/PacketCSDefine.proto
protoc -I=%SRC_DIR% --csharp_out=%DST_DUMMY_CLIENT_DIR% %SRC_DIR%/PacketCSDefine.proto

protoc -I=%SRC_DIR% --cpp_out=%DST_CPP_PACKET_DEFINE_DIR% %SRC_DIR%/PacketSCDefine.proto
protoc -I=%SRC_DIR% --csharp_out=%DST_DUMMY_CLIENT_DIR% %SRC_DIR%/PacketSCDefine.proto

protoc -I=%SRC_DIR% --cpp_out=%DST_CPP_PACKET_DEFINE_DIR% %SRC_DIR%/PacketDSDefine.proto
protoc -I=%SRC_DIR% --csharp_out=%DST_DB_SERVER_DIR% %SRC_DIR%/PacketDSDefine.proto

protoc -I=%SRC_DIR% --cpp_out=%DST_CPP_PACKET_DEFINE_DIR% %SRC_DIR%/PacketSDDefine.proto
protoc -I=%SRC_DIR% --csharp_out=%DST_DB_SERVER_DIR% %SRC_DIR%/PacketSDDefine.proto

protoc -I=%SRC_DIR% --cpp_out=%DST_CPP_PACKET_DEFINE_DIR% %SRC_DIR%/ErrorDefine.proto
protoc -I=%SRC_DIR% --csharp_out=%DST_DB_SERVER_DIR% %SRC_DIR%/ErrorDefine.proto
