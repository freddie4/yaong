#pragma once

#define CLASSNAME_TO_STR(x) #x
#define DECLARE_PACKET_NAME(direction, enumValue) direction##_##enumValue
#define DECLARE_PROTOBUF_NAME(direction, enumValue) direction::enumValue
#define DECLARE_PROTOCOL_NUMBER(direction, enumValue) direction::ProtocolNumber::E##enumValue

#define DECLARE_PACKET_TYPE(direction, enumValue)\
class DECLARE_PACKET_NAME(direction, enumValue) : public DECLARE_PROTOBUF_NAME(direction, enumValue)\
{\
public:\
    DECLARE_PACKET_NAME(direction, enumValue)() {}\
    virtual ~ DECLARE_PACKET_NAME(direction, enumValue)() {}\
	DECLARE_PACKET_NAME(direction, enumValue)(const DECLARE_PACKET_NAME(direction, enumValue)&) = delete;\
	DECLARE_PACKET_NAME(direction, enumValue)& operator=(const DECLARE_PACKET_NAME(direction, enumValue)&) = delete;\
	DECLARE_PACKET_NAME(direction, enumValue)(DECLARE_PACKET_NAME(direction, enumValue)&&) = delete;\
	DECLARE_PACKET_NAME(direction, enumValue)& operator=(DECLARE_PACKET_NAME(direction, enumValue)&&) = delete;\
    static constexpr unsigned short PROTOCOL_NUMBER = DECLARE_PROTOCOL_NUMBER(direction, enumValue);\
    static const std::string GetPacketName() { return CLASSNAME_TO_STR(direction##_##enumValue); }\
};
