﻿using System;
using System.Diagnostics;

namespace PacketGenerator
{
    internal class ProtoExecute
    {
        public bool Execute(string protoBatchFilename)
        {
            var processInfo = new ProcessStartInfo
            {
                FileName = "cmd.exe",
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            using (var process = new Process())
            {
                process.EnableRaisingEvents = false;
                process.StartInfo = processInfo;
                process.Start();
                process.StandardInput.WriteLine("cd " + PacketConfig.ProtoExeFileDirectory);
                process.StandardInput.WriteLine("{0}", protoBatchFilename);
                process.StandardInput.Close();

                while (true)
                {
                    var output = process.StandardOutput.ReadLine();
                    if (output == null)
                        break;

                    if (output.Length == 0 || output.StartsWith("Microsoft Windows")
                       || output.StartsWith("(c) 20"))
                        continue;

                    Console.WriteLine(output);
                }

                var hasError = false;
                while (true)
                {
                    var error = process.StandardError.ReadLine();
                    if (error == null)
                        break;

                    hasError = true;
                    Console.WriteLine(error);
                }

                if (hasError || process.ExitCode != 0)
                {
                    return false;
                }
            }

            Console.WriteLine("Proto.exe executing...");

            return true;
        }
    }
}
