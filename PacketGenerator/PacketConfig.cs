﻿using System;
using System.Configuration;

namespace PacketGenerator
{
    public static class PacketConfig
    {
        public static string ProtoExeFileDirectory =
            ConfigurationManager.AppSettings["ProtoExeFileDirectory"];

        public static string ProtofileDirectory =
            ConfigurationManager.AppSettings["ProtoFileDirectory"];

        public static string PacketDeclarePath =
            ConfigurationManager.AppSettings["PacketDeclarePath"];

        public static string GameServer_HandlerPath =
            ConfigurationManager.AppSettings["GameServer_HandlerPath"];

        public static string GameServer_ClientSession =
            ConfigurationManager.AppSettings["GameServer_ClientSession"];

        public static string GameServer_DBServerSession =
            ConfigurationManager.AppSettings["GameServer_DBServerSession"];
    }
}
