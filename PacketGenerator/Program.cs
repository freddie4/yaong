﻿
namespace PacketGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var protoExecute = new ProtoExecute();

            if (protoExecute.Execute(args[0]))
            {
                var packetGenerator = new PacketGenerator();
                if (packetGenerator.FindProtofiles())
                {
                    packetGenerator.Generate();
                }
            }
        }
    }
}
