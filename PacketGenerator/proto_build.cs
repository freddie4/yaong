//css_args -sconfig;

using System;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;

class Program
{
    static public void Main(string[] args)
    {
        Environment.ExitCode = 1;

        if (args.Length != 1)
        {
            Console.WriteLine("Usage: proto_build 'proto directory path'");
            return;
        }

        if (ConfigurationManager.AppSettings.Count < 1)
        {
            Console.WriteLine("proto_build.exe.config file was not found. or missing -sconfig option.");
            return;
        }

        string path = args[0];

        if (!Directory.Exists(path))
        {
            Console.WriteLine(string.Format("Directory is not exist. {0}", path));
            return;
        }

        Console.WriteLine("== Begin to generate proto files. ==");

        ProtoBuild protoBuild = new ProtoBuild(path);
        if (!protoBuild.Execute())
            return;

        DeclarePacketTypeGenerator packetTypeGenerator = new DeclarePacketTypeGenerator(path);
        packetTypeGenerator.Execute();

        SessionHandlerDeclareGenerator handlerDeclareGenerator = new SessionHandlerDeclareGenerator(path);
        if (!handlerDeclareGenerator.Execute())
            return;

        Environment.ExitCode = 0;
        Console.WriteLine("== Successfully generated proto files. ==");
    }
}

class ProtoBuild
{
    readonly private string _protoPath;
    readonly private string _protocExePath;
    readonly private string _protoCppOutPath;
    readonly private string _protoCSharpOutPath;
    readonly private string _tempCppOut;
    readonly private string _tempCSharpOut;

    public ProtoBuild(string path)
    {
        _protoPath = path;
        _protocExePath = Helper.GetProtocPathFromProtoPath(path);

        _protoCppOutPath = Helper.GetCppOutPathFromProtoPath(path);
        _protoCSharpOutPath = Helper.GetCSharpOutPathFromProtoPath(path);

        _tempCppOut = Helper.GetTempPathCppout();
        _tempCSharpOut = Helper.GetTempPathCSharpOut();
    }

    public bool Execute()
    {
        RemoveAndCreateOutputFolders();

        string commandLine = BuildCommandLine();
        CmdExecute cmdExecute = new CmdExecute();
        if (!cmdExecute.Execute(commandLine))
        {
            Console.WriteLine(string.Format("Fail to execute command. {0}", commandLine));
            return false;
        }

        CopyFromOutputFolders();
        return true;
    }

    private void RemoveAndCreateOutputFolders()
    {
        Helper.RemoveAndCreateFolder(_tempCppOut);
        Helper.RemoveAndCreateFolder(_tempCSharpOut);
    }

    private void CopyFromOutputFolders()
    {
        bool removeNotExistFileOnPathFrom = true;
        Helper.CopyNewOrChangedFiles(_tempCppOut, _protoCppOutPath, removeNotExistFileOnPathFrom);
        Helper.CopyNewOrChangedFiles(_tempCSharpOut, _protoCSharpOutPath, removeNotExistFileOnPathFrom);
    }

    private string GetProtoFiles()
    {
        string protoFiles = string.Empty;

        string[] fileEntries = Directory.GetFiles(_protoPath);
        foreach (var filePath in fileEntries)
        {
            if (Path.GetExtension(filePath) != ".proto")
                continue;

            if (protoFiles.Length != 0)
            {
                protoFiles += " ";
            }

            protoFiles += filePath;
        }

        return protoFiles;
    }

    private string BuildCommandLine()
    {
        return string.Format("{0} -I={1} --cpp_out={2} --csharp_out={3} {4}",
            _protocExePath, _protoPath, _tempCppOut, _tempCSharpOut, GetProtoFiles());
    }
}

class DeclarePacketTypeGenerator
{
    readonly private string _protoPath;
    readonly private string _destinationGeneratedPath;
    readonly private string _tempDeclarePacketTypeOut;

    public DeclarePacketTypeGenerator(string path)
    {
        _protoPath = path;
        _destinationGeneratedPath = Directory.GetParent(path).FullName;
        _tempDeclarePacketTypeOut = Helper.GetTempPathDeclarePacketTypeOut();
    }

    public void Execute()
    {
        RemoveAndCreateOutputFolders();

        GenerateFiles();

        CopyFromOutputFolders();
    }

    private void RemoveAndCreateOutputFolders()
    {
        Helper.RemoveAndCreateFolder(_tempDeclarePacketTypeOut);
    }

    private void GenerateFiles()
    {
        foreach (var protoFilePath in Helper.GetProtoFilePaths(_protoPath))
        {
            string destinationFilePath = Path.Combine(_tempDeclarePacketTypeOut, string.Format("p_{0}.h", Path.GetFileNameWithoutExtension(protoFilePath)));
            ConvertProtoToDeclareHeader(protoFilePath, destinationFilePath);
        }
    }

    private void ConvertProtoToDeclareHeader(string protoFilePath, string destinationFilePath)
    {
        if (!Helper.IsExistProtocolNumber(protoFilePath))
            return;

        List<string> messages = Helper.GetMessages(protoFilePath);
        string firstPackageName = Helper.GetFirstPackageName(protoFilePath);
        string secondPackageName = Helper.GetSecondPackageName(protoFilePath);

        using (TextWriter writer = File.CreateText(destinationFilePath))
        {
            writer.WriteLine("/////////////////////////////////////////////////");
            writer.WriteLine("// This file is auto generated by proto_build. //");
            writer.WriteLine("/////////////////////////////////////////////////");
            writer.WriteLine("#pragma once");
            writer.WriteLine();
            writer.WriteLine(string.Format("#include \"{0}\"", "packet_define.h"));
            writer.WriteLine(string.Format("#include \"{0}.pb.h\"", Path.GetFileNameWithoutExtension(protoFilePath)));
            writer.WriteLine();
            writer.WriteLine(string.Format("namespace {0}", firstPackageName));
            writer.WriteLine("{");
            writer.WriteLine();

            foreach (var message in messages)
            {
                writer.WriteLine(string.Format("DECLARE_PACKET_TYPE({0}, {1});", secondPackageName, message));
            }

            writer.WriteLine();
            writer.Write("} ");
            writer.WriteLine(string.Format("// namespace {0}", firstPackageName));
            writer.WriteLine();
            writer.WriteLine(string.Format("using namespace {0};", firstPackageName));
        }
    }

    private void CopyFromOutputFolders()
    {
        bool removeNotExistFileOnPathFrom = false;
        Helper.CopyNewOrChangedFiles(_tempDeclarePacketTypeOut, _destinationGeneratedPath, removeNotExistFileOnPathFrom);
    }
}

class SessionHandlerDeclareGenerator
{
    readonly private string _protoPath;
    readonly private string _tempSessionHandlerOut;

    private List<HandlerData> _handlerDatas = new List<HandlerData>();

    private struct HandlerData
    {
        public string Name { get; set; }
        public string SessionName { get; set; }
        public string ProtoFilename { get; set; }
        public string OutputFilePath { get; set; }
    }

    public SessionHandlerDeclareGenerator(string protoPath)
    {
        _protoPath = protoPath;
        _tempSessionHandlerOut = Helper.GetTempPathSessionHandlerOut();
    }

    public bool Execute()
    {
        if (!LoadFromConfigFiles())
        {
            Console.WriteLine("Fail to load from config files.");
            return false;
        }

        RemoveAndCreateOutputFolders();

        if (!GenerateAndCopyNewOrChangedFiles())
            return false;

        return true;
    }

    private bool LoadFromConfigFiles()
    {
        _handlerDatas.Clear();

        var appSettings = ConfigurationManager.AppSettings;
        foreach (string keyName in appSettings.Keys)
        {
            string value = appSettings.Get(keyName);
            var splitedValue = value.Split('|');
            if (splitedValue.Length != 3)
            {
                return false;
            }

            _handlerDatas.Add(new HandlerData{
                Name = keyName,
                SessionName = splitedValue[0], ProtoFilename = splitedValue[1], OutputFilePath = splitedValue[2]});
        }

        return true;
    }

    private void RemoveAndCreateOutputFolders()
    {
        Helper.RemoveAndCreateFolder(_tempSessionHandlerOut);
    }

    private bool GenerateAndCopyNewOrChangedFiles()
    {
        foreach (var handlerData in _handlerDatas)
        {
            string generatedFilePath = Path.Combine(_tempSessionHandlerOut, Path.GetFileName(handlerData.OutputFilePath));
            GenerateFile(handlerData.SessionName, Path.Combine(_protoPath, handlerData.ProtoFilename), generatedFilePath);

            string toPath = Path.GetFullPath(Path.Combine(_protoPath, handlerData.OutputFilePath));
            if (!Helper.CopyNewOrChangedFile(generatedFilePath, toPath))
            {
                Console.WriteLine(string.Format("Fail to copy file. from: {0}, to: {1}", generatedFilePath, toPath));
                return false;
            }
        }

        return true;
    }

    private void GenerateFile(string sessionName, string protoFilePath, string destinationFilePath)
    {
        List<string> messages = Helper.GetMessages(protoFilePath);
        string firstPackageName = Helper.GetFirstPackageName(protoFilePath);
        string secondPackageName = Helper.GetSecondPackageName(protoFilePath);

        using (TextWriter writer = File.CreateText(destinationFilePath))
        {
            writer.WriteLine("/////////////////////////////////////////////////");
            writer.WriteLine("// This file is auto generated by proto_build. //");
            writer.WriteLine("/////////////////////////////////////////////////");
            writer.WriteLine("#pragma once");
            writer.WriteLine();

            string sessionHeaderFileName = string.Empty;
            Helper.ConvertPascalCaseToSnakeCase(sessionName, ref sessionHeaderFileName);
            writer.WriteLine(string.Format("#include \"{0}.h\"", sessionHeaderFileName));
            writer.WriteLine(string.Format("#include \"p_{0}.h\"", Path.GetFileNameWithoutExtension(protoFilePath)));
            writer.WriteLine();
            writer.WriteLine("namespace nxn");
            writer.WriteLine("{");
            writer.WriteLine();

            foreach (var message in messages)
            {
                writer.WriteLine(string.Format("DECLARE_HANDLER({0}, {1}_{2});", sessionName, secondPackageName, message));
            }

            writer.WriteLine();
            writer.WriteLine(string.Format("// cppcheck-suppress syntaxError\r\nIMPLEMENT_INITIALIZE({0})", sessionName));
            writer.WriteLine("{");

            foreach (var message in messages)
            {
                writer.WriteLine(string.Format("    REGISTER_HANDLER({0}_{1});", secondPackageName, message));
            }

            writer.WriteLine("}");
            writer.WriteLine();
            writer.WriteLine("} // namespace nxn");
        }
    }
}

class Helper
{
    static public void ConvertPascalCaseToSnakeCase(string pascalCase, ref string snakeCase)
    {
        snakeCase = string.Concat(pascalCase.Select((x, i) => i > 0 && char.IsUpper(x) ? "_" + x.ToString() : x.ToString())).ToLower();
    }

    static public void RemoveAndCreateFolder(string path)
    {
        if (Directory.Exists(path))
            Directory.Delete(path, true);

        Directory.CreateDirectory(path);
    }

    static private bool CompareFileSizes(string fileName1, string fileName2)
    {
        bool fileSizeEqual = true;

        // Create System.IO.FileInfo objects for both files
        FileInfo fileInfo1 = new FileInfo(fileName1);
        FileInfo fileInfo2 = new FileInfo(fileName2);

        // Compare file sizes
        if (fileInfo1.Length != fileInfo2.Length)
        {
            // File sizes are not equal therefore files are not identical
            fileSizeEqual = false;
        }

        return fileSizeEqual;
    }

    static public bool CompareFileBytes(string fileName1, string fileName2)
    {
        // Compare file sizes before continuing. 
        // If sizes are equal then compare bytes.
        if (CompareFileSizes(fileName1, fileName2))
        {
            int file1byte = 0;
            int file2byte = 0;

            // Open a System.IO.FileStream for each file.
            // Note: With the 'using' keyword the streams 
            // are closed automatically.
            using (FileStream fileStream1 = new FileStream(fileName1, FileMode.Open, FileAccess.Read),
                              fileStream2 = new FileStream(fileName2, FileMode.Open, FileAccess.Read))
            {
                // Read and compare a byte from each file until a
                // non-matching set of bytes is found or the end of
                // file is reached.
                do
                {
                    file1byte = fileStream1.ReadByte();
                    file2byte = fileStream2.ReadByte();
                }
                while ((file1byte == file2byte) && (file1byte != -1));
            }

            return ((file1byte - file2byte) == 0);
        }
        else
        {
            return false;
        }
    }

    static public bool IsSameFile(string filePathFrom, string filePathTo)
    {
        if (!File.Exists(filePathFrom))
        {
            Console.WriteLine(string.Format("Fail to find file. file is not existed. {0}", filePathFrom));
            return false;
        }

        if (!File.Exists(filePathTo))
        {
            Console.WriteLine(string.Format("Fail to find file. file is not existed. {0}", filePathTo));
            return false;
        }

        return CompareFileBytes(filePathFrom, filePathTo);
    }

    static public bool CopyNewOrChangedFile(string filePathFrom, string filePathTo)
    {
        if (!File.Exists(filePathFrom))
        {
            Console.WriteLine(string.Format("Fail to copy file. file is not existed. {0}", filePathFrom));
            return false;
        }

        if (File.Exists(filePathTo))
        {
            bool isSame = CompareFileBytes(filePathFrom, filePathTo);
            if (!isSame)
            {
                File.Copy(filePathFrom, filePathTo, true);
            }
        }
        else
        {
            File.Copy(filePathFrom, filePathTo, true);
        }

        return true;
    }

    static public bool CopyNewOrChangedFiles(string pathFrom, string pathTo, bool removeNotExistFileOnPathFrom)
    {
        if (!Directory.Exists(pathFrom) || !Directory.Exists(pathTo))
        {
            Console.WriteLine("Fail to copy file. directory is not existed.");
            return false;
        }

        // Copy new or changed files
        {
            string[] fileEntries = Directory.GetFiles(pathFrom);
            foreach (var filePath in fileEntries)
            {
                string destFilePath = Path.GetFullPath(Path.Combine(pathTo, Path.GetFileName(filePath)));

                if (!CopyNewOrChangedFile(filePath, destFilePath))
                {
                    Console.WriteLine(string.Format("Fail to copy file. from: {0}, to: {1}", filePath, destFilePath));
                    return false;
                }
            }
        }

        // Remove not exist files
        if (removeNotExistFileOnPathFrom)
        {
            string[] fileEntries = Directory.GetFiles(pathTo);
            foreach (var filePath in fileEntries)
            {
                string destFilePath = Path.Combine(pathFrom, Path.GetFileName(filePath));
                if (!File.Exists(destFilePath))
                {
                    File.Delete(filePath);
                }
            }
        }

        return true;
    }

    static public List<string> GetProtoFilePaths(string protoPath)
    {
        List<string> protoFilePaths = new List<string>();

        if (!Directory.Exists(protoPath))
            return protoFilePaths;

        return Directory.GetFiles(protoPath).ToList();
    }

    static public string GetProtocPathFromProtoPath(string protoPath)
    {
        DirectoryInfo firstParentDirectoryInfo = Directory.GetParent(protoPath);
        DirectoryInfo secondParentDirectoryInfo = Directory.GetParent(firstParentDirectoryInfo.FullName);
        DirectoryInfo thirdParentDirectoryInfo = Directory.GetParent(secondParentDirectoryInfo.FullName);
        return Path.Combine(thirdParentDirectoryInfo.FullName, "build/protoc-3.6.1/bin/protoc.exe");
    }

    static public bool IsExistProtocolNumber(string protoFilePath)
    {
        foreach (var line in File.ReadLines(protoFilePath))
        {
            if (line.StartsWith("enum"))
            {
                var splitedLine = line.Split();
                if (splitedLine.Length >= 2)
                {
                    if (splitedLine[1] == "ProtocolNumber")
                        return true;
                }
            }
        }

        return false;
    }

    static public List<string> GetMessages(string protoFilePath)
    {
        List<string> messages = new List<string>();

        foreach (var line in File.ReadAllLines(protoFilePath))
        {
            if (line.StartsWith("message"))
            {
                var splitedLine = line.Split();
                if (splitedLine.Length >= 2)
                    messages.Add(splitedLine[1]);
            }
        }

        return messages;
    }

    static public string GetFirstPackageName(string protoFilePath)
    {
        string packageName = string.Empty;

        foreach (var line in File.ReadLines(protoFilePath))
        {
            if (line.StartsWith("package"))
            {
                var splitedLine = line.Split(' ', '.', ';');
                if (splitedLine.Length > 3)
                {
                    packageName = splitedLine[1];
                    break;
                }
            }
        }

        return packageName;
    }

    static public string GetSecondPackageName(string protoFilePath)
    {
        string packageName = string.Empty;

        foreach (var line in File.ReadLines(protoFilePath))
        {
            if (line.StartsWith("package"))
            {
                var splitedLine = line.Split(' ', '.', ';');
                if (splitedLine.Length > 3)
                {
                    packageName = splitedLine[2];
                    break;
                }
            }
        }

        return packageName;
    }

    static public string GetCppOutPathFromProtoPath(string protoPath)
    {
        DirectoryInfo parentDirectoryInfo = Directory.GetParent(protoPath);
        return Path.Combine(parentDirectoryInfo.FullName, "pb_cpp");
    }

    static public string GetCSharpOutPathFromProtoPath(string protoPath)
    {
        DirectoryInfo grandParentDirectoryInfo = (Directory.GetParent(protoPath)).Parent;
        string csharpOutPath = grandParentDirectoryInfo.FullName;
        csharpOutPath = Path.Combine(csharpOutPath, "nxn_x_db_server");
        csharpOutPath = Path.Combine(csharpOutPath, "pb_csharp");
        return csharpOutPath;
    }

    static public string GetTempPathCppout()
    {
        return Path.Combine(Path.GetTempPath(), "gameX_server_cpp_out");
    }

    static public string GetTempPathCSharpOut()
    {
        return Path.Combine(Path.GetTempPath(), "gameX_server_csharp_out");
    }

    static public string GetTempPathDeclarePacketTypeOut()
    {
        return Path.Combine(Path.GetTempPath(), "gameX_server_declare_packet_type_out");
    }

    static public string GetTempPathSessionHandlerOut()
    {
        return Path.Combine(Path.GetTempPath(), "gameX_server_session_handler_out");
    }
}

class CmdExecute
{
    private ProcessStartInfo _processStartInfo = new ProcessStartInfo();

    public CmdExecute()
    {
        _processStartInfo.FileName = "cmd.exe";
        _processStartInfo.CreateNoWindow = true;
        _processStartInfo.UseShellExecute = false;
        _processStartInfo.RedirectStandardOutput = true;
        _processStartInfo.RedirectStandardInput = true;
        _processStartInfo.RedirectStandardError = true;
    }

    public bool Execute(string command)
    {
        using (Process process = new Process())
        {
            process.EnableRaisingEvents = false;
            process.StartInfo = _processStartInfo;
            process.Start();
            process.StandardInput.WriteLine(command);
            process.StandardInput.Close();

            while (true)
            {
                string output = process.StandardOutput.ReadLine();
                if (output == null)
                    break;

                if (output.Length == 0 || output.StartsWith("Microsoft Windows") || output.StartsWith("(c) 20"))
                    continue;

                Console.WriteLine(output);
            }

            bool hasError = false;
            while (true)
            {
                string error = process.StandardError.ReadLine();
                if (error == null)
                    break;

                hasError = true;
                Console.WriteLine(error);
            }

            if (hasError || process.ExitCode != 0)
            {
                return false;
            }

            return true;
        }
    }
}